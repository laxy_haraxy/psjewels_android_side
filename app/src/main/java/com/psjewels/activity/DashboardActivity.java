package com.psjewels.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.messaging.Constants;
import com.psjewels.R;
import com.psjewels.adapter.CatListAdapter;
import com.psjewels.adapter.CatWiseProductListAdapter;
import com.psjewels.adapter.NewlyArrivalAdapter;
import com.psjewels.databinding.ActivityDashboardBinding;
import com.psjewels.databinding.DialogBankDetailsBinding;
import com.psjewels.model.AddCategory;
import com.psjewels.model.AddUser;
import com.psjewels.model.ProductDeatils;
import com.psjewels.model.SearchKeyword;
import com.psjewels.model.SubCatList;
import com.psjewels.utils.AutoCompleteSubCatNameAdapter;
import com.psjewels.utils.Constant;
import com.psjewels.utils.SpacingItemDecoration;
import com.psjewels.utils.Tools;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class DashboardActivity extends AppCompatActivity {

    ActivityDashboardBinding mActivityDashboardBinding;
    CollectionReference mCollectionReferenceCatList;
    CatListAdapter mCatListAdapter;
    CollectionReference mCollectionReferenceUserList;

    NewlyArrivalAdapter mNewlyArrivalAdapter;
    LinearLayoutManager HorizontalLayout;
    CollectionReference mCollectionReferenceProductListForNewArriaval;
    boolean singleBack = false;

    String IMEI_Number_Holder;
    TelephonyManager telephonyManager;

    List<DocumentSnapshot> mDocumentChangeList;
    CatWiseProductListAdapter mCatWiseProductListAdapter;
    int loopCount = 0;
    int totalSize = 0;
    List<SearchKeyword> subCategoryList;
    ArrayAdapter<String> adapter;
    String docId, mKey;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_dashboard);
        binding();
        checkConnection();
        initDb();
        getCategory();
        clickEvent();
        getNewlyArrival();
        searchViewAllProducts();
        getData();
        bankDetails();
        checkUserStatus();


        mActivityDashboardBinding.logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FirebaseFirestore.getInstance().collection("product_list").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (DocumentSnapshot document : task.getResult()) {
                                ProductDeatils productModel = document.toObject(ProductDeatils.class);
//                                mDocumentChangeList.add(document);

                                Log.d("asdasd", "getPro_concet_name:-----  " + productModel.getPro_concet_name() + "------document.getId():----" + document.getId());
                            }


                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("asdasdasd", "onFailure: " + e.getLocalizedMessage());

                    }
                });

            }
        });

    }


    private void checkUserStatus() {
        FirebaseUser mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        if (mFirebaseUser != null) {
            mCollectionReferenceUserList.document(mFirebaseUser.getUid()).addSnapshotListener(new EventListener<DocumentSnapshot>() {
                @Override
                public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                    if (documentSnapshot != null) {
                        AddUser mAddUser = documentSnapshot.toObject(AddUser.class);
                        String deviceIdCurrent = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

                        if (mAddUser != null && !mAddUser.isStatus()) {

                            Intent newIntent = new Intent(DashboardActivity.this, RegistrationTemporayScreenActivity.class);
                            newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(newIntent);

                        } else if (mAddUser != null && !mAddUser.getDeviceId().equals(deviceIdCurrent)) {
                            FirebaseAuth.getInstance().signOut();
                            Intent newIntent = new Intent(DashboardActivity.this, LoginScreenActivity.class);
                            newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(newIntent);
                        }

                    }

                }
            });
        }
    }

    private void bankDetails() {

        mActivityDashboardBinding.iconBankDeatils.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final DialogBankDetailsBinding mDialogBankDetailsBinding;
                final Dialog dialog = new Dialog(DashboardActivity.this, R.style.MyAlertDialogTheme);
                mDialogBankDetailsBinding = DataBindingUtil.inflate(LayoutInflater.from(DashboardActivity.this), R.layout.dialog_bank_details, null, false);
                mDialogBankDetailsBinding.imgclose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });

               dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);

                dialog.setContentView(mDialogBankDetailsBinding.getRoot());
                dialog.show();

            }
        });
    }

    private void getData() {

//
////____________________Real_Time_Code__________________________________________________________________________________________________________________________________________________________________________________________________
//        if (!((Activity) this).isFinishing()) {
////            Toast.makeText(this, Constant.PROGRESS_DIALOG_MESSAGE, Toast.LENGTH_SHORT).show();
//        }
//        mDocumentChangeList = new ArrayList<>();
//
//
//        mCatWiseProductListAdapter = new CatWiseProductListAdapter(mDocumentChangeList, DashboardActivity.this);
//        mActivityDashboardBinding.productListRecyclerView.setLayoutManager(new GridLayoutManager(DashboardActivity.this, 2));
//        mActivityDashboardBinding.productListRecyclerView.setAdapter(mCatWiseProductListAdapter);
//
//        Query mQuery = FirebaseFirestore.getInstance().collection(Constant.PRODUCT_LIST_REF).orderBy(Constant.TIME_STAMP_KEY, Query.Direction.DESCENDING);
//
//        Query mQueryfinal = mQuery.orderBy(Constant.TIME_STAMP_KEY, Query.Direction.DESCENDING);
//        mQuery.addSnapshotListener(new EventListener<QuerySnapshot>() {
//            @Override
//            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
//                if (!((Activity) DashboardActivity.this).isFinishing()) {
////                    Toast.makeText(this, Constant.PROGRESS_DIALOG_MESSAGE, Toast.LENGTH_SHORT).show();
//                }
//
//
//                if (queryDocumentSnapshots != null) {
//                    ArrayList<DocumentSnapshot> mDocumentSnapshotsBookingListArray = new ArrayList<>();
////                    mMangeBookingBinding.txtTodaysAppoin.setText(String.valueOf(queryDocumentSnapshots.size()));
//                    mDocumentChangeList.clear();
//                    mDocumentChangeList.addAll(queryDocumentSnapshots.getDocuments());
//                    mCatWiseProductListAdapter.notifyDataSetChanged();
//                } else {
//                    Log.d(Constants.TAG, "onEvent: NULL");
//                }
//            }
//        });


    }


    @SuppressLint("ClickableViewAccessibility")
    private void searchViewAllProducts() {
        mActivityDashboardBinding.autoCompleteText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mActivityDashboardBinding.close.setVisibility(View.VISIBLE);
                return false;
            }
        });


        mActivityDashboardBinding.close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                in.hideSoftInputFromWindow(v.getWindowToken(), 0);
                mActivityDashboardBinding.autoCompleteText.setText(null);
                mActivityDashboardBinding.productListRecyclerView.setVisibility(View.GONE);

//                getData();
                onStart();
                mActivityDashboardBinding.close.setVisibility(View.GONE);
                mActivityDashboardBinding.allProductSearch.setVisibility(View.GONE);
            }
        });


//        mActivityDashboardBinding.autoCompleteText.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//                mActivityDashboardBinding.productListRecyclerView.setVisibility(View.VISIBLE);
//                filter(s.toString());
//
//            }
//        });


        //---------------

        CollectionReference mCollectionReferenceSubCatName = FirebaseFirestore.getInstance().collection(Constant.SEARCH_KEYWORD_LIST_COLL);

        subCategoryList = new ArrayList<>();
        mCollectionReferenceSubCatName.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {

                for (final QueryDocumentSnapshot documentSnapshot : task.getResult()) {
                    SearchKeyword mSubCategory = documentSnapshot.toObject(SearchKeyword.class);
                    docId = documentSnapshot.getId();
                    subCategoryList.add(mSubCategory);
                }
                AutoCompleteSubCatNameAdapter mCustomAdapter = new AutoCompleteSubCatNameAdapter(DashboardActivity.this, subCategoryList);
                mActivityDashboardBinding.autoCompleteText.setAdapter(mCustomAdapter);
                mActivityDashboardBinding.autoCompleteText.setThreshold(1);
                onStart();

            }
        });

        //------------
        mActivityDashboardBinding.autoCompleteText.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String keyword = mActivityDashboardBinding.autoCompleteText.getText().toString();

                Intent i = new Intent(DashboardActivity.this, SearchActivity.class);
                i.putExtra("keyword", keyword);
                startActivity(i);


                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
            }
        });
        //------------


        //---------------

    }

    private void filter(String text) {


//
//        ArrayList<DocumentSnapshot> filteredList = new ArrayList<>();
//        for (DocumentSnapshot mSubCategoryDS : mDocumentChangeList) {
//            ProductDeatils mProductDeatils = mSubCategoryDS.toObject(ProductDeatils.class);
//            if ((mProductDeatils.getPro_code() + mProductDeatils.getPro_code_number() + mProductDeatils.getKeyword_list_name()).toLowerCase().contains(text.toLowerCase())) {
//                filteredList.add(mSubCategoryDS);
//            } else {
//                mActivityDashboardBinding.allProductSearch.setVisibility(View.VISIBLE);
//            }
//        }
//        mCatWiseProductListAdapter.filterList(filteredList);
//
//
//


    }


    private void getNewlyArrival() {

        Query mQuery = mCollectionReferenceProductListForNewArriaval.limit(20);
        FirestoreRecyclerOptions<ProductDeatils> mProductDetailsFirestoreRecyclerOptions = new FirestoreRecyclerOptions
                .Builder<ProductDeatils>()
                .setQuery(mQuery.orderBy(Constant.TIME_STAMP_KEY, Query.Direction.DESCENDING), ProductDeatils.class).build();
        mNewlyArrivalAdapter = new NewlyArrivalAdapter(mProductDetailsFirestoreRecyclerOptions, this);

        HorizontalLayout
                = new LinearLayoutManager(
                DashboardActivity.this,
                LinearLayoutManager.HORIZONTAL,
                false);

        mActivityDashboardBinding.newlyArrival.setLayoutManager(HorizontalLayout);
        mActivityDashboardBinding.newlyArrival.setLayoutManager(new GridLayoutManager(DashboardActivity.this, 2));
        mActivityDashboardBinding.newlyArrival.addItemDecoration(new SpacingItemDecoration(2, Tools.dpToPx(DashboardActivity.this, 15), true));
        mActivityDashboardBinding.newlyArrival.setAdapter(mNewlyArrivalAdapter);

        //checkactivity
        mQuery.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if (queryDocumentSnapshots != null) {
                    if (queryDocumentSnapshots.size() > 0) {
                        mActivityDashboardBinding.emptyView.setVisibility(View.GONE);
                        mActivityDashboardBinding.newlyArrival.setVisibility(View.VISIBLE);
                    } else {

                        mActivityDashboardBinding.emptyView.setVisibility(View.VISIBLE);
                        mActivityDashboardBinding.newlyArrival.setVisibility(View.GONE);
                    }
                } else {

                    mActivityDashboardBinding.emptyView.setVisibility(View.VISIBLE);
                    mActivityDashboardBinding.newlyArrival.setVisibility(View.GONE);
                }
            }
        });


    }

    private void clickEvent() {
        mActivityDashboardBinding.iconCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), CartActivity.class));
            }
        });


        mActivityDashboardBinding.orderDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), MyOrderActivity.class));
            }
        });


        mActivityDashboardBinding.iconBankDeatils.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    public boolean checkConnection() {

        ConnectivityManager manager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = manager.getActiveNetworkInfo();

        if (null != activeNetwork) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
//                Toast.makeText(this, "Wifi Enabled", Toast.LENGTH_SHORT).show();
                return true;
            }
            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
//                Toast.makeText(this, "Data Network Enabled", Toast.LENGTH_SHORT).show();
                return true;
            }
        } else {
            Toast.makeText(this, "No Internet Connection", Toast.LENGTH_SHORT).show();
            return false;
        }
        return false;
    }

    private void getCategory() {
        FirestoreRecyclerOptions<AddCategory> mCategoryFirestoreRecyclerOptions = new FirestoreRecyclerOptions
                .Builder<AddCategory>()
                .setQuery(mCollectionReferenceCatList, AddCategory.class).build();

        mCatListAdapter = new CatListAdapter(mCategoryFirestoreRecyclerOptions, DashboardActivity.this);
        mActivityDashboardBinding.categoryList.setLayoutManager(new GridLayoutManager(DashboardActivity.this, 2));
        mActivityDashboardBinding.categoryList.addItemDecoration(new SpacingItemDecoration(2, Tools.dpToPx(DashboardActivity.this, 4), true));
        mActivityDashboardBinding.categoryList.setLayoutManager(new GridLayoutManager(DashboardActivity.this, 2));
        mActivityDashboardBinding.categoryList.setAdapter(mCatListAdapter);
    }

    private void initDb() {
        mCollectionReferenceCatList = FirebaseFirestore.getInstance().collection(Constant.CAT_LIST_REF);
//        mQueryProductListLastColle = FirebaseFirestore.getInstance().collection(Constant.PRODUCT_LIST_REF).orderBy(Constant.TIME_STAMP_KEY, Query.Direction.DESCENDING).limit(4);
        mCollectionReferenceProductListForNewArriaval = FirebaseFirestore.getInstance().collection(Constant.PRODUCT_LIST_REF);
        mCollectionReferenceUserList = FirebaseFirestore.getInstance().collection(Constant.USER_LIST_REF);

    }

    private void binding() {

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mActivityDashboardBinding = DataBindingUtil.setContentView(this, R.layout.activity_dashboard);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
    }


    @Override
    protected void onStart() {
        super.onStart();

        mCatListAdapter.startListening();
        mNewlyArrivalAdapter.startListening();

        checkUserIsExistOrNot();


    }

    @Override
    protected void onStop() {
        super.onStop();
        mCatListAdapter.stopListening();
        mNewlyArrivalAdapter.stopListening();
    }

    private void checkUserIsExistOrNot() {
        FirebaseAuth mAuth;
        mAuth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        FirebaseFirestore.getInstance().collection(Constant.USER_LIST_REF).document(currentUser.getUid()).addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {

                if (documentSnapshot.exists()) {
//                    Toast.makeText(DashboardActivity.this, "exist", Toast.LENGTH_SHORT).show();
                } else {
                    finish();
                    mAuth.signOut();
                }

            }
        });


    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        mCatListAdapter.stopListening();
        mNewlyArrivalAdapter.stopListening();
    }

    @Override
    public void finish() {
        super.finish();
    }

    @Override
    public void onBackPressed() {
        if (singleBack) {
            super.onBackPressed();
            return;
        }

        this.singleBack = true;
        Toast.makeText(this, "Press again to exit!", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                singleBack = false;
            }
        }, 2000);
    }

}


//------old data

//package com.psjewels.activity;
//
//import androidx.annotation.NonNull;
//        import androidx.annotation.Nullable;
//        import androidx.appcompat.app.AppCompatActivity;
//        import androidx.databinding.DataBindingUtil;
//        import androidx.recyclerview.widget.GridLayoutManager;
//        import androidx.recyclerview.widget.LinearLayoutManager;
//
//        import android.annotation.SuppressLint;
//        import android.app.Activity;
//        import android.app.Dialog;
//        import android.content.Context;
//        import android.content.Intent;
//        import android.net.ConnectivityManager;
//        import android.net.NetworkInfo;
//        import android.os.Bundle;
//        import android.os.Handler;
//        import android.provider.Settings;
//        import android.telephony.TelephonyManager;
//        import android.text.Editable;
//        import android.text.TextWatcher;
//        import android.util.Log;
//        import android.view.LayoutInflater;
//        import android.view.MotionEvent;
//        import android.view.View;
//        import android.view.Window;
//        import android.view.WindowManager;
//        import android.view.inputmethod.InputMethodManager;
//        import android.widget.ArrayAdapter;
//        import android.widget.Toast;
//
//        import com.firebase.ui.firestore.FirestoreRecyclerOptions;
//        import com.google.android.gms.tasks.OnCompleteListener;
//        import com.google.android.gms.tasks.Task;
//        import com.google.firebase.auth.FirebaseAuth;
//        import com.google.firebase.auth.FirebaseUser;
//        import com.google.firebase.firestore.CollectionReference;
//        import com.google.firebase.firestore.DocumentSnapshot;
//        import com.google.firebase.firestore.EventListener;
//        import com.google.firebase.firestore.FirebaseFirestore;
//        import com.google.firebase.firestore.FirebaseFirestoreException;
//        import com.google.firebase.firestore.Query;
//        import com.google.firebase.firestore.QueryDocumentSnapshot;
//        import com.google.firebase.firestore.QuerySnapshot;
//        import com.google.firebase.messaging.Constants;
//        import com.psjewels.R;
//        import com.psjewels.adapter.CatListAdapter;
//        import com.psjewels.adapter.CatWiseProductListAdapter;
//        import com.psjewels.adapter.NewlyArrivalAdapter;
//        import com.psjewels.databinding.ActivityDashboardBinding;
//        import com.psjewels.databinding.DialogBankDetailsBinding;
//        import com.psjewels.model.AddCategory;
//        import com.psjewels.model.AddUser;
//        import com.psjewels.model.ProductDeatils;
//        import com.psjewels.utils.Constant;
//        import com.psjewels.utils.SpacingItemDecoration;
//        import com.psjewels.utils.Tools;
//
//        import java.util.ArrayList;
//        import java.util.List;
//
//public class DashboardActivity extends AppCompatActivity {
//
//    ActivityDashboardBinding mActivityDashboardBinding;
//    CollectionReference mCollectionReferenceCatList;
//    CatListAdapter mCatListAdapter;
//    CollectionReference mCollectionReferenceUserList;
//
//    NewlyArrivalAdapter mNewlyArrivalAdapter;
//    LinearLayoutManager HorizontalLayout;
//    CollectionReference mCollectionReferenceProductListForNewArriaval;
//    boolean singleBack = false;
//
//    String IMEI_Number_Holder;
//    TelephonyManager telephonyManager;
//
//    List<DocumentSnapshot> mDocumentChangeList;
//    CatWiseProductListAdapter mCatWiseProductListAdapter;
//
//    List<String> mProductDetailsList;
//    ArrayAdapter<String> adapter;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
////        setContentView(R.layout.activity_dashboard);
//        binding();
//        checkConnection();
//        initDb();
//        getCategory();
//        clickEvent();
//        getNewlyArrival();
//        searchViewAllProducts();
//        getData();
//        bankDetails();
//        checkUserStatus();
//
//    }
//
//
//
//    private void checkUserStatus() {
//        FirebaseUser mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
//        if (mFirebaseUser != null) {
//            mCollectionReferenceUserList.document(mFirebaseUser.getUid()).addSnapshotListener(new EventListener<DocumentSnapshot>() {
//                @Override
//                public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
//                    if (documentSnapshot != null) {
//                        AddUser mAddUser = documentSnapshot.toObject(AddUser.class);
//                        String deviceIdCurrent = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
//
//                        if (mAddUser != null && !mAddUser.isStatus()) {
//
//                            Intent newIntent = new Intent(DashboardActivity.this, RegistrationTemporayScreenActivity.class);
//                            newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                            newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                            newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                            startActivity(newIntent);
//
//                        } else if (mAddUser != null && !mAddUser.getDeviceId().equals(deviceIdCurrent)) {
//                            FirebaseAuth.getInstance().signOut();
//                            Intent newIntent = new Intent(DashboardActivity.this, LoginScreenActivity.class);
//                            newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                            newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                            newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                            startActivity(newIntent);
//                        }
//
//                    }
//
//                }
//            });
//        }
//    }
//
//    private void bankDetails() {
//
//        mActivityDashboardBinding.iconBankDeatils.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                final DialogBankDetailsBinding mDialogBankDetailsBinding;
//                final Dialog dialog = new Dialog(DashboardActivity.this, R.style.MyAlertDialogTheme);
//                mDialogBankDetailsBinding = DataBindingUtil.inflate(LayoutInflater.from(DashboardActivity.this), R.layout.dialog_bank_details, null, false);
//                mDialogBankDetailsBinding.imgclose.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        dialog.dismiss();
//                    }
//                });
//                dialog.setContentView(mDialogBankDetailsBinding.getRoot());
//                dialog.show();
//
//            }
//        });
//    }
//
//    private void getData() {
//
//
////____________________Real_Time_Code__________________________________________________________________________________________________________________________________________________________________________________________________
//        if (!((Activity) this).isFinishing()) {
////            Toast.makeText(this, Constant.PROGRESS_DIALOG_MESSAGE, Toast.LENGTH_SHORT).show();
//        }
//        mDocumentChangeList = new ArrayList<>();
//
//
//        mCatWiseProductListAdapter = new CatWiseProductListAdapter(mDocumentChangeList, DashboardActivity.this);
//        mActivityDashboardBinding.productListRecyclerView.setLayoutManager(new GridLayoutManager(DashboardActivity.this, 2));
//        mActivityDashboardBinding.productListRecyclerView.setAdapter(mCatWiseProductListAdapter);
//
//        Query mQuery = FirebaseFirestore.getInstance().collection(Constant.PRODUCT_LIST_REF).orderBy(Constant.TIME_STAMP_KEY, Query.Direction.DESCENDING);
//
//        Query mQueryfinal = mQuery.orderBy(Constant.TIME_STAMP_KEY, Query.Direction.DESCENDING);
//        mQuery.addSnapshotListener(new EventListener<QuerySnapshot>() {
//            @Override
//            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
//                if (!((Activity) DashboardActivity.this).isFinishing()) {
////                    Toast.makeText(this, Constant.PROGRESS_DIALOG_MESSAGE, Toast.LENGTH_SHORT).show();
//                }
//
//
//                if (queryDocumentSnapshots != null) {
//                    ArrayList<DocumentSnapshot> mDocumentSnapshotsBookingListArray = new ArrayList<>();
////                    mMangeBookingBinding.txtTodaysAppoin.setText(String.valueOf(queryDocumentSnapshots.size()));
//                    mDocumentChangeList.clear();
//                    mDocumentChangeList.addAll(queryDocumentSnapshots.getDocuments());
//                    mCatWiseProductListAdapter.notifyDataSetChanged();
//                } else {
//                    Log.d(Constants.TAG, "onEvent: NULL");
//                }
//            }
//        });
//
//
//
//    }
//
//
//    @SuppressLint("ClickableViewAccessibility")
//    private void searchViewAllProducts() {
//        mActivityDashboardBinding.autoCompleteText.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                mActivityDashboardBinding.close.setVisibility(View.VISIBLE);
//                return false;
//            }
//        });
//
//
//        mActivityDashboardBinding.close.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                in.hideSoftInputFromWindow(v.getWindowToken(), 0);
//                mActivityDashboardBinding.autoCompleteText.setText(null);
//                mActivityDashboardBinding.productListRecyclerView.setVisibility(View.GONE);
//
////                getData();
//                onStart();
//                mActivityDashboardBinding.close.setVisibility(View.GONE);
//                mActivityDashboardBinding.allProductSearch.setVisibility(View.GONE);
//            }
//        });
//
//
//        mActivityDashboardBinding.autoCompleteText.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//                mActivityDashboardBinding.productListRecyclerView.setVisibility(View.VISIBLE);
//                filter(s.toString());
//
//            }
//        });
//
//
//
//    }
//
//    private void filter(String text) {
//
//
//
//
//        ArrayList<DocumentSnapshot> filteredList = new ArrayList<>();
//        for (DocumentSnapshot mSubCategoryDS : mDocumentChangeList) {
//            ProductDeatils mProductDeatils = mSubCategoryDS.toObject(ProductDeatils.class);
//            if ((mProductDeatils.getPro_code() + mProductDeatils.getPro_code_number() + mProductDeatils.getKeyword_list_name()).toLowerCase().contains(text.toLowerCase())) {
//                filteredList.add(mSubCategoryDS);
//            } else {
//                mActivityDashboardBinding.allProductSearch.setVisibility(View.VISIBLE);
//            }
//        }
//        mCatWiseProductListAdapter.filterList(filteredList);
//
//
//
//
//
//    }
//
//
//    private void getNewlyArrival() {
//
//        Query mQuery = mCollectionReferenceProductListForNewArriaval.limit(20);
//        FirestoreRecyclerOptions<ProductDeatils> mProductDetailsFirestoreRecyclerOptions = new FirestoreRecyclerOptions
//                .Builder<ProductDeatils>()
//                .setQuery(mQuery.orderBy(Constant.TIME_STAMP_KEY, Query.Direction.DESCENDING), ProductDeatils.class).build();
//        mNewlyArrivalAdapter = new NewlyArrivalAdapter(mProductDetailsFirestoreRecyclerOptions, this);
//
//        HorizontalLayout
//                = new LinearLayoutManager(
//                DashboardActivity.this,
//                LinearLayoutManager.HORIZONTAL,
//                false);
//
//        mActivityDashboardBinding.newlyArrival.setLayoutManager(HorizontalLayout);
//        mActivityDashboardBinding.newlyArrival.setLayoutManager(new GridLayoutManager(DashboardActivity.this, 2));
//        mActivityDashboardBinding.newlyArrival.addItemDecoration(new SpacingItemDecoration(2, Tools.dpToPx(DashboardActivity.this, 15), true));
//        mActivityDashboardBinding.newlyArrival.setAdapter(mNewlyArrivalAdapter);
//
//        //checkactivity
//        mQuery.addSnapshotListener(new EventListener<QuerySnapshot>() {
//            @Override
//            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
//                if (queryDocumentSnapshots != null) {
//                    if (queryDocumentSnapshots.size() > 0) {
//                        mActivityDashboardBinding.emptyView.setVisibility(View.GONE);
//                        mActivityDashboardBinding.newlyArrival.setVisibility(View.VISIBLE);
//                    } else {
//
//                        mActivityDashboardBinding.emptyView.setVisibility(View.VISIBLE);
//                        mActivityDashboardBinding.newlyArrival.setVisibility(View.GONE);
//                    }
//                } else {
//
//                    mActivityDashboardBinding.emptyView.setVisibility(View.VISIBLE);
//                    mActivityDashboardBinding.newlyArrival.setVisibility(View.GONE);
//                }
//            }
//        });
//
//
//    }
//
//    private void clickEvent() {
//        mActivityDashboardBinding.iconCart.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                startActivity(new Intent(getApplicationContext(), CartActivity.class));
//            }
//        });
//
//
//        mActivityDashboardBinding.orderDetails.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                startActivity(new Intent(getApplicationContext(), MyOrderActivity.class));
//            }
//        });
//
//
//        mActivityDashboardBinding.iconBankDeatils.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//            }
//        });
//    }
//
//    public boolean checkConnection() {
//
//        ConnectivityManager manager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
//        NetworkInfo activeNetwork = manager.getActiveNetworkInfo();
//
//        if (null != activeNetwork) {
//            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
////                Toast.makeText(this, "Wifi Enabled", Toast.LENGTH_SHORT).show();
//                return true;
//            }
//            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
////                Toast.makeText(this, "Data Network Enabled", Toast.LENGTH_SHORT).show();
//                return true;
//            }
//        } else {
//            Toast.makeText(this, "No Internet Connection", Toast.LENGTH_SHORT).show();
//            return false;
//        }
//        return false;
//    }
//
//    private void getCategory() {
//        FirestoreRecyclerOptions<AddCategory> mCategoryFirestoreRecyclerOptions = new FirestoreRecyclerOptions
//                .Builder<AddCategory>()
//                .setQuery(mCollectionReferenceCatList, AddCategory.class).build();
//
//        mCatListAdapter = new CatListAdapter(mCategoryFirestoreRecyclerOptions, DashboardActivity.this);
//        mActivityDashboardBinding.categoryList.setLayoutManager(new GridLayoutManager(DashboardActivity.this, 2));
//        mActivityDashboardBinding.categoryList.addItemDecoration(new SpacingItemDecoration(2, Tools.dpToPx(DashboardActivity.this, 4), true));
//        mActivityDashboardBinding.categoryList.setLayoutManager(new GridLayoutManager(DashboardActivity.this, 2));
//        mActivityDashboardBinding.categoryList.setAdapter(mCatListAdapter);
//    }
//
//    private void initDb() {
//        mCollectionReferenceCatList = FirebaseFirestore.getInstance().collection(Constant.CAT_LIST_REF);
////        mQueryProductListLastColle = FirebaseFirestore.getInstance().collection(Constant.PRODUCT_LIST_REF).orderBy(Constant.TIME_STAMP_KEY, Query.Direction.DESCENDING).limit(4);
//        mCollectionReferenceProductListForNewArriaval = FirebaseFirestore.getInstance().collection(Constant.PRODUCT_LIST_REF);
//        mCollectionReferenceUserList = FirebaseFirestore.getInstance().collection(Constant.USER_LIST_REF);
//
//    }
//
//    private void binding() {
//
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        mActivityDashboardBinding = DataBindingUtil.setContentView(this, R.layout.activity_dashboard);
//
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
//    }
//
//
//    @Override
//    protected void onStart() {
//        super.onStart();
//
//
//        mCatListAdapter.startListening();
//        mNewlyArrivalAdapter.startListening();
//    }
//
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        mCatListAdapter.stopListening();
//        mNewlyArrivalAdapter.stopListening();
//    }
//
//    @Override
//    public void finish() {
//        super.finish();
//    }
//
//    @Override
//    public void onBackPressed() {
//        if (singleBack) {
//            super.onBackPressed();
//            return;
//        }
//
//        this.singleBack = true;
//        Toast.makeText(this, "Press again to exit!", Toast.LENGTH_SHORT).show();
//
//        new Handler().postDelayed(new Runnable() {
//
//            @Override
//            public void run() {
//                singleBack = false;
//            }
//        }, 2000);
//    }
//
//}
//
//
//
//
//
//
//
//
