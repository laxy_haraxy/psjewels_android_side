package com.psjewels.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.psjewels.R;
import com.psjewels.adapter.ImagesFireBaseAdapter;
import com.psjewels.adapter.MultipleImageCollectDetailsAdapter;
import com.psjewels.databinding.ActivityCollectUserDetailsBinding;
import com.psjewels.model.AddUser;
import com.psjewels.model.ImageUrl;
import com.psjewels.utils.Constant;
import com.psjewels.utils.GpsTracker;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import id.zelory.compressor.Compressor;

public class CollectUserDetailsActivity extends AppCompatActivity {

    ActivityCollectUserDetailsBinding mActivityCollectUserDetailsBinding;


    FirebaseUser mFirebaseUser;
    ProgressDialog mProgressDialog;
    DocumentReference mDocumentReferenceImg, mDocumentReferenceUserDetails;
    CollectionReference mCollectionReferenceUser, mCollectionReferenceImg;

    private static final int PICK_IMAGE_REQUEST = 123;
    Uri filePath = null;
    Bitmap image_compress = null;
    String docId, userId;
    String tokenId;
    String bussinessCardFrontImage, bussinessCardBackImage;
    DateFormat dfDate;
    String date;
    String companyName, contactPerson, phoneNo, address, state, city;
    long currentTimeDate;
    String deviceId;
    List<Uri> mListImgList;
    List<Bitmap> mImgListCompress;
    long currentTimeMills;
    String imageUrl;

    RecyclerView.LayoutManager mLayoutManager;

    ImagesFireBaseAdapter mImagesFireBaseAdapter;


    ArrayList<String> listSpinner = new ArrayList<String>();
    ArrayList<String> listAll = new ArrayList<String>();
    ArrayList<String> listState = new ArrayList<String>();
    ArrayList<String> listCity = new ArrayList<String>();
    AutoCompleteTextView act;


    String Latitude;
    String Longitude;

    private GpsTracker gpsTracker;


    @SuppressLint("MissingPermission")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_collect_user_details);
        binding();
        initDb();
        checkConnection();
        getImages();
        initializeArray();
        insertImagesInDB();
        clickEvent();
        getData();
        retrieveImages();
        imgMorethanTwoThanGonePlusIcon();


//        obj_list();
//        addToSpinner();


        callAll();


    }


    public void callAll() {
        obj_list();
        addCity();
        addState();
    }

    public String getJson() {
        String json = null;
        try {
            InputStream is = getAssets().open("list_state_city.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return json;
        }
        return json;
    }

    void obj_list() {
        try {
            JSONObject jsonObject = new JSONObject(getJson());
            JSONArray array = jsonObject.getJSONArray("array");
            for (int i = 0; i < array.length(); i++) {
                JSONObject object = array.getJSONObject(i);
                String city = object.getString("name");
                String state = object.getString("state");
                listSpinner.add(String.valueOf(i + 1) + " : " + city + " , " + state);
                listAll.add(city + " , " + state);
                listCity.add(city);
                listState.add(state);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    void addCity() {
        act = (AutoCompleteTextView) findViewById(R.id.actCity);
        adapterSetting(listCity);
    }

    void addState() {
        Set<String> set = new HashSet<String>(listState);
        act = (AutoCompleteTextView) findViewById(R.id.actState);
        adapterSetting(new ArrayList(set));
    }

    void adapterSetting(ArrayList arrayList) {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, arrayList);
        act.setAdapter(adapter);
        hideKeyBoard();
    }

    public void hideKeyBoard() {
        act.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
            }
        });
    }


//    //--------------------------------------------------
//
//    // Get the content of cities.json from assets directory and store it as string
//    public String getJson() {
//        String json = null;
//        try {
//            // Opening cities.json file
//            InputStream is = getAssets().open("list_state_city.json");
//            // is there any content in the file
//            int size = is.available();
//            byte[] buffer = new byte[size];
//            is.read(buffer);
//            is.close();
//            json = new String(buffer, "UTF-8");
//        } catch (IOException ex) {
//            ex.printStackTrace();
//            return json;
//        }
//        return json;
//    }
//
//    // This add all JSON object's data to the respective lists
//    void obj_list() {
//        // Exceptions are returned by JSONObject when the object cannot be created
//        try {
//            // Convert the string returned to a JSON object
//            JSONObject jsonObject = new JSONObject(getJson());
//            // Get Json array
//            JSONArray array = jsonObject.getJSONArray("array");
//            // Navigate through an array item one by one
//            for (int i = 0; i < array.length(); i++) {
//                // select the particular JSON data
//                JSONObject object = array.getJSONObject(i);
//                String city = object.getString("name");
//                String state = object.getString("state");
//                // add to the lists in the specified format
//                listSpinner.add(String.valueOf(i + 1) + " : " + city + " , " + state);
//                listAll.add(city + " , " + state);
//                listCity.add(city);
//                listState.add(state);
//            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//    }
//
//    // Add the data items to the spinner
//    void addToSpinner() {
//        final Spinner spinner = (Spinner) findViewById(R.id.state);
//        // Adapter for spinner
//
//
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, listState);
//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spinner.setAdapter(adapter);
//
//        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> arg0, View view, int arg2, long arg3) {
//
//                seletedState = spinner.getSelectedItem().toString();
//
//                Toast.makeText(getApplicationContext(), seletedState,
//                        Toast.LENGTH_SHORT).show();
//
//
//
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> arg0) {
//                // TODO Auto-generated method stub
//
//            }
//        });
//
//
//
//        Spinner spinner2 = (Spinner) findViewById(R.id.city);
//        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, queryCityList);
//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spinner2.setAdapter(adapter2);
//    }
//    //--------------------------------------------------


    private void imgMorethanTwoThanGonePlusIcon() {
        if (checkConnection()) {

            mCollectionReferenceImg = FirebaseFirestore.getInstance().collection(Constant.USER_LIST_REF).document(mFirebaseUser.getUid()).collection(Constant.IMG_COLLECTION_CARDS);

            mCollectionReferenceImg.addSnapshotListener(new EventListener<QuerySnapshot>() {
                @Override
                public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                    if (queryDocumentSnapshots != null) {
                        int count = queryDocumentSnapshots.size();
//                            Toast.makeText(ProductListActivity.this, "count: " + count, Toast.LENGTH_SHORT).show();
                        String counts = String.valueOf(count);
                        counts.toString();
//                        Toast.makeText(CollectUserDetailsActivity.this, "asd :- " + count, Toast.LENGTH_SHORT).show();

                        if (count == 2 || count > 3) {
                            mActivityCollectUserDetailsBinding.selectImg.setVisibility(View.GONE);
                            mActivityCollectUserDetailsBinding.recycleView.setPadding(30, 0, 0, 0);
                        } else {
                            mActivityCollectUserDetailsBinding.selectImg.setVisibility(View.VISIBLE);

                        }


                        if (count == 2) {
                            mActivityCollectUserDetailsBinding.btnRegister.setVisibility(View.VISIBLE);
                        }


                    }
                }
            });
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        mImagesFireBaseAdapter.startListening();


    }

    @Override
    protected void onStop() {
        super.onStop();
        mImagesFireBaseAdapter.stopListening();
    }


    private void retrieveImages() {


//        mLayoutManager = new GridLayoutManager(this,3);

//        mLayoutManager = new GridLayoutManager(this,1);

        mLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.HORIZONTAL);

        mActivityCollectUserDetailsBinding.recycleView.setLayoutManager(mLayoutManager);
        mProgressDialog.setMessage(Constant.PROGRESS_DIALOG_MESSAGE);
        mProgressDialog.show();
        mCollectionReferenceImg = FirebaseFirestore.getInstance().collection(Constant.USER_LIST_REF).document(mFirebaseUser.getUid()).collection(Constant.IMG_COLLECTION_CARDS);

        FirestoreRecyclerOptions<ImageUrl> mFirebaseRecyclerOptionsImgList = new FirestoreRecyclerOptions
                .Builder<ImageUrl>()
                .setQuery(mCollectionReferenceImg, ImageUrl.class).build();
        mImagesFireBaseAdapter = new ImagesFireBaseAdapter(mFirebaseRecyclerOptionsImgList,
                this);
        mProgressDialog.dismiss();

        mActivityCollectUserDetailsBinding.recycleView.setAdapter(mImagesFireBaseAdapter);
    }

    private void binding() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mActivityCollectUserDetailsBinding = DataBindingUtil.setContentView(this, R.layout.activity_collect_user_details);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);


    }

    private void initDb() {
        mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        mDocumentReferenceImg = FirebaseFirestore.getInstance().collection(Constant.USER_LIST_REF).document(mFirebaseUser.getUid()).collection(Constant.IMG_COLLECTION_CARDS).document();
        mDocumentReferenceUserDetails = FirebaseFirestore.getInstance().collection(Constant.USER_LIST_REF).document(mFirebaseUser.getUid());
        mProgressDialog = new ProgressDialog(this, R.style.ProgressDialogTheme);
        mCollectionReferenceImg = FirebaseFirestore.getInstance().collection(Constant.USER_LIST_REF).document(mFirebaseUser.getUid()).collection(Constant.IMG_COLLECTION_CARDS);
        mCollectionReferenceUser = FirebaseFirestore.getInstance().collection(Constant.USER_LIST_REF).document(mFirebaseUser.getUid()).collection(Constant.USER_DETAILS);
    }

    public boolean checkConnection() {

        ConnectivityManager manager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = manager.getActiveNetworkInfo();

        if (null != activeNetwork) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
//                Toast.makeText(this, "Wifi Enabled", Toast.LENGTH_SHORT).show();
                return true;
            }
            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
//                Toast.makeText(this, "Data Network Enabled", Toast.LENGTH_SHORT).show();
                return true;
            }
        } else {
            Toast.makeText(this, "No Internet Connection", Toast.LENGTH_SHORT).show();
            return false;
        }
        return false;
    }


    private void getImages() {

        mActivityCollectUserDetailsBinding.selectImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!checkConnection()) {
//                    Toast.makeText(AddBannerActivity.this, "Check Your Internet", Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);

                }
            }
        });
    }


    private void initializeArray() {
        mListImgList = new ArrayList<>();
        mImgListCompress = new ArrayList<>();
    }

    private void insertImagesInDB() {
        mActivityCollectUserDetailsBinding.btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //calculate imges
                calculateSelectedImages();
            }
        });
    }

    private void calculateSelectedImages() {
        if (filePath != null) {
            for (int i = 0; i < mImgListCompress.size(); i++) {
                Bitmap compressimg = mImgListCompress.get(i);
                uploadImages(compressimg, i);
            }
        } else {
            Toast.makeText(this, "Select Image", Toast.LENGTH_SHORT).show();
        }
    }


    //store image into onactiviyresult
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();

            try {
                /*
                 * create a bitmap to store image
                 * */
                CropImage.activity(filePath).setGuidelines(CropImageView.Guidelines.ON).start(this);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        /*
         * Display result of crop image
         * */
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);


            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                mListImgList.add(resultUri);
                MultipleImageCollectDetailsAdapter multipleImageAdapter = new MultipleImageCollectDetailsAdapter(mListImgList, this);
                mActivityCollectUserDetailsBinding.recycleView.setAdapter(multipleImageAdapter);


//changes to check
                mActivityCollectUserDetailsBinding.recycleView.setLayoutManager(new GridLayoutManager(this, 3));
                /*
                 * Store image uri into File so we can compress file in Compressor class
                 * */
                File filecompressor = new File(Objects.requireNonNull(resultUri.getPath()));
                try {
                    image_compress = new Compressor(this).setQuality(80).compressToBitmap(filecompressor);
                    mImgListCompress.add(image_compress);

                } catch (Exception e) {
                    e.getMessage();
                }
            }
        }
    }


    private void getData() {
        mActivityCollectUserDetailsBinding.edtPhoneNo.setEnabled(false);
        FirebaseAuth mAuth = FirebaseAuth.getInstance();

        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser != null) {
            mActivityCollectUserDetailsBinding.edtPhoneNo.setText(currentUser.getPhoneNumber());
        }

        mDocumentReferenceUserDetails.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                if (documentSnapshot != null) {
                    AddUser mAddUser = documentSnapshot.toObject(AddUser.class);
                    if (mAddUser != null) {
                        mActivityCollectUserDetailsBinding.edtCompanyName.setText(mAddUser.getCompany_name());
                        mActivityCollectUserDetailsBinding.edtContactPerson.setText(mAddUser.getContact_person());

                        mActivityCollectUserDetailsBinding.edtAddress.setText(mAddUser.getAddress());
                        mActivityCollectUserDetailsBinding.actState.setText(mAddUser.getState());
                        mActivityCollectUserDetailsBinding.actCity.setText(mAddUser.getCity());


//                        Picasso.get().load(mAddUser.getBussinessCardFrontImage()).fit().centerInside().into(mActivityCollectUserDetailsBinding.selectImg);


                    }
                }
            }
        });
    }


    private void clickEvent() {


        mActivityCollectUserDetailsBinding.btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkConnection()) {


                    companyName = mActivityCollectUserDetailsBinding.edtCompanyName.getText().toString();
                    contactPerson = mActivityCollectUserDetailsBinding.edtContactPerson.getText().toString();
                    phoneNo = mActivityCollectUserDetailsBinding.edtPhoneNo.getText().toString();
                    address = mActivityCollectUserDetailsBinding.edtAddress.getText().toString();
                    state = mActivityCollectUserDetailsBinding.actState.getText().toString();
                    city = mActivityCollectUserDetailsBinding.actCity.getText().toString();

                    if (TextUtils.isEmpty(companyName)) {
                        Toast.makeText(CollectUserDetailsActivity.this, "Enter company name", Toast.LENGTH_SHORT).show();
                    } else if (TextUtils.isEmpty(contactPerson)) {
                        Toast.makeText(CollectUserDetailsActivity.this, "Enter contact person", Toast.LENGTH_SHORT).show();
                    } else if (TextUtils.isEmpty(phoneNo)) {
                        Toast.makeText(CollectUserDetailsActivity.this, "Enter phone number", Toast.LENGTH_SHORT).show();
                    } else if (TextUtils.isEmpty(address)) {
                        Toast.makeText(CollectUserDetailsActivity.this, "Enter address", Toast.LENGTH_SHORT).show();
                    } else if (TextUtils.isEmpty(state)) {
                        Toast.makeText(CollectUserDetailsActivity.this, "Enter state", Toast.LENGTH_SHORT).show();
                    } else if (TextUtils.isEmpty(city)) {
                        Toast.makeText(CollectUserDetailsActivity.this, "Enter City", Toast.LENGTH_SHORT).show();
                    } else {
                        mProgressDialog.setMessage(Constant.PROGRESS_DIALOG_MESSAGE);
                        mProgressDialog.show();

                        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                            @Override
                            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                                if (!task.isSuccessful()) {
                                    mProgressDialog.dismiss();

                                    Toast.makeText(CollectUserDetailsActivity.this, R.string.something_wrong, Toast.LENGTH_SHORT).show();
                                } else {


                                    SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                                    SharedPreferences.Editor editor = pref.edit();
                                    editor.putString("edt_company_name", companyName);
                                    editor.commit(); // commit changes


                                    tokenId = Objects.requireNonNull(task.getResult()).getToken();

                                    userId = mFirebaseUser.getUid();

                                    dfDate = new SimpleDateFormat("dd-MM-yyyy");
                                    date = dfDate.format(Calendar.getInstance().getTime());


                                    if (filePath != null) {
                                        for (int i = 0; i < mImgListCompress.size(); i++) {
                                            Bitmap compressimg = mImgListCompress.get(i);
                                            uploadImages(compressimg, i);
                                        }
                                    } else {
//                                        Toast.makeText(CollectUserDetailsActivity.this, "Please select only 2 images", Toast.LENGTH_SHORT).show();
//                                        mProgressDialog.dismiss();


                                    }


                                    deviceId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);


                                    currentTimeDate = System.currentTimeMillis();

//                                    updateData();


//--------------------------------------------------------------------------------------------------


                                    SharedPreferences prefLocation = getApplicationContext().getSharedPreferences("prefLocation", 0); // 0 - for private mode
//                                    SharedPreferences.Editor editorprefLocation = prefLocation.edit();
//
                                    Latitude = prefLocation.getString("FinalLatitude", null);
                                    Longitude = prefLocation.getString("FinalLongitude", null);
//                                    Toast.makeText(getApplicationContext(), "FinalLatitude colle"+Latitude, Toast.LENGTH_SHORT).show();
//                                    Toast.makeText(getApplicationContext(), "FinalLongitude colle"+Longitude, Toast.LENGTH_SHORT).show();


                                    AddUser mAddUser = new AddUser(tokenId, userId,
                                            companyName, contactPerson, phoneNo,
                                            address, state, city, deviceId, date,
                                            false, currentTimeDate, Latitude, Longitude);

                                    mDocumentReferenceUserDetails.set(mAddUser).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            try {
                                                mProgressDialog.dismiss();

                                            } catch (Exception e) {
                                                mProgressDialog.dismiss();

                                            }

                                            startActivity(new Intent(CollectUserDetailsActivity.this, RegistrationTemporayScreenActivity.class));
                                            finish();
                                        }
                                    }).addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            mProgressDialog.dismiss();
                                            Toast.makeText(CollectUserDetailsActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                                        }
                                    });
//--------------------------------------------------------------------------------------------------


                                }
                            }

                        });

                    }


                }
            }
        });
    }


    private void uploadImages(Bitmap compressimg, final int i) {
        final ProgressDialog progressDialog = new ProgressDialog(this, R.style.ProgressDialogTheme);
        progressDialog.setMessage("Please Wait..");
        progressDialog.show();

        /*
         * ByteArrayOutputStream is use to store outputstream(image output)
         * and convent into byte array beacuse we want to compress image
         * */
        if (image_compress != null) {

            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            compressimg.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream);
            final byte[] thumb_byte = byteArrayOutputStream.toByteArray();
            /*
             * upload image to storagereferance bucket
             *
             * */
            StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(Constant.VISITING_CARD);
            currentTimeMills = System.currentTimeMillis();
            final StorageReference storageReference_uploadimage = storageReference.child("Cards :" + currentTimeMills);

            UploadTask uploadTask = storageReference_uploadimage.putBytes(thumb_byte);
            uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if (!task.isSuccessful()) {
                        throw Objects.requireNonNull(task.getException());
                    }
                    return storageReference_uploadimage.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful()) {
                        Uri downloadUri = task.getResult();
                        /*
                         * Store data to firebase local database
                         *
                         * */
                        if (downloadUri != null) {
                            imageUrl = downloadUri.toString();
                        }


                        ImageUrl mUrl = new ImageUrl(imageUrl);


                        mCollectionReferenceImg.add(mUrl).addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                            @Override
                            public void onComplete(@NonNull Task<DocumentReference> task) {

//                                Toast.makeText(CollectUserDetailsActivity.this, "Image Successfully Uploaded", Toast.LENGTH_SHORT).show();
                                finish();

                            }
                        });


                    }
                }
            });
        } else {
            Toast.makeText(this, "error!!", Toast.LENGTH_SHORT).show();
        }
    }

    public void yourDesiredMethod() {
        mActivityCollectUserDetailsBinding.recycleView.setPadding(30, 0, 0, 0);
        mActivityCollectUserDetailsBinding.selectImg.setVisibility(View.GONE);
        mActivityCollectUserDetailsBinding.btnRegister.setVisibility(View.VISIBLE);
    }


//        /*
//         * ByteArrayOutputStream is use to store outputstream(image output)
//         * and convent into byte array beacuse we want to compress image
//         * */
//        if (compressimg != null) {
//            Log.d(Constant.TAG, "uploadImages: " + image_compress);
//            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//            compressimg.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream);
//            final byte[] thumb_byte = byteArrayOutputStream.toByteArray();
//            /*
//             * upload image to storagereferance bucket
//             *
//             * */
//
//            String imgType = "";
//            String mainPath = "";
//
////            String userId = mBeauticianDetails.getId();
//            if (i == 0) {
//                imgType = "";
//                mainPath = "front_img";
//            }
//
//            if (i == 1) {
//
//                imgType = "id_proof";
//                mainPath = "back_img";
//
//            }
//
//            if (i == 2) {
//
//                imgType = "address_proof_1";
//                mainPath = "sign_bank_id_proof";
//
//            }
//
//
//            storageReference = FirebaseStorage.getInstance().getReference().child(mainPath);
//            final StorageReference storageReference_uploadimage = storageReference.child(this.userId + " " + imgType);
//
//            mDocumentReferenceImg = FirebaseFirestore.getInstance().collection(Constant.USER_LIST_REF).document(mFirebaseUser.getUid()).collection(Constant.IMG_COLLECTION_CARDS).document();
//            UploadTask uploadTask = storageReference_uploadimage.putBytes(thumb_byte);
//            uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
//                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
//                @Override
//                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
//                    if (!task.isSuccessful()) {
//                        throw Objects.requireNonNull(task.getException());
//                    }
//                    return storageReference_uploadimage.getDownloadUrl();
//                }
//            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
//                @Override
//                public void onComplete(@NonNull Task<Uri> task) {
//                    if (task.isSuccessful()) {
//
//                        Uri downloadUri = task.getResult();
//                        /*
//                         * Store data to firebase local database
//                         *
//                         * */
//                        if (downloadUri != null) {
//
//                            if (i == 0) {
//                                String imageUrl = downloadUri.toString();
//
//
//                                Log.d(Constant.TAG, "onComplete: front: " + imageUrl);
//                                mDocumentReferenceImg.update(Constant.FRONT_IMG, imageUrl).addOnCompleteListener(new OnCompleteListener<Void>() {
//                                    @Override
//                                    public void onComplete(@NonNull Task<Void> task) {
//
//                                    }
//                                });
//
//                            }
//
//                            if (i == 1) {
//                                String imageUrl = downloadUri.toString();
//
//                                Map<String, Object> map = new HashMap<>();
//
//
//                                Log.d(Constant.TAG, "onComplete: back: " + imageUrl);
//
//                                map.put(Constant.BACK_IMG, imageUrl);
//
//                                mDocumentReferenceImg.set(map).addOnCompleteListener(new OnCompleteListener<Void>() {
//                                    @Override
//                                    public void onComplete(@NonNull Task<Void> task) {
//
//                                    }
//                                }).addOnFailureListener(new OnFailureListener() {
//                                    @Override
//                                    public void onFailure(@NonNull Exception e) {
//                                        Log.d(Constant.TAG, "onFailure: address1 " + e.getLocalizedMessage());
//                                        Toast.makeText(CollectUserDetailsActivity.this, "Error: " + e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
//                                    }
//                                });
//                            }
//
//
//
//
//                        }
//
//                    }
//                }
//            }).addOnFailureListener(new OnFailureListener() {
//                @Override
//                public void onFailure(@NonNull Exception e) {
//                    Log.d(Constant.TAG, "onFailure: " + e.getLocalizedMessage());
//                    mProgressDialog.dismiss();
//                    Toast.makeText(CollectUserDetailsActivity.this, "Error: " + e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
//                }
//            });
//        } else {
//            mProgressDialog.dismiss();
//            Toast.makeText(CollectUserDetailsActivity.this, "error!!", Toast.LENGTH_SHORT).show();
//        }


}


//package com.psjewels.activity;
//
//import androidx.annotation.NonNull;
//import androidx.annotation.Nullable;
//import androidx.annotation.RequiresApi;
//import androidx.appcompat.app.AppCompatActivity;
//import androidx.core.app.ActivityCompat;
//import androidx.databinding.DataBindingUtil;
//import androidx.recyclerview.widget.GridLayoutManager;
//import androidx.recyclerview.widget.RecyclerView;
//import androidx.recyclerview.widget.StaggeredGridLayoutManager;
//
//import android.Manifest;
//import android.app.ProgressDialog;
//import android.content.ClipData;
//import android.content.Context;
//import android.content.Intent;
//import android.content.pm.PackageManager;
//import android.graphics.Bitmap;
//import android.graphics.BitmapFactory;
//import android.net.ConnectivityManager;
//import android.net.NetworkInfo;
//import android.net.Uri;
//import android.os.Build;
//import android.os.Bundle;
//import android.provider.MediaStore;
//import android.text.TextUtils;
//import android.util.Log;
//import android.view.View;
//import android.view.Window;
//import android.view.WindowManager;
//import android.widget.ImageView;
//import android.widget.Toast;
//
//import com.firebase.ui.firestore.FirestoreRecyclerOptions;
//import com.google.android.gms.tasks.Continuation;
//import com.google.android.gms.tasks.OnCompleteListener;
//import com.google.android.gms.tasks.OnFailureListener;
//import com.google.android.gms.tasks.OnSuccessListener;
//import com.google.android.gms.tasks.Task;
//import com.google.firebase.auth.FirebaseAuth;
//import com.google.firebase.auth.FirebaseUser;
//import com.google.firebase.firestore.CollectionReference;
//import com.google.firebase.firestore.DocumentReference;
//import com.google.firebase.firestore.DocumentSnapshot;
//import com.google.firebase.firestore.EventListener;
//import com.google.firebase.firestore.FirebaseFirestore;
//import com.google.firebase.firestore.FirebaseFirestoreException;
//import com.google.firebase.firestore.QueryDocumentSnapshot;
//import com.google.firebase.firestore.QuerySnapshot;
//import com.google.firebase.firestore.ThrowOnExtraProperties;
//import com.google.firebase.iid.FirebaseInstanceId;
//import com.google.firebase.iid.InstanceIdResult;
//import com.google.firebase.storage.FirebaseStorage;
//import com.google.firebase.storage.StorageReference;
//import com.google.firebase.storage.UploadTask;
//import com.psjewels.R;
//import com.psjewels.adapter.ImagesFireBaseAdapter;
//import com.psjewels.adapter.MultipleImageCollectDetailsAdapter;
//import com.psjewels.databinding.ActivityCollectUserDetailsBinding;
//import com.psjewels.model.AddUser;
//import com.psjewels.model.ImageUrl;
//import com.psjewels.utils.Constant;
//import com.squareup.picasso.Picasso;
//import com.theartofdev.edmodo.cropper.CropImage;
//import com.theartofdev.edmodo.cropper.CropImageView;
//
//import java.io.ByteArrayOutputStream;
//import java.io.File;
//import java.io.FileNotFoundException;
//import java.io.InputStream;
//import java.text.DateFormat;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.List;
//import java.util.Objects;
//
//import id.zelory.compressor.Compressor;
//
//public class CollectUserDetailsActivity extends AppCompatActivity {
//
//    ActivityCollectUserDetailsBinding mActivityCollectUserDetailsBinding;
//
//
//    FirebaseUser mFirebaseUser;
//    ProgressDialog mProgressDialog;
//    DocumentReference mDocumentReferenceImg, mDocumentReferenceUserDetails;
//    CollectionReference mCollectionReferenceUser, mCollectionReferenceImg;
//
//    private static final int PICK_IMAGE_REQUEST = 123;
//    Uri filePath = null;
//    Bitmap image_compress = null;
//    String docId, userId;
//    String tokenId;
//    String bussinessCardFrontImage, bussinessCardBackImage;
//    DateFormat dfDate;
//    String date;
//    String companyName, contactPerson, phoneNo, address, state, city;
//
//
//    List<Uri> mListImgList;
//    List<Bitmap> mImgListCompress;
//    long currentTimeMills;
//    String imageUrl;
//
//    RecyclerView.LayoutManager mLayoutManager;
//
//    ImagesFireBaseAdapter mImagesFireBaseAdapter;
//
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
////        setContentView(R.layout.activity_collect_user_details);
//        binding();
//        initDb();
//        checkConnection();
//
//
//        getImages();
//        initializeArray();
//        insertImagesInDB();
//
//
//        clickEvent();
//
//        getData();
//
//        retrieveImages();
//
//
////        ActivityCompat.requestPermissions(CollectUserDetailsActivity.this, new String[]{Manifest.permission.READ_PHONE_STATE}, PackageManager.PERMISSION_GRANTED);
////
////        mActivityCollectUserDetailsBinding.logo.setOnClickListener(new View.OnClickListener() {
////            @RequiresApi(api = Build.VERSION_CODES.O)
////            @Override
////            public void onClick(View v) {
////
////                TelephonyManager telephonyManager = (TelephonyManager) CollectUserDetailsActivity.this.getSystemService(Context.TELEPHONY_SERVICE);
////
////
////                if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
////                    // TODO: Consider calling
////                    //    ActivityCompat#requestPermissions
////                    // here to request the missing permissions, and then overriding
////                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
////                    //                                          int[] grantResults)
////                    // to handle the case where the user grants the permission. See the documentation
////                    // for ActivityCompat#requestPermissions for more details.
////                    return;
////                }
////                String stringIMEI = telephonyManager.getImei();
////
////                Log.d(Constant.TAG, "asdasd :- " + stringIMEI);
////
////            }
////        });
//    }
//
//
//    private void retrieveImages() {
//
//
////        mLayoutManager = new GridLayoutManager(this,3);
//
////        mLayoutManager = new GridLayoutManager(this,1);
//
//        mLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
//
//        mActivityCollectUserDetailsBinding.recycleView.setLayoutManager(mLayoutManager);
//        mProgressDialog.setMessage(Constant.PROGRESS_DIALOG_MESSAGE);
//        mProgressDialog.show();
//        mProgressDialog.setCancelable(false);
//        mCollectionReferenceImg = FirebaseFirestore.getInstance().collection(Constant.USER_LIST_REF).document(mFirebaseUser.getUid()).collection(Constant.IMG_COLLECTION_CARDS);
//
//        FirestoreRecyclerOptions<ImageUrl> mFirebaseRecyclerOptionsImgList = new FirestoreRecyclerOptions
//                .Builder<ImageUrl>()
//                .setQuery(mCollectionReferenceImg, ImageUrl.class).build();
//        mImagesFireBaseAdapter = new ImagesFireBaseAdapter(mFirebaseRecyclerOptionsImgList,
//                this);
//        mProgressDialog.dismiss();
//
//        mActivityCollectUserDetailsBinding.recycleView.setAdapter(mImagesFireBaseAdapter);
//    }
//
//    private void binding() {
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        mActivityCollectUserDetailsBinding = DataBindingUtil.setContentView(this, R.layout.activity_collect_user_details);
//    }
//
//    private void initDb() {
//        mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
//        Toast.makeText(CollectUserDetailsActivity.this, "getUid ; -" + mFirebaseUser.getUid(), Toast.LENGTH_SHORT).show();
//
//        mDocumentReferenceImg = FirebaseFirestore.getInstance().collection(Constant.USER_LIST_REF).document(mFirebaseUser.getUid()).collection(Constant.IMG_COLLECTION_CARDS).document();
//
//
//        mDocumentReferenceUserDetails = FirebaseFirestore.getInstance().collection(Constant.USER_LIST_REF).document(mFirebaseUser.getUid());
//        mProgressDialog = new ProgressDialog(this);
////        mCollectionReferenceUser = FirebaseFirestore.getInstance().collection(Constant.USER_LIST_REF);
//        mCollectionReferenceImg = FirebaseFirestore.getInstance().collection(Constant.USER_LIST_REF).document(mFirebaseUser.getUid()).collection(Constant.IMG_COLLECTION_CARDS);
//
//        mCollectionReferenceUser = FirebaseFirestore.getInstance().collection(Constant.USER_LIST_REF).document(mFirebaseUser.getUid()).collection(Constant.USER_DETAILS);
//
//    }
//
//    public boolean checkConnection() {
//
//        ConnectivityManager manager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
//        NetworkInfo activeNetwork = manager.getActiveNetworkInfo();
//
//        if (null != activeNetwork) {
//            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
////                Toast.makeText(this, "Wifi Enabled", Toast.LENGTH_SHORT).show();
//                return true;
//            }
//            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
////                Toast.makeText(this, "Data Network Enabled", Toast.LENGTH_SHORT).show();
//                return true;
//            }
//        } else {
//            Toast.makeText(this, "No Internet Connection", Toast.LENGTH_SHORT).show();
//            return false;
//        }
//        return false;
//    }
//
//
//    private void getImages() {
//
//        mActivityCollectUserDetailsBinding.selectImg.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                if (!checkConnection()) {
////                    Toast.makeText(AddBannerActivity.this, "Check Your Internet", Toast.LENGTH_SHORT).show();
//                } else {
//                    Intent intent = new Intent();
//                    intent.setType("image/*");
//                    intent.setAction(Intent.ACTION_GET_CONTENT);
//                    startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
//                }
//            }
//        });
//    }
//
//
//    private void initializeArray() {
//        mListImgList = new ArrayList<>();
//        mImgListCompress = new ArrayList<>();
//    }
//
//    private void insertImagesInDB() {
//        mActivityCollectUserDetailsBinding.btnRegister.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                //calculate imges
//                calculateSelectedImages();
//            }
//        });
//    }
//
//    private void calculateSelectedImages() {
////        if (filePath != null) {
////            for (int i = 0; i < mImgListCompress.size(); i++) {
////                Bitmap compressimg = mImgListCompress.get(i);
////                uploadImages(compressimg, i);
////            }
////        } else {
////            Toast.makeText(this, "Select Image", Toast.LENGTH_SHORT).show();
////        }
//    }
//
//
//    //store image into onactiviyresult
//    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
//            filePath = data.getData();
//
//            try {
//                /*
//                 * create a bitmap to store image
//                 * */
//                CropImage.activity(filePath).setGuidelines(CropImageView.Guidelines.ON).start(this);
//
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//        /*
//         * Display result of crop image
//         * */
//        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
//            CropImage.ActivityResult result = CropImage.getActivityResult(data);
//
//
//            if (resultCode == RESULT_OK) {
//                Uri resultUri = result.getUri();
//                mListImgList.add(resultUri);
//                MultipleImageCollectDetailsAdapter multipleImageAdapter = new MultipleImageCollectDetailsAdapter(mListImgList, CollectUserDetailsActivity.this);
//                mActivityCollectUserDetailsBinding.recycleView.setAdapter(multipleImageAdapter);
//
////changes to check
//                mActivityCollectUserDetailsBinding.recycleView.setLayoutManager(new GridLayoutManager(this, 3));
//                /*
//                 * Store image uri into File so we can compress file in Compressor class
//                 * */
//                File filecompressor = new File(Objects.requireNonNull(resultUri.getPath()));
//                try {
//                    image_compress = new Compressor(this).setQuality(80).compressToBitmap(filecompressor);
//                    mImgListCompress.add(image_compress);
//                } catch (Exception e) {
//                    e.getMessage();
//                }
//            }
//        }
//    }
//
//
//    private void getData() {
//
//
//        mDocumentReferenceUserDetails.addSnapshotListener(new EventListener<DocumentSnapshot>() {
//            @Override
//            public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
//                if (documentSnapshot != null) {
//                    AddUser mAddUser = documentSnapshot.toObject(AddUser.class);
//                    if (mAddUser != null) {
//                        mActivityCollectUserDetailsBinding.edtCompanyName.setText(mAddUser.getCompany_name());
//                        mActivityCollectUserDetailsBinding.edtContactPerson.setText(mAddUser.getContact_person());
//                        mActivityCollectUserDetailsBinding.edtPhoneNo.setText(mAddUser.getPhone_number());
//                        mActivityCollectUserDetailsBinding.edtAddress.setText(mAddUser.getAddress());
//                        mActivityCollectUserDetailsBinding.edtState.setText(mAddUser.getState());
//                        mActivityCollectUserDetailsBinding.edtCity.setText(mAddUser.getCity());
//
//
////                        Picasso.get().load(mAddUser.getBussinessCardFrontImage()).fit().centerInside().into(mActivityCollectUserDetailsBinding.selectImg);
//
//
//                    }
//                }
//            }
//        });
//    }
//
//
//    private void clickEvent() {
//
//
//        mActivityCollectUserDetailsBinding.btnRegister.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (checkConnection()) {
//
//
//                    companyName = mActivityCollectUserDetailsBinding.edtCompanyName.getText().toString();
//                    contactPerson = mActivityCollectUserDetailsBinding.edtContactPerson.getText().toString();
//                    phoneNo = mActivityCollectUserDetailsBinding.edtPhoneNo.getText().toString();
//                    address = mActivityCollectUserDetailsBinding.edtAddress.getText().toString();
//                    state = mActivityCollectUserDetailsBinding.edtState.getText().toString();
//                    city = mActivityCollectUserDetailsBinding.edtCity.getText().toString();
//
//                    if (TextUtils.isEmpty(companyName)) {
//                        Toast.makeText(CollectUserDetailsActivity.this, "Enter company name", Toast.LENGTH_SHORT).show();
//                    } else if (TextUtils.isEmpty(contactPerson)) {
//                        Toast.makeText(CollectUserDetailsActivity.this, "Enter contact person", Toast.LENGTH_SHORT).show();
//                    } else if (TextUtils.isEmpty(phoneNo)) {
//                        Toast.makeText(CollectUserDetailsActivity.this, "Enter phone number", Toast.LENGTH_SHORT).show();
//                    } else if (TextUtils.isEmpty(address)) {
//                        Toast.makeText(CollectUserDetailsActivity.this, "Enter address", Toast.LENGTH_SHORT).show();
//                    } else if (TextUtils.isEmpty(state)) {
//                        Toast.makeText(CollectUserDetailsActivity.this, "Enter state", Toast.LENGTH_SHORT).show();
//                    } else if (TextUtils.isEmpty(city)) {
//                        Toast.makeText(CollectUserDetailsActivity.this, "Enter City", Toast.LENGTH_SHORT).show();
//                    } else {
//                        mProgressDialog.setMessage(Constant.PROGRESS_DIALOG_MESSAGE);
//                        mProgressDialog.show();
//
//                        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
//                            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
//                            @Override
//                            public void onComplete(@NonNull Task<InstanceIdResult> task) {
//                                if (!task.isSuccessful()) {
//                                    mProgressDialog.dismiss();
//
//                                    Toast.makeText(CollectUserDetailsActivity.this, R.string.something_wrong, Toast.LENGTH_SHORT).show();
//                                } else {
//                                    tokenId = Objects.requireNonNull(task.getResult()).getToken();
//
//                                    userId = mFirebaseUser.getUid();
//
//                                    dfDate = new SimpleDateFormat("dd-MM-yyyy");
//                                    date = dfDate.format(Calendar.getInstance().getTime());
//
//
//                                    if (filePath != null) {
//                                        for (int i = 0; i < mImgListCompress.size(); i++) {
//                                            Bitmap compressimg = mImgListCompress.get(i);
//                                            uploadImages(compressimg, i);
//                                        }
//                                    } else {
//                                        Toast.makeText(CollectUserDetailsActivity.this, "Select Image", Toast.LENGTH_SHORT).show();
//                                        mProgressDialog.dismiss();
//
//                                    }
//
//
//                                    AddUser mAddUser = new AddUser(tokenId, userId, companyName, contactPerson, phoneNo, address, state, city, date);
//
//                                    mDocumentReferenceUserDetails.set(mAddUser).addOnCompleteListener(new OnCompleteListener<Void>() {
//                                        @Override
//                                        public void onComplete(@NonNull Task<Void> task) {
//                                            mProgressDialog.dismiss();
//                                            Toast.makeText(CollectUserDetailsActivity.this, "succ", Toast.LENGTH_SHORT).show();
//                                            finish();
//                                        }
//                                    }).addOnFailureListener(new OnFailureListener() {
//                                        @Override
//                                        public void onFailure(@NonNull Exception e) {
//                                            mProgressDialog.dismiss();
//                                            Toast.makeText(CollectUserDetailsActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
//                                        }
//                                    });
//
//
//                                }
//                            }
//
//                        });
//
//                    }
//
//
//                }
//            }
//        });
//    }
//
//
//    private void uploadImages(Bitmap compressimg, int i) {
//        final ProgressDialog progressDialog = new ProgressDialog(this);
//        progressDialog.setMessage("Please Wait..");
//        progressDialog.show();
//
//        /*
//         * ByteArrayOutputStream is use to store outputstream(image output)
//         * and convent into byte array beacuse we want to compress image
//         * */
//        if (image_compress != null) {
//
//            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//            compressimg.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream);
//            final byte[] thumb_byte = byteArrayOutputStream.toByteArray();
//            /*
//             * upload image to storagereferance bucket
//             *
//             * */
//            StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(Constant.VISITING_CARD);
//            currentTimeMills = System.currentTimeMillis();
//            final StorageReference storageReference_uploadimage = storageReference.child("Cards :" + currentTimeMills);
//
//            UploadTask uploadTask = storageReference_uploadimage.putBytes(thumb_byte);
//            uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
//                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
//                @Override
//                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
//                    if (!task.isSuccessful()) {
//                        throw Objects.requireNonNull(task.getException());
//                    }
//                    return storageReference_uploadimage.getDownloadUrl();
//                }
//            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
//                @Override
//                public void onComplete(@NonNull Task<Uri> task) {
//                    if (task.isSuccessful()) {
//                        Uri downloadUri = task.getResult();
//                        /*
//                         * Store data to firebase local database
//                         *
//                         * */
//                        if (downloadUri != null) {
//                            imageUrl = downloadUri.toString();
//                        }
//
//
//                        ImageUrl mUrl = new ImageUrl(imageUrl);
//
//
//                        mDocumentReferenceImg.set(mUrl).addOnCompleteListener(new OnCompleteListener<Void>() {
//                            @Override
//                            public void onComplete(@NonNull Task<Void> task) {
//                                mProgressDialog.dismiss();
//                                Toast.makeText(CollectUserDetailsActivity.this, "succ img", Toast.LENGTH_SHORT).show();
//                                finish();
//                            }
//                        }).addOnFailureListener(new OnFailureListener() {
//                            @Override
//                            public void onFailure(@NonNull Exception e) {
//                                mProgressDialog.dismiss();
//                                Toast.makeText(CollectUserDetailsActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
//                            }
//                        });
//
//
//                    }
//                }
//            });
//        } else {
//            Toast.makeText(this, "error!!", Toast.LENGTH_SHORT).show();
//        }
//    }
//
//}
//
//
//
