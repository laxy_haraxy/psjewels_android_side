package com.psjewels.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.psjewels.R;
import com.psjewels.adapter.MyEditOrderProductListAdapter;
import com.psjewels.databinding.ActivityMyOrderProductListBinding;
import com.psjewels.databinding.DialogBankDetailsBinding;
import com.psjewels.model.MyCart;
import com.psjewels.utils.Constant;


public class MyEditOrderProductListActivity extends AppCompatActivity {
    ActivityMyOrderProductListBinding mActivityMyOrderProductListBinding;
    CollectionReference mCollectionReferenceMyOrder;
    FirebaseUser mFirebaseUser;
    String pushKey;
    MyEditOrderProductListAdapter mMyOrderProductListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_my_order_product_list);
        binding();
        checkConnection();
        initDb();
        getData();
        clickEvents();
        back();
        bankDetails();

    }

    private void bankDetails() {

        mActivityMyOrderProductListBinding.iconBankDeatils.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final DialogBankDetailsBinding mDialogBankDetailsBinding;
                final Dialog dialog = new Dialog(MyEditOrderProductListActivity.this,R.style.MyAlertDialogTheme);
                mDialogBankDetailsBinding = DataBindingUtil.inflate(LayoutInflater.from(MyEditOrderProductListActivity.this), R.layout.dialog_bank_details, null, false);
                mDialogBankDetailsBinding.imgclose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                dialog.setContentView(mDialogBankDetailsBinding.getRoot());
                dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
                dialog.show();

            }
        });
    }

    private void back() {
        mActivityMyOrderProductListBinding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void clickEvents() {
        mActivityMyOrderProductListBinding.iconCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), CartActivity.class));

            }
        });
        mActivityMyOrderProductListBinding.iconOrderDeatils.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), MyOrderActivity.class));
            }
        });
        mActivityMyOrderProductListBinding.iconBankDeatils.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        mActivityMyOrderProductListBinding.iconHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });

    }

    public boolean checkConnection() {

        ConnectivityManager manager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = manager.getActiveNetworkInfo();

        if (null != activeNetwork) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
//                Toast.makeText(this, "Wifi Enabled", Toast.LENGTH_SHORT).show();
                return true;
            }
            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
//                Toast.makeText(this, "Data Network Enabled", Toast.LENGTH_SHORT).show();
                return true;
            }
        } else {
            Toast.makeText(this, "No Internet Connection", Toast.LENGTH_SHORT).show();
            return false;
        }
        return false;
    }


    private void getData() {
        FirestoreRecyclerOptions<MyCart> myCartFirestoreRecyclerOptions = new FirestoreRecyclerOptions
                .Builder<MyCart>()
                .setQuery(mCollectionReferenceMyOrder, MyCart.class).build();
        mMyOrderProductListAdapter = new MyEditOrderProductListAdapter(myCartFirestoreRecyclerOptions, this, pushKey);
        mActivityMyOrderProductListBinding.myOrderProductList.setLayoutManager(new LinearLayoutManager(this));
        mActivityMyOrderProductListBinding.myOrderProductList.setAdapter(mMyOrderProductListAdapter);

        mCollectionReferenceMyOrder.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if (queryDocumentSnapshots != null) {
                    if (queryDocumentSnapshots.size() > 0) {
                        mActivityMyOrderProductListBinding.emptyView.setVisibility(View.GONE);
                        mActivityMyOrderProductListBinding.myOrderProductList.setVisibility(View.VISIBLE);
                    } else {

                        mActivityMyOrderProductListBinding.emptyView.setVisibility(View.VISIBLE);
                        mActivityMyOrderProductListBinding.myOrderProductList.setVisibility(View.GONE);
                    }
                } else {

                    mActivityMyOrderProductListBinding.emptyView.setVisibility(View.VISIBLE);
                    mActivityMyOrderProductListBinding.myOrderProductList.setVisibility(View.GONE);
                }
            }
        });
    }

    private void initDb() {
        mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        pushKey = getIntent().getStringExtra(Constant.PUSH_KEY);
        mCollectionReferenceMyOrder = FirebaseFirestore.getInstance().collection(Constant.ORDER_LIST_REF).document(pushKey).collection(Constant.ITEM_LIST_REF);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mMyOrderProductListAdapter.startListening();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mMyOrderProductListAdapter.stopListening();
    }

    private void binding() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mActivityMyOrderProductListBinding = DataBindingUtil.setContentView(this, R.layout.activity_my_order_product_list);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
    }


}
