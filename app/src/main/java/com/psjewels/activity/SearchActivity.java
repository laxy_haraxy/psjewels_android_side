package com.psjewels.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.psjewels.R;
import com.psjewels.adapter.CatWiseProductListAdapter;
import com.psjewels.databinding.ActivitySearchBinding;
import com.psjewels.databinding.DialogBankDetailsBinding;
import com.psjewels.model.ProductDeatils;
import com.psjewels.utils.AutoCompleteSearchAdapterForCategoryWiseProList;
import com.psjewels.utils.AutoCompleteSearchAdapterForSearchActivity;
import com.psjewels.utils.Constant;

import java.util.ArrayList;
import java.util.List;

public class SearchActivity extends AppCompatActivity {

    ActivitySearchBinding mActivitySearchBinding;

    List<DocumentSnapshot> mDocumentChangeList;
    CatWiseProductListAdapter mCatWiseProductListAdapter;

    ProgressDialog mProgressDialog;

    String keyword;
    List<ProductDeatils> subCategoryList;


    private int limit = 15;
    boolean isScrolling = false;
    List<ProductDeatils> list = new ArrayList<>();

    boolean isLastItemReached = false;
    DocumentSnapshot lastVisible;

    String docId, mKey;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_search);
        binding();
        checkConnection();
        clickEvent();
        bankDetails();
        getData();

        searchView();
//        autoCompleteTextView();


    }

//    private void autoCompleteTextView() {
//        //---------------
//
//        CollectionReference mCollectionReferenceSubCatName = FirebaseFirestore.getInstance().collection(Constant.SEARCH_KEYWORD_LIST_COLL);
//
//        subCategoryList = new ArrayList<>();
//        mCollectionReferenceSubCatName.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
//            @Override
//            public void onComplete(@NonNull Task<QuerySnapshot> task) {
//
//                for (final QueryDocumentSnapshot documentSnapshot : task.getResult()) {
//                    ProductDeatils mSubCategory = documentSnapshot.toObject(ProductDeatils.class);
//                    subCategoryList.add(mSubCategory);
//                }
//                AutoCompleteSearchAdapter mCustomAdapter = new AutoCompleteSearchAdapter(SearchActivity.this, subCategoryList);
//                mActivitySearchBinding.autoCompleteText.setAdapter(mCustomAdapter);
//                mActivitySearchBinding.autoCompleteText.setThreshold(1);
//                onStart();
//
//            }
//        });
//
//        //------------
//        mActivitySearchBinding.autoCompleteText.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//
//                String keyword = mActivitySearchBinding.autoCompleteText.getText().toString();
//                Toast.makeText(SearchActivity.this, "asd:- " + keyword, Toast.LENGTH_SHORT).show();
//
////                Intent i = new Intent(SearchActivity.this, SearchActivity.class);
////                i.putExtra("keyword", keyword);
////                startActivity(i);
//
//
//                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                imm.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
//            }
//        });
//        //------------
//
//
//        //---------------
//    }


    private void searchView() {
        mActivitySearchBinding.autoCompleteText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mActivitySearchBinding.close.setVisibility(View.VISIBLE);
                return false;
            }
        });


        mActivitySearchBinding.close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                in.hideSoftInputFromWindow(v.getWindowToken(), 0);


                Intent i = new Intent(SearchActivity.this, SearchActivity.class);
                overridePendingTransition(0, 0);
                overridePendingTransition(0, 0);
                overridePendingTransition(0, 0);
                overridePendingTransition(0, 0);
                i.putExtra("keyword", keyword);
                startActivity(i);
                finish();


//                mActivitySearchBinding.autoCompleteText.setText(null);
//                mActivitySearchBinding.productListRecyclerView.setVisibility(View.GONE);
//
//                onStart();
//                getData();
//
//                mActivitySearchBinding.close.setVisibility(View.GONE);
//                mActivitySearchBinding.allProductSearch.setVisibility(View.GONE);
            }
        });


//        mActivityDashboardBinding.autoCompleteText.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//                mActivityDashboardBinding.productListRecyclerView.setVisibility(View.VISIBLE);
//                filter(s.toString());
//
//            }
//        });


        //---------------


        //------------
        mActivitySearchBinding.autoCompleteText.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

//                Toast.makeText(SearchActivity.this, "asd_: " + mActivitySearchBinding.autoCompleteText.getText().toString(), Toast.LENGTH_SHORT).show();

                String searchData = mActivitySearchBinding.autoCompleteText.getText().toString();

                getDataSearch(searchData);
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);


            }
        });
        //------------


        mActivitySearchBinding.autoCompleteText.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
                String keyword2 = s.toString();


                mActivitySearchBinding.progressBar.setVisibility(View.VISIBLE);
//                mProgressDialog.setMessage(Constant.PROGRESS_DIALOG_MESSAGE);
//                mProgressDialog.show();

//                Intent i = new Intent(CategoryWiseProductShowActivity.this, SearchActivity.class);
//                i.putExtra("keyword", keyword);
//                startActivity(i);
                Query mCollectionReferenceSubCatName = FirebaseFirestore.getInstance().collection(Constant.PRODUCT_LIST_REF).
                        whereArrayContains("keyword_list_name", keyword).orderBy("pro_concet_name").startAt(keyword2).endAt(keyword2 + "\uf8ff").limit(limit);

                subCategoryList = new ArrayList<>();
                mCollectionReferenceSubCatName.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {


                        for (final QueryDocumentSnapshot documentSnapshot : task.getResult()) {
                            ProductDeatils mSubCategory = documentSnapshot.toObject(ProductDeatils.class);
                            Log.d(Constant.TAG, "onComplete: DETAILS  " + mSubCategory.getPro_name());
                            docId = documentSnapshot.getId();
                            subCategoryList.add(mSubCategory);
                        }
                        AutoCompleteSearchAdapterForSearchActivity mCustomAdapter = new AutoCompleteSearchAdapterForSearchActivity(SearchActivity.this, subCategoryList);
                        mActivitySearchBinding.autoCompleteText.setAdapter(mCustomAdapter);
                        mActivitySearchBinding.autoCompleteText.setThreshold(1);
//                        mProgressDialog.dismiss();
                        mActivitySearchBinding.progressBar.setVisibility(View.GONE);

//                onStart();

                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d(Constant.TAG, "onFailure search  : " + e.getLocalizedMessage());
                    }
                });


            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // you can call or do what you want with your EditText here

                // yourEditText...


            }
        });

        //---------------

    }


    private void getData() {


        mProgressDialog = new ProgressDialog(this, R.style.ProgressDialogTheme);

        mProgressDialog.setMessage(Constant.PROGRESS_DIALOG_MESSAGE);
        mProgressDialog.show();
//____________________Real_Time_Code__________________________________________________________________________________________________________________________________________________________________________________________________
        if (!((Activity) this).isFinishing()) {
//            Toast.makeText(this, Constant.PROGRESS_DIALOG_MESSAGE, Toast.LENGTH_SHORT).show();
        }
        mDocumentChangeList = new ArrayList<>();


        mCatWiseProductListAdapter = new CatWiseProductListAdapter(mDocumentChangeList, SearchActivity.this);
        mActivitySearchBinding.productListRecyclerView.setLayoutManager(new GridLayoutManager(SearchActivity.this, 2));
//        mActivityCategoryWiseProductShowBinding.productListRecyclerView.setLayoutManager(new LinearLayoutManager(CategoryWiseProductShowActivity.this));
        mActivitySearchBinding.productListRecyclerView.setAdapter(mCatWiseProductListAdapter);
//        mActivitySearchBinding.nestedScrollViewDashboard.setNestedScrollingEnabled(false);


        Query mQuery = FirebaseFirestore.getInstance().collection(Constant.PRODUCT_LIST_REF).orderBy(Constant.TIME_STAMP_KEY, Query.Direction.DESCENDING).whereArrayContains("keyword_list_name", keyword).limit(limit);


//        mQuery.addSnapshotListener(new EventListener<QuerySnapshot>() {
//            @Override
//            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
//                if (!((Activity) SearchActivity.this).isFinishing()) {
//                    Toast.makeText(SearchActivity.this, Constant.PROGRESS_DIALOG_MESSAGE, Toast.LENGTH_SHORT).show();
//                }
//
//                if (queryDocumentSnapshots != null) {
//                    ArrayList<DocumentSnapshot> mDocumentSnapshotsBookingListArray = new ArrayList<>();
////                    mMangeBookingBinding.txtTodaysAppoin.setText(String.valueOf(queryDocumentSnapshots.size()));
//                    mDocumentChangeList.clear();
//                    mDocumentChangeList.addAll(queryDocumentSnapshots.getDocuments());
//                    mCatWiseProductListAdapter.notifyDataSetChanged();
//                    mProgressDialog.dismiss();
//
//                } else {
//                    Log.d(Constants.TAG, "onEvent: NULL");
//                    mProgressDialog.dismiss();
//                }
//
//
//
////                RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener(){
////                    @Override
////                    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
////                        super.onScrollStateChanged(recyclerView, newState);
////                        if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
////                            isScrolling = true;
//////                            mProgressDialog.show();
////                        }
////                    }
////
////                    @Override
////                    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
////                        super.onScrolled(recyclerView, dx, dy);
////
////                        LinearLayoutManager linearLayoutManager = ((LinearLayoutManager) recyclerView.getLayoutManager());
////                        int firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition();
////                        int visibleItemCount = linearLayoutManager.getChildCount();
////                        int totalItemCount = linearLayoutManager.getItemCount();
////
////
////
////                        if (isScrolling && (firstVisibleItemPosition + visibleItemCount == totalItemCount) && !isLastItemReached) {
////                            isScrolling = false;
////                            Query nextQuery = FirebaseFirestore.getInstance().collection(Constant.PRODUCT_LIST_REF).whereArrayContains("keyword_list_name", keyword).startAfter(lastVisible).limit(limit);
////                            nextQuery.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
////                                @Override
////                                public void onComplete(@NonNull Task<QuerySnapshot> t) {
////                                    if (t.isSuccessful()) {
////                                        for (DocumentSnapshot d : t.getResult()) {
////                                            ProductDeatils productModel = d.toObject(ProductDeatils.class);
////                                            list.add(productModel);
////                                        }
////                                        mCatWiseProductListAdapter.notifyDataSetChanged();
////                                        lastVisible = t.getResult().getDocuments().get(t.getResult().size() - 1);
////
////                                        if (t.getResult().size() < limit) {
////                                            isLastItemReached = true;
////                                            Toast.makeText(SearchActivity.this, "asd", Toast.LENGTH_SHORT).show();
////
////
////                                        }
////                                    }
////                                }
////                            });
////                        }
////                    }
////                };
////                mActivitySearchBinding.productListRecyclerView.addOnScrollListener(onScrollListener);
//
//                }
//        });

        mQuery.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (DocumentSnapshot document : task.getResult()) {
                        ProductDeatils productModel = document.toObject(ProductDeatils.class);
                        mDocumentChangeList.add(document);
                    }


                    mProgressDialog.dismiss();
                    mCatWiseProductListAdapter.notifyDataSetChanged();
                    lastVisible = task.getResult().getDocuments().get(task.getResult().size() - 1);

                    RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                            super.onScrollStateChanged(recyclerView, newState);
                            if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                                isScrolling = true;

                            }
                        }

                        @Override
                        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);

                            LinearLayoutManager linearLayoutManager = ((LinearLayoutManager) recyclerView.getLayoutManager());
                            int firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition();
                            int visibleItemCount = linearLayoutManager.getChildCount();
                            int totalItemCount = linearLayoutManager.getItemCount();


                            Log.d("asdasd", "firstVisibleItemPosition: " + firstVisibleItemPosition);
                            Log.d("asdasd", "visibleItemCount: " + visibleItemCount);
                            Log.d("asdasd", "totalItemCount: " + totalItemCount);
                            Log.d("asdasd", "isLastItemReached: " + isLastItemReached);
                            Log.d("asdasd", "lastVisible: " + lastVisible);
                            Log.d("asdasd", "isScrolling: " + isScrolling);

                            if (isScrolling && (firstVisibleItemPosition + visibleItemCount == totalItemCount) && !isLastItemReached) {


                                isScrolling = false;
                                Query nextQuery = FirebaseFirestore.getInstance().collection(Constant.PRODUCT_LIST_REF).orderBy(Constant.TIME_STAMP_KEY, Query.Direction.DESCENDING).whereArrayContains("keyword_list_name", keyword).startAfter(lastVisible).limit(limit);
                                nextQuery.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                    @Override
                                    public void onComplete(@NonNull Task<QuerySnapshot> t) {
                                        if (t.isSuccessful()) {
                                            for (DocumentSnapshot d : t.getResult()) {
                                                ProductDeatils productModel = d.toObject(ProductDeatils.class);
                                                mDocumentChangeList.add(d);

                                            }
                                            mCatWiseProductListAdapter.notifyDataSetChanged();
                                            lastVisible = t.getResult().getDocuments().get(t.getResult().size() - 1);

                                            if (t.getResult().size() < limit) {
                                                isLastItemReached = true;

                                            }
                                        }
                                    }
                                });
                            } else {

                            }
                        }
                    };
                    mActivitySearchBinding.productListRecyclerView.addOnScrollListener(onScrollListener);
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d("asdasdasd", "onFailure: " + e.getLocalizedMessage());

            }
        });


//        Query mQuery =FirebaseFirestore.getInstance().collection(Constant.PRODUCT_LIST_REF).whereArrayContains("keyword_list_name", keyword);
//        FirestoreRecyclerOptions<ProductDeatils> mProductDetailsFirestoreRecyclerOptions = new FirestoreRecyclerOptions
//                .Builder<ProductDeatils>()
//                .setQuery(mQuery, ProductDeatils.class).build();
//        mCatWiseProductListAdapter = new CatWiseProductListAdapter(mProductDetailsFirestoreRecyclerOptions, this);
//
//        mActivitySearchBinding.productListRecyclerView.setLayoutManager(new GridLayoutManager(SearchActivity.this, 2));
//
//        mActivitySearchBinding.productListRecyclerView.setAdapter(mCatWiseProductListAdapter);


//        Query mQuery =FirebaseFirestore.getInstance().collection(Constant.PRODUCT_LIST_REF).whereArrayContains("keyword_list_name", keyword);
//
//        PagedList.Config config = new PagedList.Config.Builder()
//                .setInitialLoadSizeHint(10)
//                .setPageSize(3)
//                .build();


//        FirestorePagingOptions<ProductDeatils> mProductDetailsFirestoreRecyclerOptions = new FirestorePagingOptions.Builder<ProductDeatils>()
//                .setQuery(mQuery,config, ProductDeatils.class).build();
//        mCatWiseProductListAdapter = new CatWiseProductListAdapter(mProductDetailsFirestoreRecyclerOptions, SearchActivity.this);
//
//        mActivitySearchBinding.productListRecyclerView.setLayoutManager(new GridLayoutManager(SearchActivity.this, 2));
//
//        mActivitySearchBinding.productListRecyclerView.setAdapter(mCatWiseProductListAdapter);


        //------------
    }


    private void getDataSearch(String searchData) {




        mProgressDialog.setMessage(Constant.PROGRESS_DIALOG_MESSAGE);
        mProgressDialog.show();
//____________________Real_Time_Code__________________________________________________________________________________________________________________________________________________________________________________________________
        if (!((Activity) this).isFinishing()) {
//            Toast.makeText(this, Constant.PROGRESS_DIALOG_MESSAGE, Toast.LENGTH_SHORT).show();
        }
        mDocumentChangeList = new ArrayList<>();


        mCatWiseProductListAdapter = new CatWiseProductListAdapter(mDocumentChangeList, SearchActivity.this);
        mActivitySearchBinding.productListRecyclerView.setLayoutManager(new GridLayoutManager(SearchActivity.this, 2));
//        mActivityCategoryWiseProductShowBinding.productListRecyclerView.setLayoutManager(new LinearLayoutManager(CategoryWiseProductShowActivity.this));
        mActivitySearchBinding.productListRecyclerView.setAdapter(mCatWiseProductListAdapter);
//        mActivitySearchBinding.nestedScrollViewDashboard.setNestedScrollingEnabled(false);

        Query mQuery = FirebaseFirestore.getInstance().collection(Constant.PRODUCT_LIST_REF).whereEqualTo("pro_concet_name", searchData).limit(limit);


//        mQuery.addSnapshotListener(new EventListener<QuerySnapshot>() {
//            @Override
//            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
//                if (!((Activity) SearchActivity.this).isFinishing()) {
//                    Toast.makeText(SearchActivity.this, Constant.PROGRESS_DIALOG_MESSAGE, Toast.LENGTH_SHORT).show();
//                }
//
//                if (queryDocumentSnapshots != null) {
//                    ArrayList<DocumentSnapshot> mDocumentSnapshotsBookingListArray = new ArrayList<>();
////                    mMangeBookingBinding.txtTodaysAppoin.setText(String.valueOf(queryDocumentSnapshots.size()));
//                    mDocumentChangeList.clear();
//                    mDocumentChangeList.addAll(queryDocumentSnapshots.getDocuments());
//                    mCatWiseProductListAdapter.notifyDataSetChanged();
//                    mProgressDialog.dismiss();
//
//                } else {
//                    Log.d(Constants.TAG, "onEvent: NULL");
//                    mProgressDialog.dismiss();
//                }
//
//
//
////                RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener(){
////                    @Override
////                    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
////                        super.onScrollStateChanged(recyclerView, newState);
////                        if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
////                            isScrolling = true;
//////                            mProgressDialog.show();
////                        }
////                    }
////
////                    @Override
////                    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
////                        super.onScrolled(recyclerView, dx, dy);
////
////                        LinearLayoutManager linearLayoutManager = ((LinearLayoutManager) recyclerView.getLayoutManager());
////                        int firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition();
////                        int visibleItemCount = linearLayoutManager.getChildCount();
////                        int totalItemCount = linearLayoutManager.getItemCount();
////
////
////
////                        if (isScrolling && (firstVisibleItemPosition + visibleItemCount == totalItemCount) && !isLastItemReached) {
////                            isScrolling = false;
////                            Query nextQuery = FirebaseFirestore.getInstance().collection(Constant.PRODUCT_LIST_REF).whereArrayContains("keyword_list_name", keyword).startAfter(lastVisible).limit(limit);
////                            nextQuery.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
////                                @Override
////                                public void onComplete(@NonNull Task<QuerySnapshot> t) {
////                                    if (t.isSuccessful()) {
////                                        for (DocumentSnapshot d : t.getResult()) {
////                                            ProductDeatils productModel = d.toObject(ProductDeatils.class);
////                                            list.add(productModel);
////                                        }
////                                        mCatWiseProductListAdapter.notifyDataSetChanged();
////                                        lastVisible = t.getResult().getDocuments().get(t.getResult().size() - 1);
////
////                                        if (t.getResult().size() < limit) {
////                                            isLastItemReached = true;
////                                            Toast.makeText(SearchActivity.this, "asd", Toast.LENGTH_SHORT).show();
////
////
////                                        }
////                                    }
////                                }
////                            });
////                        }
////                    }
////                };
////                mActivitySearchBinding.productListRecyclerView.addOnScrollListener(onScrollListener);
//
//                }
//        });

        mQuery.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (DocumentSnapshot document : task.getResult()) {
                        ProductDeatils productModel = document.toObject(ProductDeatils.class);
                        mDocumentChangeList.add(document);
                    }


                    mProgressDialog.dismiss();
                    mCatWiseProductListAdapter.notifyDataSetChanged();
                    lastVisible = task.getResult().getDocuments().get(task.getResult().size() - 1);

                    RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                            super.onScrollStateChanged(recyclerView, newState);
                            if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                                isScrolling = true;

                            }
                        }

                        @Override
                        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);

                            LinearLayoutManager linearLayoutManager = ((LinearLayoutManager) recyclerView.getLayoutManager());
                            int firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition();
                            int visibleItemCount = linearLayoutManager.getChildCount();
                            int totalItemCount = linearLayoutManager.getItemCount();


                            Log.d("asdasd", "firstVisibleItemPosition: " + firstVisibleItemPosition);
                            Log.d("asdasd", "visibleItemCount: " + visibleItemCount);
                            Log.d("asdasd", "totalItemCount: " + totalItemCount);
                            Log.d("asdasd", "isLastItemReached: " + isLastItemReached);
                            Log.d("asdasd", "lastVisible: " + lastVisible);
                            Log.d("asdasd", "isScrolling: " + isScrolling);

                            if (isScrolling && (firstVisibleItemPosition + visibleItemCount == totalItemCount) && !isLastItemReached) {


                                isScrolling = false;
                                Query nextQuery = FirebaseFirestore.getInstance().collection(Constant.PRODUCT_LIST_REF).whereArrayContains("keyword_list_name", keyword).startAfter(lastVisible).limit(limit);
                                nextQuery.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                    @Override
                                    public void onComplete(@NonNull Task<QuerySnapshot> t) {
                                        if (t.isSuccessful()) {
                                            for (DocumentSnapshot d : t.getResult()) {
                                                ProductDeatils productModel = d.toObject(ProductDeatils.class);
                                                mDocumentChangeList.add(d);

                                            }
                                            mCatWiseProductListAdapter.notifyDataSetChanged();
                                            lastVisible = t.getResult().getDocuments().get(t.getResult().size() - 1);

                                            if (t.getResult().size() < limit) {
                                                isLastItemReached = true;

                                            }
                                        }
                                    }
                                });
                            } else {

                            }
                        }
                    };
                    mActivitySearchBinding.productListRecyclerView.addOnScrollListener(onScrollListener);
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d("asdasdasd", "onFailure: " + e.getLocalizedMessage());

            }
        });


//        Query mQuery =FirebaseFirestore.getInstance().collection(Constant.PRODUCT_LIST_REF).whereArrayContains("keyword_list_name", keyword);
//        FirestoreRecyclerOptions<ProductDeatils> mProductDetailsFirestoreRecyclerOptions = new FirestoreRecyclerOptions
//                .Builder<ProductDeatils>()
//                .setQuery(mQuery, ProductDeatils.class).build();
//        mCatWiseProductListAdapter = new CatWiseProductListAdapter(mProductDetailsFirestoreRecyclerOptions, this);
//
//        mActivitySearchBinding.productListRecyclerView.setLayoutManager(new GridLayoutManager(SearchActivity.this, 2));
//
//        mActivitySearchBinding.productListRecyclerView.setAdapter(mCatWiseProductListAdapter);


//        Query mQuery =FirebaseFirestore.getInstance().collection(Constant.PRODUCT_LIST_REF).whereArrayContains("keyword_list_name", keyword);
//
//        PagedList.Config config = new PagedList.Config.Builder()
//                .setInitialLoadSizeHint(10)
//                .setPageSize(3)
//                .build();


//        FirestorePagingOptions<ProductDeatils> mProductDetailsFirestoreRecyclerOptions = new FirestorePagingOptions.Builder<ProductDeatils>()
//                .setQuery(mQuery,config, ProductDeatils.class).build();
//        mCatWiseProductListAdapter = new CatWiseProductListAdapter(mProductDetailsFirestoreRecyclerOptions, SearchActivity.this);
//
//        mActivitySearchBinding.productListRecyclerView.setLayoutManager(new GridLayoutManager(SearchActivity.this, 2));
//
//        mActivitySearchBinding.productListRecyclerView.setAdapter(mCatWiseProductListAdapter);


        //------------
    }

//    @Override
//    protected void onStart() {
//        super.onStart();
//        mCatWiseProductListAdapter.startListening();
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        mCatWiseProductListAdapter.stopListening();
//    }

    private void binding() {

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mActivitySearchBinding = DataBindingUtil.setContentView(this, R.layout.activity_search);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);


        keyword = getIntent().getStringExtra("keyword");


        mProgressDialog = new ProgressDialog(this, R.style.ProgressDialogTheme);

//        Toast.makeText(this, "asdasd:- " + keyword, Toast.LENGTH_SHORT).show();


    }

    public boolean checkConnection() {

        ConnectivityManager manager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = manager.getActiveNetworkInfo();

        if (null != activeNetwork) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
//                Toast.makeText(this, "Wifi Enabled", Toast.LENGTH_SHORT).show();
                return true;
            }
            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
//                Toast.makeText(this, "Data Network Enabled", Toast.LENGTH_SHORT).show();
                return true;
            }
        } else {
            Toast.makeText(this, "No Internet Connection", Toast.LENGTH_SHORT).show();
            return false;
        }
        return false;
    }

    private void clickEvent() {
        mActivitySearchBinding.iconCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), CartActivity.class));
            }
        });
        mActivitySearchBinding.iconOrderDeatils.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), MyOrderActivity.class));
            }
        });
        mActivitySearchBinding.iconBankDeatils.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        mActivitySearchBinding.iconHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });

        mActivitySearchBinding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void bankDetails() {

        mActivitySearchBinding.iconBankDeatils.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final DialogBankDetailsBinding mDialogBankDetailsBinding;
                final Dialog dialog = new Dialog(SearchActivity.this, R.style.MyAlertDialogTheme);
                mDialogBankDetailsBinding = DataBindingUtil.inflate(LayoutInflater.from(SearchActivity.this), R.layout.dialog_bank_details, null, false);
                mDialogBankDetailsBinding.imgclose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                dialog.setContentView(mDialogBankDetailsBinding.getRoot());
                dialog.show();

            }
        });
    }


}