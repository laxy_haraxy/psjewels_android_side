package com.psjewels.activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.viewpager.widget.ViewPager;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.psjewels.R;
import com.psjewels.adapter.CatListAdapterTempRegi;
import com.psjewels.adapter.ViewpagerAdapter;
import com.psjewels.databinding.ActivityRegistarationTemporaryScreenBinding;
import com.psjewels.model.AddCategory;
import com.psjewels.model.AddUser;
import com.psjewels.model.BannerImg;
import com.psjewels.utils.Constant;
import com.psjewels.utils.SpacingItemDecoration;
import com.psjewels.utils.Tools;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class RegistrationTemporayScreenActivity extends AppCompatActivity {

    ActivityRegistarationTemporaryScreenBinding mActivityRegistarationTemporaryScreenBinding;
    CollectionReference mCollectionReferenceBannerList, mCollectionReferenceCatList;
    List<BannerImg> models;

    ViewpagerAdapter viewpagerAdapter;
    ViewPager viewPager2;
    Timer timer;

    CatListAdapterTempRegi mCatListAdapter;

    CollectionReference mCollectionReferenceAdminAccess;
    FirebaseUser mFirebaseUser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_registaration_temporary_screen);
        binding();
        checkConnection();
        initDb();
        getBannerList();
        getCategory();

        clickEvents();




    }

    private void clickEvents() {

        mActivityRegistarationTemporaryScreenBinding.whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent wts = new Intent(Intent.ACTION_VIEW);
//                wts.setData(Uri.parse("https://api.whatsapp.com/send?phone=919033075677"));
                wts.setData(Uri.parse("https://wa.me/message/GR43XAHJMFSXA1"));
                startActivity(wts);
            }
        });


        mActivityRegistarationTemporaryScreenBinding.instagram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent insta = new Intent(Intent.ACTION_VIEW);
                insta.setData(Uri.parse("https://www.instagram.com/psjewels_ahmedabad/"));
                startActivity(insta);
            }
        });

        mActivityRegistarationTemporaryScreenBinding.facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent fb = new Intent(Intent.ACTION_VIEW);
                fb.setData(Uri.parse("https://www.facebook.com/psjewelsahmedabad/"));
                startActivity(fb);
            }
        });


    }

    private void getCategory() {
        FirestoreRecyclerOptions<AddCategory> mCategoryFirestoreRecyclerOptions = new FirestoreRecyclerOptions
                .Builder<AddCategory>()
                .setQuery(mCollectionReferenceCatList, AddCategory.class).build();
        mCatListAdapter = new CatListAdapterTempRegi(mCategoryFirestoreRecyclerOptions, RegistrationTemporayScreenActivity.this);
        mActivityRegistarationTemporaryScreenBinding.categoryList.setLayoutManager(new GridLayoutManager(RegistrationTemporayScreenActivity.this, 2));
        mActivityRegistarationTemporaryScreenBinding.categoryList.addItemDecoration(new SpacingItemDecoration(2, Tools.dpToPx(RegistrationTemporayScreenActivity.this, 4), true));
        mActivityRegistarationTemporaryScreenBinding.categoryList.setLayoutManager(new GridLayoutManager(RegistrationTemporayScreenActivity.this, 2));
        mActivityRegistarationTemporaryScreenBinding.categoryList.setAdapter(mCatListAdapter);

    }

    public boolean checkConnection() {

        ConnectivityManager manager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = manager.getActiveNetworkInfo();

        if (null != activeNetwork) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
//                Toast.makeText(this, "Wifi Enabled", Toast.LENGTH_SHORT).show();
                return true;
            }
            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
//                Toast.makeText(this, "Data Network Enabled", Toast.LENGTH_SHORT).show();
                return true;
            }
        } else {
            Toast.makeText(this, "No Internet Connection", Toast.LENGTH_SHORT).show();
            return false;
        }
        return false;
    }

    private void initDb() {
        mCollectionReferenceBannerList = FirebaseFirestore.getInstance().collection(Constant.BANNER_LIST_COLLE);
        mCollectionReferenceCatList = FirebaseFirestore.getInstance().collection(Constant.CAT_LIST_REF);


    }

    private void getBannerList() {
        mCollectionReferenceBannerList = FirebaseFirestore.getInstance().collection(Constant.BANNER_LIST_COLLE);
        models = new ArrayList<>();

        mCollectionReferenceBannerList.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {

                if (queryDocumentSnapshots != null) {
                    for (DocumentChange dc : queryDocumentSnapshots.getDocumentChanges()) {
                        switch (dc.getType()) {
                            case ADDED:
                                BannerImg mBannerImg = dc.getDocument().toObject(BannerImg.class);
                                models.add(mBannerImg);
                                break;
                            case MODIFIED:
                                break;
                            case REMOVED:
                                break;
                        }
                    }
                    viewpagerAdapter = new ViewpagerAdapter(models, RegistrationTemporayScreenActivity.this);
                    viewPager2 = findViewById(R.id.viewpager_dashboard);
                    viewPager2.setAdapter(viewpagerAdapter);

                    viewPager2.setPadding(100, 0, 100, 0);
                    viewPager2.setOnTouchListener(new View.OnTouchListener() {
                        @SuppressLint("ClickableViewAccessibility")
                        @Override
                        public boolean onTouch(View view, MotionEvent motionEvent) {
                            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                                timer.cancel();
                            }
                            if (motionEvent.getAction() == MotionEvent.ACTION_MOVE) {
                                timer.cancel();
                            }
                            if (motionEvent.getAction() == MotionEvent.ACTION_UP) {

                                Handler mHandler = new Handler();
                                mHandler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        timer.cancel();
                                        automateViewPagerSwiping();
                                    }
                                }, 3000);

                            }

                            return false;
                        }
                    });
                    automateViewPagerSwiping();


                }
            }
        });
    }

    private void automateViewPagerSwiping() {
        final long DELAY_MS = 2000;
        final long PERIOD_MS = 4000;

        final Handler handler = new Handler();
        final Runnable update = new Runnable() {
            public void run() {
                if (viewPager2.getCurrentItem() == viewpagerAdapter.getCount() - 1) {
                    viewPager2.setCurrentItem(0);


                } else {

                    viewPager2.setCurrentItem(viewPager2.getCurrentItem() + 1, true);
                }
            }
        };


        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(update);
            }
        }, DELAY_MS, PERIOD_MS);
    }


    private void binding() {

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mActivityRegistarationTemporaryScreenBinding = DataBindingUtil.setContentView(this, R.layout.activity_registaration_temporary_screen);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,WindowManager.LayoutParams.FLAG_SECURE);




    }

    @Override
    protected void onStart() {
        super.onStart();

        mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        DocumentReference mDocumentReference = FirebaseFirestore.getInstance().collection(Constant.USER_LIST_REF).document(mFirebaseUser.getUid());


        mDocumentReference.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {

                AddUser mAddUser = documentSnapshot.toObject(AddUser.class);

                if(mAddUser.isStatus() == true){
                    Intent i = new Intent(getApplicationContext(),DashboardActivity.class);


                    SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putBoolean("key_name", mAddUser.isStatus());

                    Log.d(Constant.TAG, "sahil regi temp: " +mAddUser.isStatus());

                    editor.commit(); // commit changes

                    startActivity(i);
                    finish();
                }else {

                    Toast.makeText(RegistrationTemporayScreenActivity.this, "Please wait for the admin access!", Toast.LENGTH_SHORT).show();
                }
            }
        });


        mCatListAdapter.startListening();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        mCatListAdapter.stopListening();
    }
}
