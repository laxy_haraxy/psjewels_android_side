package com.psjewels.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.psjewels.R;
import com.psjewels.adapter.SubCatListAdapter;
import com.psjewels.databinding.ActivitySubCatListBinding;
import com.psjewels.databinding.DialogBankDetailsBinding;
import com.psjewels.model.SubCatList;
import com.psjewels.utils.Constant;
import com.psjewels.utils.SpacingItemDecoration;
import com.psjewels.utils.Tools;

import java.util.ArrayList;
import java.util.List;

public class SubCatListActivity extends AppCompatActivity {

    ActivitySubCatListBinding mActivitySubCatListBinding;

    SubCatListAdapter mSubCatListAdapter;

    CollectionReference mCollectionReferenceSubCatList;
    Query mQueryProductListLastColle;

    String catId, catName;



    List<String> mProductDetailsList;
    ArrayAdapter<String> adapter;


//---custom_search
//    List<SubCatList> subCategoryList;
//    String subcatId, subcatName;
//    Query mQuery;


    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_sub_cat_list);
        binding();
        initDb();
        checkConnection();

        clickEvent();
        getSubCategory();
        back();

        searchView();


        mActivitySubCatListBinding.autoCompleteText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mActivitySubCatListBinding.close.setVisibility(View.VISIBLE);
                return false;
            }
        });

        bankDetails();

    }

    private void bankDetails() {

        mActivitySubCatListBinding.iconBankDeatils.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final DialogBankDetailsBinding mDialogBankDetailsBinding;
                final Dialog dialog = new Dialog(SubCatListActivity.this,R.style.MyAlertDialogTheme);
                mDialogBankDetailsBinding = DataBindingUtil.inflate(LayoutInflater.from(SubCatListActivity.this), R.layout.dialog_bank_details, null, false);
                mDialogBankDetailsBinding.imgclose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                dialog.setContentView(mDialogBankDetailsBinding.getRoot());
                dialog.show();

            }
        });
    }


    private void searchView() {

        mProductDetailsList = new ArrayList<>();





        FirebaseFirestore.getInstance().collection(Constant.SUB_CAT_LIST_REF).whereEqualTo(Constant.CAT_ID_KEY,catId).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {

                for (final QueryDocumentSnapshot document : task.getResult()) {
                    SubCatList mSubCatList = document.toObject(SubCatList.class);



                    mProductDetailsList.add(mSubCatList.getSub_cat_name());

                }

                adapter = new ArrayAdapter<String>(SubCatListActivity.this, android.R.layout.select_dialog_item, mProductDetailsList);
                //Getting the instance of AutoCompleteTextView
                mActivitySubCatListBinding.autoCompleteText.setThreshold(1);//will start working from first character
                mActivitySubCatListBinding.autoCompleteText.setAdapter(adapter);


            }
        });



        mActivitySubCatListBinding.autoCompleteText.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String keyword = adapter.getItem(position);




                catId = getIntent().getStringExtra(Constant.CAT_ID_KEY);

                Query mQueryID = mCollectionReferenceSubCatList.whereEqualTo(Constant.CAT_ID_KEY, catId);
                Query mQueryName = mCollectionReferenceSubCatList.whereEqualTo(Constant.CAT_NAME, catName);


                FirestoreRecyclerOptions<SubCatList> mCategoryFirestoreRecyclerOptions = new FirestoreRecyclerOptions
                        .Builder<SubCatList>()
                        .setQuery(mQueryID, SubCatList.class).build();
                mSubCatListAdapter = new SubCatListAdapter(mCategoryFirestoreRecyclerOptions, SubCatListActivity.this);
                mActivitySubCatListBinding.categoryList.setLayoutManager(new GridLayoutManager(SubCatListActivity.this, 2));
                mActivitySubCatListBinding.categoryList.setLayoutManager(new GridLayoutManager(SubCatListActivity.this, 2));
                mActivitySubCatListBinding.categoryList.setAdapter(mSubCatListAdapter);


                onStart();

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
            }
        });




        mActivitySubCatListBinding.close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                in.hideSoftInputFromWindow(v.getWindowToken(), 0);
                mActivitySubCatListBinding.autoCompleteText.setText(null);
//                getData();
                onStart();
                mActivitySubCatListBinding.close.setVisibility(View.GONE);
            }
        });



    }



    private void back() {
        mActivitySubCatListBinding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void clickEvent() {
        mActivitySubCatListBinding.iconCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), CartActivity.class));
            }
        });


        mActivitySubCatListBinding.orderDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), MyOrderActivity.class));
            }
        });


        mActivitySubCatListBinding.iconBankDeatils.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        mActivitySubCatListBinding.iconHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });
    }

    private void initDb() {
        mCollectionReferenceSubCatList = FirebaseFirestore.getInstance().collection(Constant.SUB_CAT_LIST_REF);
//        mQueryProductListLastColle = FirebaseFirestore.getInstance().collection(Constant.PRODUCT_LIST_REF).orderBy(Constant.TIME_STAMP_KEY, Query.Direction.DESCENDING).limit(4);


    }

    public boolean checkConnection() {

        ConnectivityManager manager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = manager.getActiveNetworkInfo();

        if (null != activeNetwork) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
//                Toast.makeText(this, "Wifi Enabled", Toast.LENGTH_SHORT).show();
                return true;
            }
            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
//                Toast.makeText(this, "Data Network Enabled", Toast.LENGTH_SHORT).show();
                return true;
            }
        } else {
            Toast.makeText(this, "No Internet Connection", Toast.LENGTH_SHORT).show();
            return false;
        }
        return false;
    }


    private void getSubCategory() {
        CollectionReference mCollectionReferenceSubCatList = FirebaseFirestore.getInstance().collection(Constant.SUB_CAT_LIST_REF);


        catId = getIntent().getStringExtra(Constant.CAT_ID_KEY);

        Query mQueryID = mCollectionReferenceSubCatList.whereEqualTo(Constant.CAT_ID_KEY, catId);
        Query mQueryName = mCollectionReferenceSubCatList.whereEqualTo(Constant.CAT_NAME, catName);




        FirestoreRecyclerOptions<SubCatList> mCategoryFirestoreRecyclerOptions = new FirestoreRecyclerOptions
                .Builder<SubCatList>()
                .setQuery(mQueryID, SubCatList.class).build();
        mSubCatListAdapter = new SubCatListAdapter(mCategoryFirestoreRecyclerOptions, SubCatListActivity.this);
        mActivitySubCatListBinding.categoryList.setLayoutManager(new GridLayoutManager(SubCatListActivity.this, 2));
        mActivitySubCatListBinding.categoryList.addItemDecoration(new SpacingItemDecoration(2, Tools.dpToPx(SubCatListActivity.this, 4), true));
        mActivitySubCatListBinding.categoryList.setLayoutManager(new GridLayoutManager(SubCatListActivity.this, 2));
        mActivitySubCatListBinding.categoryList.setAdapter(mSubCatListAdapter);




        mQueryID.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if (queryDocumentSnapshots != null) {
                    if (queryDocumentSnapshots.size() > 0) {

//                        Toast.makeText(SubCatListActivity.this, "if :- " + queryDocumentSnapshots.size(), Toast.LENGTH_SHORT).show();
                        mActivitySubCatListBinding.emptyView.setVisibility(View.GONE);
                        mActivitySubCatListBinding.categoryList.setVisibility(View.VISIBLE);
                    } else {
//                        Toast.makeText(SubCatListActivity.this, "else :- " + queryDocumentSnapshots.size(), Toast.LENGTH_SHORT).show();

                        mActivitySubCatListBinding.emptyView.setVisibility(View.VISIBLE);
                        mActivitySubCatListBinding.categoryList.setVisibility(View.GONE);
                    }
                } else {
//                    Toast.makeText(SubCatListActivity.this, "else2 :- " + queryDocumentSnapshots.size(), Toast.LENGTH_SHORT).show();

                    mActivitySubCatListBinding.emptyView.setVisibility(View.VISIBLE);
                    mActivitySubCatListBinding.categoryList.setVisibility(View.GONE);
                }
            }
        });
    }

    private void binding() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mActivitySubCatListBinding = DataBindingUtil.setContentView(this, R.layout.activity_sub_cat_list);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);


        catId = getIntent().getStringExtra(Constant.CAT_ID_KEY);
        catName = getIntent().getStringExtra(Constant.CAT_NAME);
//        Toast.makeText(this, "catId:- " +catId, Toast.LENGTH_SHORT).show();
//        Toast.makeText(this, "catName:- " +catName, Toast.LENGTH_SHORT).show();

        mActivitySubCatListBinding.catName.setText(catName);


    }


    @Override
    protected void onStart() {
        super.onStart();


        mSubCatListAdapter.startListening();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSubCatListAdapter.stopListening();

    }
}





//----custom search-------------------
//    private void searchView() {
//
//        subCategoryList = new ArrayList<>();
//        mCollectionReferenceSubCatList.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
//            @Override
//            public void onComplete(@NonNull Task<QuerySnapshot> task) {
//
//                for (final QueryDocumentSnapshot documentSnapshot : task.getResult()) {
//                    SubCatList mSubCategory = documentSnapshot.toObject(SubCatList.class);
//                    subcatId = documentSnapshot.getId();
//                    mSubCategory.setSub_cat_id(subcatId);
////                    Toast.makeText(SearchActivity.this, "id :- " + subcatId + "name:- " + subcatName, Toast.LENGTH_SHORT).show();
//                    subcatName = mSubCategory.getSub_cat_name();
//                    subCategoryList.add(mSubCategory);
//                }
//
//
//                AutoCompleteSubCatNameAdapter mCustomAdapter = new AutoCompleteSubCatNameAdapter(SubCatListActivity.this, subCategoryList);
//                mActivitySubCatListBinding.autoCompleteText.setAdapter(mCustomAdapter);
//                mActivitySubCatListBinding.autoCompleteText.setThreshold(3);
//
//                onStart();
//
//            }
//        });
//
//
//        mActivitySubCatListBinding.autoCompleteText.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @SuppressLint("ClickableViewAccessibility")
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//
//
////                String subCatId2 = adapter.getItem(position);
//                String subCatId2 = subCategoryList.get(position).getSub_cat_name();
////                Toast.makeText(SearchActivity.this, "position :- " + subCatId2, Toast.LENGTH_SHORT).show();
////                Log.d(Constant.MY_TAG   , "postion: " + subCatId2);
//
//                mQuery = mCollectionReferenceSubCatList.whereEqualTo(Constant.SUB_CAT_NAME, subCatId2);
//
//
////                mQuery = mCollectionReferenceProductList.whereEqualTo(Constant.SUB_CAT, subcatName);
//                FirestoreRecyclerOptions<SubCatList> mProductDetailsFirestoreRecyclerOptions = new FirestoreRecyclerOptions
//                        .Builder<SubCatList>()
//                        .setQuery(mQuery, SubCatList.class).build();
//                mSubCatListAdapter = new SubCatListAdapter(mProductDetailsFirestoreRecyclerOptions, SubCatListActivity.this);
//                mActivitySubCatListBinding.categoryList.setLayoutManager(new GridLayoutManager(SubCatListActivity.this, 2));
//                mActivitySubCatListBinding.categoryList.setAdapter(mSubCatListAdapter);
//                onStart();
//
//                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                imm.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
//            }
//        });
//
//        mActivitySubCatListBinding.close.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                in.hideSoftInputFromWindow(v.getWindowToken(), 0);
//                mActivitySubCatListBinding.autoCompleteText.setText(null);
////                getSubCategory();
//                onStart();
//                mActivitySubCatListBinding.close.setVisibility(View.GONE);
//            }
//        });
//
//    }

