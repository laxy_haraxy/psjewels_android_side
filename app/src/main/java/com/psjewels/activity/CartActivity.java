package com.psjewels.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.psjewels.R;
import com.psjewels.adapter.MyCartAdapter;
import com.psjewels.databinding.ActivityCartBinding;
import com.psjewels.databinding.DialogAddNotesBinding;
import com.psjewels.databinding.DialogBankDetailsBinding;
import com.psjewels.databinding.OrderPlaceDialogBinding;
import com.psjewels.model.AddUser;
import com.psjewels.model.MyCart;
import com.psjewels.model.MyOrder;
import com.psjewels.utils.Constant;
import com.psjewels.utils.SpacingItemDecoration;
import com.psjewels.utils.Tools;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Objects;
import java.util.Random;


public class CartActivity extends AppCompatActivity {
    ActivityCartBinding mActivityCartBinding;
    OrderPlaceDialogBinding mOrderPlaceDialogBinding;
    FirebaseUser mFirebaseUser;
    DocumentReference mDocumentReferenceMyCart;
    CollectionReference mCollectionReferenceMyCart;
    CollectionReference mCollectionReferenceOrderList;
    MyCartAdapter myCartAdapter;
    int totalProductPrice = 0;
    ProgressDialog progressDialog;
    DocumentReference mDocumentReferenceUserList;
    int counter = 0;
    AlertDialog.Builder builder;
    //    MyOrder myOrder;
    String orderDBID;
    private FirebaseAuth mAuth;
    MyOrder myOrder;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_category_name);

        binding();
        checkConnection();
        initDb();
        getData();

        badgeCount();


        DisplayTotalPrice();


        clickEvent();
        back();
        todayDate();

        bankDetails();

    }

    private void bankDetails() {

        mActivityCartBinding.iconBankDeatils.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final DialogBankDetailsBinding mDialogBankDetailsBinding;
                final Dialog dialog = new Dialog(CartActivity.this,R.style.MyAlertDialogTheme);
                mDialogBankDetailsBinding = DataBindingUtil.inflate(LayoutInflater.from(CartActivity.this), R.layout.dialog_bank_details, null, false);
                mDialogBankDetailsBinding.imgclose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                dialog.setContentView(mDialogBankDetailsBinding.getRoot());
                dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);

                dialog.show();

            }
        });
    }

    private void todayDate() {
        DateFormat dfDate = new SimpleDateFormat("dd-MM-yyyy");
        final String date = dfDate.format(Calendar.getInstance().getTime());
        mActivityCartBinding.todayDate.setText(date);
    }

    private void back() {
        mActivityCartBinding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    public int gen() {
        Random r = new Random(System.currentTimeMillis());
        return ((1 + r.nextInt(9)) * 10000 + r.nextInt(10000));
    }

    private void clickEvent() {

        mActivityCartBinding.iconCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(CartActivity.this, "\uD83D\uDE0A", Toast.LENGTH_SHORT).show();
            }
        });
        mActivityCartBinding.iconOrderDeatils.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), MyOrderActivity.class));
            }
        });
        mActivityCartBinding.iconBankDeatils.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        mActivityCartBinding.iconHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });


        mActivityCartBinding.placeorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Dialog mDialog = new Dialog(CartActivity.this);

                mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                mOrderPlaceDialogBinding = OrderPlaceDialogBinding.inflate(LayoutInflater.from(CartActivity.this));
                mDialog.setContentView(mOrderPlaceDialogBinding.getRoot());
                mDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                mDialog.show();

                mOrderPlaceDialogBinding.imgclose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mDialog.dismiss();
                    }
                });
                mOrderPlaceDialogBinding.btnNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mDialog.dismiss();
                    }
                });

                mOrderPlaceDialogBinding.btnYes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        hideKeyboard(v);

                        mDialog.dismiss();
                        final String totalSubScriPrice = String.valueOf(totalProductPrice);

                        mDocumentReferenceUserList.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                DocumentSnapshot mDocumentSnapshot = task.getResult();
                                final AddUser mAddUser = mDocumentSnapshot.toObject(AddUser.class);
                                String company_name = null;
                                String contact_person = null;
                                String phone_number = null;
                                String address = null;
                                String state = null;
                                String city = null;
                                if (mAddUser != null) {
                                    company_name = mAddUser.getCompany_name();
                                    contact_person = mAddUser.getContact_person();
                                    phone_number = mAddUser.getPhone_number();
                                    address = mAddUser.getAddress();
                                    state = mAddUser.getState();

                                    final int orderId = gen();
                                    DateFormat dfDate = new SimpleDateFormat("dd-MM-yyyy");
                                    final String date = dfDate.format(Calendar.getInstance().getTime());
                                    String time = new SimpleDateFormat("hh : mm a", Locale.getDefault()).format(Calendar.getInstance().getTime()).toLowerCase();
                                    myOrder = new MyOrder();
                                    myOrder.setOrder_date(date);
                                    myOrder.setOrder_time(time);
                                    myOrder.setUser_id(mFirebaseUser.getUid());
                                    myOrder.setmAddUser(mAddUser);
                                    myOrder.setOrder_id(orderId);
                                    myOrder.setTime_stamp(System.currentTimeMillis());

                                    orderDBID = mCollectionReferenceOrderList.document().getId();

//                            myOrder.setNote(mActivityCartBinding.note.getText().toString());



                                    placeOrder(myOrder, orderDBID);





                                }
                            }
                        });


                    }
                });




            }
        });

    }
    public void hideKeyboard(View view) {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception ignored) {
        }
    }

    private void placeOrder(final MyOrder myOrder, final String orderDBID) {
        progressDialog = new ProgressDialog(CartActivity.this,R.style.ProgressDialogTheme);
        progressDialog.setMessage(Constant.PROGRESS_DIALOG_MESSAGE);
        progressDialog.show();
        progressDialog.setCancelable(false);

//            FOR REGULAR ORDER
        mCollectionReferenceMyCart.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                final int sizeOfCart = Objects.requireNonNull(task.getResult()).size();

//                USER SIDE

                mCollectionReferenceOrderList.document(orderDBID).set(myOrder).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isComplete()) {

                        }
                    }
                });

                for (final QueryDocumentSnapshot document : task.getResult()) {

//                    USER SIDE
                    mCollectionReferenceOrderList.document(orderDBID).collection(Constant.ITEM_LIST_REF).document(document.getId()).
                            set(document.getData()).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {


                            mCollectionReferenceMyCart.document(document.getId()).delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    counter++;

                                    if (counter == sizeOfCart) {
                                        //user side

                                        mDocumentReferenceMyCart.delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                Toast.makeText(CartActivity.this, "Order placed successfully", Toast.LENGTH_SHORT).show();
                                                progressDialog.dismiss();
                                            }
                                        });
                                    }

                                }
                            });


                        }
                    });
                }
            }
        });

    }

    private void DisplayTotalPrice() {

//        if (checkConnection()) {
//            mCollectionReferenceMyCart.addSnapshotListener(new EventListener<QuerySnapshot>() {
//                @Override
//                public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
//                    totalProductPrice = 0;
//
//                    subTotal = 0;
//                    if (queryDocumentSnapshots != null) {
//
//                        for (QueryDocumentSnapshot doc : queryDocumentSnapshots) {
//                            MyCart myCart = doc.toObject(MyCart.class);
//                            String productPrice = String.valueOf(myCart.getPro_total_price());
//                            totalProductPrice = totalProductPrice + Integer.parseInt(productPrice);
//                        }
//
//
//                        mActivityCartBinding.placeorder.setText("Place Order : " + String.valueOf(totalProductPrice));
//
//                    }
//                }
//            });
//        }
    }

    private void badgeCount() {

        if (checkConnection()) {

            mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();

            mCollectionReferenceMyCart = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF);
            mCollectionReferenceMyCart.addSnapshotListener(new EventListener<QuerySnapshot>() {
                @Override
                public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                    if (queryDocumentSnapshots != null) {
                        int count = queryDocumentSnapshots.size();
//                            Toast.makeText(ProductListActivity.this, "count: " + count, Toast.LENGTH_SHORT).show();
                        String counts = String.valueOf(count);
                        counts.toString();
                        mActivityCartBinding.totalCartItem.setText(counts);


                        if (count == 0) {
                            mActivityCartBinding.emptyView.setVisibility(View.VISIBLE);
                            mActivityCartBinding.recyclerViewMyCart.setVisibility(View.GONE);
                            mActivityCartBinding.totalItemLi.setVisibility(View.GONE);
                            mActivityCartBinding.placeorder.setVisibility(View.GONE);
                            mActivityCartBinding.orderDate.setVisibility(View.GONE);
//                            mActivityCartBinding.note.setVisibility(View.GONE);

                        } else {
                            mActivityCartBinding.emptyView.setVisibility(View.GONE);
                            mActivityCartBinding.recyclerViewMyCart.setVisibility(View.VISIBLE);
                            mActivityCartBinding.totalItemLi.setVisibility(View.VISIBLE);
                            mActivityCartBinding.placeorder.setVisibility(View.VISIBLE);
                            mActivityCartBinding.orderDate.setVisibility(View.VISIBLE);
//                            mActivityCartBinding.note.setVisibility(View.VISIBLE);

                        }
                    }
                }
            });
        }
    }

    private void getData() {
        FirestoreRecyclerOptions<MyCart> myCartFirestoreRecyclerOptions = new FirestoreRecyclerOptions
                .Builder<MyCart>()
                .setQuery(mCollectionReferenceMyCart.orderBy("cart_timestamp", Query.Direction.DESCENDING), MyCart.class).build();
        myCartAdapter = new MyCartAdapter(myCartFirestoreRecyclerOptions, this);
//        mActivityCartBinding.recyclerViewMyCart.setLayoutManager(new LinearLayoutManager(this));
        mActivityCartBinding.recyclerViewMyCart.setLayoutManager(new GridLayoutManager(this,2));
        mActivityCartBinding.recyclerViewMyCart.addItemDecoration(new SpacingItemDecoration(2, Tools.dpToPx(CartActivity.this, 1), true));

        mActivityCartBinding.recyclerViewMyCart.setAdapter(myCartAdapter);
    }

    private void initDb() {


        mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        mAuth = FirebaseAuth.getInstance();

        mCollectionReferenceMyCart = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF);
        builder = new AlertDialog.Builder(this);
        mCollectionReferenceOrderList = FirebaseFirestore.getInstance().collection(Constant.ORDER_LIST_REF);


        if (mFirebaseUser != null) {
            mDocumentReferenceMyCart = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mAuth.getUid());
            mDocumentReferenceUserList = FirebaseFirestore.getInstance().collection(Constant.USER_LIST_REF).document(mAuth.getUid());
        }
    }


    public boolean checkConnection() {

        ConnectivityManager manager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = manager.getActiveNetworkInfo();

        if (null != activeNetwork) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
//                Toast.makeText(this, "Wifi Enabled", Toast.LENGTH_SHORT).show();
                return true;
            }
            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
//                Toast.makeText(this, "Data Network Enabled", Toast.LENGTH_SHORT).show();
                return true;
            }
        } else {
            Toast.makeText(this, "No Internet Connection", Toast.LENGTH_SHORT).show();
            return false;
        }
        return false;
    }

    private void binding() {

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mActivityCartBinding = DataBindingUtil.setContentView(this, R.layout.activity_cart);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,WindowManager.LayoutParams.FLAG_SECURE);

    }


    @Override
    protected void onStart() {
        super.onStart();

        myCartAdapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();

        myCartAdapter.stopListening();
    }
}