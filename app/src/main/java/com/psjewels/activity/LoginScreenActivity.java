package com.psjewels.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCanceledListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.psjewels.MainActivity;
import com.psjewels.R;
import com.psjewels.databinding.ActivityLoginScreenBinding;
import com.psjewels.model.AddUser;
import com.psjewels.utils.Constant;
import com.psjewels.utils.GpsTracker;

import android.location.LocationListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class LoginScreenActivity extends AppCompatActivity implements LocationListener {

    ActivityLoginScreenBinding mActivityLoginScreenBinding;
    private FirebaseAuth mAuth;
    private String mobileNumber;
    ProgressDialog mProgressDialog;
    private String mVerificationId;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    FirebaseUser mFirebaseUser;
    String phoneNumber, deviceId, permissionChecker;
    boolean deviceMatch, deviceNotMatch;

    private GpsTracker gpsTracker;
    String FinalLatitude, FinalLongitude;
    private FusedLocationProviderClient fusedLocationClient;


    private Boolean mLocationPermissionGranted = false;
    private static final int LOCATION_REQUEST_CODE = 1234;
    private FusedLocationProviderClient fusedLocationProviderClient;
    LocationManager locationManager;
    double lat, lng;
    Dialog mDialog;
    String myAddress;

    private static ArrayList<AddUser> mArrayList = new ArrayList<>();

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_login_screen);
        binding();
        checkConnection();

        clickEvent();
        initDB();

        call();


        mActivityLoginScreenBinding.logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String deviceIdCurrent = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
                Toast.makeText(LoginScreenActivity.this, "device ID ;-" +deviceIdCurrent, Toast.LENGTH_SHORT).show();
            }
        });

    }


    @Override
    protected void onResume() {
        super.onResume();
//        Toast.makeText(getApplicationContext(), "resume", Toast.LENGTH_SHORT).show();
        gpsTracker = new GpsTracker(LoginScreenActivity.this);
        if (gpsTracker.canGetLocation()) {
            double latitude = gpsTracker.getLatitude();
            double longitude = gpsTracker.getLongitude();

            FinalLatitude = String.valueOf(latitude);
            FinalLongitude = String.valueOf(longitude);


        } else {
//            gpsTracker.showSettingsAlert();
        }

//        getLocationPermission();
    }


    //--------------------------------------
    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                        finish();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }
//--------------------------------------

    private void call() {

        mActivityLoginScreenBinding.call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String phone = "9426416723";
                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel:" + phone));
                startActivity(callIntent);

            }
        });

    }

    public boolean checkConnection() {

        ConnectivityManager manager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = manager.getActiveNetworkInfo();

        if (null != activeNetwork) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
//                Toast.makeText(this, "Wifi Enabled", Toast.LENGTH_SHORT).show();
                return true;
            }
            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
//                Toast.makeText(this, "Data Network Enabled", Toast.LENGTH_SHORT).show();
                return true;
            }
        } else {
            Toast.makeText(this, "No Internet Connection", Toast.LENGTH_SHORT).show();
            return false;
        }
        return false;
    }


    private void clickEvent() {


        mActivityLoginScreenBinding.btnSendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String phoneNumber = mActivityLoginScreenBinding.edtPhoneNo.getText().toString();
                if (TextUtils.isEmpty(phoneNumber)) {
                    Toast.makeText(LoginScreenActivity.this, "Enter your phone number!!", Toast.LENGTH_SHORT).show();
                    return;
                }
                gpsTracker = new GpsTracker(LoginScreenActivity.this);
                if (gpsTracker.canGetLocation()) {
                    double latitude = gpsTracker.getLatitude();
                    double longitude = gpsTracker.getLongitude();

                    FinalLatitude = String.valueOf(latitude);
                    FinalLongitude = String.valueOf(longitude);

                } else {

                }


//                if (FinalLatitude.equals("0.0") && FinalLongitude.equals("0.0")) {
//                    Toast.makeText(LoginScreenActivity.this, "Location is not available!", Toast.LENGTH_SHORT).show();
//                } else {
//                    Toast.makeText(getApplicationContext(), "asd; sahil:- " + FinalLatitude, Toast.LENGTH_SHORT).show();


                SharedPreferences prefLocation = getApplicationContext().getSharedPreferences("prefLocation", 0); // 0 - for private mode
                SharedPreferences.Editor editorprefLocation = prefLocation.edit();
                editorprefLocation.putString("FinalLatitude", FinalLatitude);
                editorprefLocation.putString("FinalLongitude", FinalLongitude);
                editorprefLocation.commit();


                String LatitudeSharePre = prefLocation.getString("FinalLatitude", "null");
                String LongitudeSharePre = prefLocation.getString("FinalLongitude", "null");
//                    Toast.makeText(getApplicationContext(), "FinalLatitude login" + LatitudeSharePre, Toast.LENGTH_SHORT).show();
//                    Toast.makeText(getApplicationContext(), "FinalLongitude login" + LongitudeSharePre, Toast.LENGTH_SHORT).show();


                login(v);
//                }


            }
        });
    }


    private void login(View v) {

        mProgressDialog.setMessage(Constant.PROGRESS_DIALOG_MESSAGE);
        mProgressDialog.show();
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(v.getApplicationWindowToken(), 0);


        if (checkConnection()) {


            //---------------------------------------------------------------------------------------


            FirebaseFirestore.getInstance().collection(Constant.USER_LIST_REF)
                    .whereEqualTo(Constant.PHONE_NOUMBER_KEY, "+91" + mActivityLoginScreenBinding.edtPhoneNo.getText().toString())
                    .get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                @Override
                public void onSuccess(QuerySnapshot documentSnapshots) {
                        if (documentSnapshots.isEmpty()) {
                            Log.d(Constant.TAG, "onSuccess: LIST EMPTY");


                        //-----------------------------------------------------
                        String deviceIdCurrent = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
                        Log.d(Constant.TAG, "deviceIdCurrent: "+deviceIdCurrent);

                        FirebaseFirestore.getInstance().collection(Constant.USER_LIST_REF)
                                .whereEqualTo("deviceId", deviceIdCurrent)
                                .get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                            @Override
                            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {



                                for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {

                                    if (documentSnapshot.exists()) {
                                        Toast.makeText(LoginScreenActivity.this, "can't login", Toast.LENGTH_SHORT).show();
                                        mActivityLoginScreenBinding.edtPhoneNo.setText(null);
                                        mProgressDialog.dismiss();
                                    }
                                }

                                PhoneAuthProvider.getInstance().verifyPhoneNumber(
                                        "+91" + mActivityLoginScreenBinding.edtPhoneNo.getText().toString(),
                                        60,
                                        TimeUnit.SECONDS,
                                        LoginScreenActivity.this,
                                        new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                                            @Override
                                            public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
                                                Toast.makeText(LoginScreenActivity.this, Constant.PROGRESS_DIALOG_MESSAGE, Toast.LENGTH_SHORT).show();
                                                mProgressDialog.dismiss();
                                            }
                                            @Override
                                            public void onVerificationFailed(@NonNull FirebaseException e) {
//                                                Toast.makeText(LoginScreenActivity.this, "Error: " + e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();

                                                Log.d(Constant.TAG, "onVerificationFailed: "+e.getLocalizedMessage());
                                                mProgressDialog.dismiss();
                                            }
                                            @Override
                                            public void onCodeSent(@NonNull String verificationID, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                                                Intent intent = new Intent(getApplicationContext(), OtpActivity.class);
                                                intent.putExtra(Constant.PHONE_NO, mActivityLoginScreenBinding.edtPhoneNo.getText().toString());
                                                intent.putExtra(Constant.VERIFICATION_ID, verificationID);
                                                startActivity(intent);
                                                mProgressDialog.dismiss();
                                            }
                                        }
                                );

                            }
                        });


                        //-----------------------------------------------------


                    } else {
                        for (DocumentSnapshot documentSnapshot : documentSnapshots) {
                            if (documentSnapshot.exists()) {
                                Log.d(Constant.TAG, "onSuccess: DOCUMENT" + documentSnapshot.getId() + " ; " + documentSnapshot.getData());


                                AddUser mAddUser = documentSnapshot.toObject(AddUser.class);
                                phoneNumber = mAddUser.getPhone_number();
                                deviceId = mAddUser.getDeviceId();
                                String deviceIdCurrent = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);


                                if (deviceId.equals(deviceIdCurrent)) {
//                                    Toast.makeText(LoginScreenActivity.this, "Welcome again!!", Toast.LENGTH_SHORT).show();
                                    PhoneAuthProvider.getInstance().verifyPhoneNumber(
                                            "+91" + mActivityLoginScreenBinding.edtPhoneNo.getText().toString(),
                                            60,
                                            TimeUnit.SECONDS,
                                            LoginScreenActivity.this,
                                            new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                                                @Override
                                                public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
                                                    Toast.makeText(LoginScreenActivity.this, Constant.PROGRESS_DIALOG_MESSAGE, Toast.LENGTH_SHORT).show();
                                                    mProgressDialog.dismiss();
                                                }
                                                @Override
                                                public void onVerificationFailed(@NonNull FirebaseException e) {
                                                    Toast.makeText(LoginScreenActivity.this, "Error: " + e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                                                    mProgressDialog.dismiss();
                                                }
                                                @Override
                                                public void onCodeSent(@NonNull String verificationID, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                                                    Intent intent = new Intent(getApplicationContext(), OtpActivity.class);
                                                    intent.putExtra(Constant.PHONE_NO, mActivityLoginScreenBinding.edtPhoneNo.getText().toString());
                                                    intent.putExtra(Constant.VERIFICATION_ID, verificationID);
                                                    startActivity(intent);
                                                    mProgressDialog.dismiss();
                                                }
                                            }
                                    );

                                } else {
                                    Toast.makeText(LoginScreenActivity.this, "can't login", Toast.LENGTH_SHORT).show();
                                    Toast.makeText(LoginScreenActivity.this, "Device is not same!!", Toast.LENGTH_LONG).show();
                                    mActivityLoginScreenBinding.edtPhoneNo.setText(null);
                                    mProgressDialog.dismiss();
                                    return;
                                }


                            }
                        }
                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
//                    Toast.makeText(getApplicationContext(), "Error getting data!!!", Toast.LENGTH_LONG).show();
                    Log.d(Constant.TAG, "onFailure: ");
                }
            });


        }
    }


    private void binding() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mActivityLoginScreenBinding = DataBindingUtil.setContentView(this, R.layout.activity_login_screen);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);


        //-----------location-----------start-------------------------------

        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();
            Toast.makeText(LoginScreenActivity.this, "Please turn on location:", Toast.LENGTH_SHORT).show();
        } else {
//            Toast.makeText(LoginScreenActivity.this, "location on", Toast.LENGTH_SHORT).show();

        }
        //-----------location-----------end-------------------------------


    }

    private void initDB() {
        mAuth = FirebaseAuth.getInstance();
        mProgressDialog = new ProgressDialog(this, R.style.ProgressDialogTheme);

    }


    @Override
    public void onStart() {
        super.onStart();

        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);


//                //-----------location-----------start-------------------------------
//
//        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//
//        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
//            buildAlertMessageNoGps();
//            Toast.makeText(LoginScreenActivity.this, "Please turn on location:", Toast.LENGTH_SHORT).show();
//        } else {
//            Toast.makeText(LoginScreenActivity.this, "location on", Toast.LENGTH_SHORT).show();
//            finish();
//        }
//        //-----------location-----------end-------------------------------
    }

    private void updateUI(FirebaseUser currentUser) {
        if (currentUser != null) {

            //-------------------------------------------------------------------

            mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
            DocumentReference mDocumentReference = FirebaseFirestore.getInstance().collection(Constant.USER_LIST_REF).document(mFirebaseUser.getUid());


            SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode

            String companyName = pref.getString("edt_company_name", "");


            boolean status = pref.getBoolean("key_name", false);
            Log.d(Constant.TAG, "sahil top 1: " + status);


            if (companyName.equals("")) {
                Log.d(Constant.TAG, "sahil 1: " + status);


                startActivity(new Intent(this, CollectUserDetailsActivity.class));
                finish();
            } else if (status) {
                Log.d(Constant.TAG, "sahil 2: " + status);
                startActivity(new Intent(this, DashboardActivity.class));
                finish();
            } else {
                Log.d(Constant.TAG, "sahil 3: " + status);

                startActivity(new Intent(this, RegistrationTemporayScreenActivity.class));
                finish();
            }


        } else {
//
////            FirebaseFirestore.getInstance().collection(Constant.USER_LIST_REF).orderBy().whereEqualTo()
//            FirebaseFirestore.getInstance().collection(Constant.USER_LIST_REF).get()
//                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
//                        @Override
//                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
//                            if (task.isSuccessful()) {
//                                for (QueryDocumentSnapshot document : task.getResult()) {
//                                    Log.d(Constant.TAG, document.getId() + " => " + document.getData());
//
//                                    AddUser mAddUser = document.toObject(AddUser.class);
//                                    Log.d("asdasd", "phone: " + mAddUser.getPhone_number());
//                                    Log.d("asdasd", "device: " + mAddUser.getDeviceId());
//
//                                    phoneNumber = mAddUser.getPhone_number();
//                                    deviceId = mAddUser.getDeviceId();
//
//                                    String deviceIdCurrent = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
//                                    if (deviceId.equals(deviceIdCurrent)) {
//                                        Log.d("checker", "match device: ");
////                                        Toast.makeText(LoginScreenActivity.this, "match", Toast.LENGTH_SHORT).show();
//                                        deviceMatch = true;
//                                    } else {
//                                        Log.d("checker", "not match device: ");
////                                        Toast.makeText(LoginScreenActivity.this, " no match", Toast.LENGTH_SHORT).show();
//
//                                        deviceNotMatch = true;
//                                    }
//
//
//                                }
//                            } else {
//                                Log.d(Constant.TAG, "Error getting documents: ", task.getException());
//                            }
//                        }
//                    });


        }
    }


    @Override
    public void finish() {
        super.finish();
    }


    @Override
    public void onBackPressed() {
        appCloser();
    }

    private long exitTime = 0;

    public void appCloser() {
        if ((System.currentTimeMillis() - exitTime) > 2000) {
            Toast.makeText(this, "Press again to exit!", Toast.LENGTH_SHORT).show();
            exitTime = System.currentTimeMillis();
        } else {
            finish();
        }
    }


    @Override
    public void onLocationChanged(Location location) {

//        Toast.makeText(this, ""+location.getLatitude()+","+location.getLongitude(), Toast.LENGTH_SHORT).show();
//
//        try {
//            Geocoder geocoder = new Geocoder(LoginScreenActivity.this, Locale.getDefault());
//        }catch (Exception e){
//            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
//        }

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onProviderEnabled(String s) {
//        getDeviceLocation();
    }

    @Override
    public void onProviderDisabled(String s) {

    }
}


//
//    private void clickEvent() {
//
//
//        mActivityLoginScreenBinding.btnSendOtp.setOnClickListener(new View.OnClickListener() {
//            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
//            @Override
//            public void onClick(View v) {
//                mobileNumber = mActivityLoginScreenBinding.edtPhoneNo.getText().toString();
//                if (TextUtils.isEmpty(mobileNumber)) {
//                    mActivityLoginScreenBinding.edtPhoneNo.setError(getString(R.string.field_required));
//                }
//                else {
//                    sendVerificationCode(mobileNumber);
//                }
//            }
//        });
//
//        mActivityLoginScreenBinding.btnSendOtp.setOnClickListener(new View.OnClickListener() {
//            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
//            @Override
//            public void onClick(View view) {
//
//                String otp = Objects.requireNonNull(mActivityLoginScreenBinding.edtOtp.getText()).toString();
//                if (!TextUtils.isEmpty(otp)) {
//                    verifyVerificationCode(otp);
//                } else {
//                    mActivityLoginScreenBinding.edtOtp.setError(getString(R.string.field_required));
//                }
//
//            }
//        });
//
//        mActivityLoginScreenBinding.resendOtp.setOnClickListener(new View.OnClickListener() {
//            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
//            @Override
//            public void onClick(View view) {
//                mobileNumber = Objects.requireNonNull(mActivityLoginScreenBinding.edtPhoneNo.getText()).toString().trim();
//
//                resendVerificationCode(mobileNumber, mResendToken);
//            }
//        });
//    }
//
//    private void sendVerificationCode(String mobileNumber) {
//        mProgressDialog.show();
//        mProgressDialog.setTitle(getString(R.string.attention));
//        mProgressDialog.setMessage(Constant.PROGRESS_DIALOG_MESSAGE);
//        mProgressDialog.setCancelable(false);
//        PhoneAuthProvider.getInstance().verifyPhoneNumber(
//                "+91" + mobileNumber,
//                60,
//                TimeUnit.SECONDS,
//                LoginScreenActivity.this,
//                mCallbacks);
//    }
//
//    private void resendVerificationCode(String mobileNumber,
//                                        PhoneAuthProvider.ForceResendingToken token) {
//        mProgressDialog.show();
//        mProgressDialog.setTitle(getString(R.string.attention));
//        mProgressDialog.setMessage(Constant.PROGRESS_DIALOG_MESSAGE);
//        mProgressDialog.setCancelable(false);
//
//        PhoneAuthProvider.getInstance().verifyPhoneNumber(
//                "+91" + mobileNumber,        // Phone number to verify
//                60,                 // Timeout duration
//                TimeUnit.SECONDS,   // Unit of timeout
//                this,               // Activity (for callback binding)
//                mCallbacks,         // OnVerificationStateChangedCallbacks
//                token);             // ForceResendingToken from callbacks
//    }
//
//
//
//
//
//
//    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
//        @Override
//        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
//            String code = phoneAuthCredential.getSmsCode();
//            mProgressDialog.dismiss();
//
//            if (code != null) {
//                mActivityLoginScreenBinding.edtOtp.setText(code);
//                verifyVerificationCode(code);
//            }
//        }
//
//        @Override
//        public void onVerificationFailed(FirebaseException e) {
//            mProgressDialog.dismiss();
//            Toast.makeText(LoginScreenActivity.this, e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
//        }
//
//        @Override
//        public void onCodeSent(@NonNull String s, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
//            super.onCodeSent(s, forceResendingToken);
//            mVerificationId = s;
//            mResendToken = forceResendingToken;
//            mProgressDialog.dismiss();
//            Toast.makeText(LoginScreenActivity.this, "code sent", Toast.LENGTH_SHORT).show();
//
//
//
//
//        }
//    };
//
//
//
//
//    private void verifyVerificationCode(String otp) {
//        try {
//            mProgressDialog.show();
//            mProgressDialog.setTitle(getString(R.string.attention));
//            mProgressDialog.setMessage(Constant.PROGRESS_DIALOG_MESSAGE);
//            mProgressDialog.setCancelable(false);
//            PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, otp);
//            signInWithPhoneAuthCredential(credential);
//        } catch (Exception e) {
//            Toast.makeText(this, "Error: ", Toast.LENGTH_SHORT).show();
//        }
//    }
//
//
//    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
//        mAuth.signInWithCredential(credential)
//                .addOnCompleteListener(LoginScreenActivity.this, new OnCompleteListener<AuthResult>() {
//                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
//                    @Override
//                    public void onComplete(@NonNull Task<AuthResult> task) {
//                        if (task.isSuccessful()) {
//                            FirebaseUser mFirebaseUser = mAuth.getCurrentUser();
//                            if (mFirebaseUser != null) {
//                                final boolean isNew = Objects.requireNonNull(Objects.requireNonNull(task.getResult()).getAdditionalUserInfo()).isNewUser();
//
//                                mobileNumber = mFirebaseUser.getPhoneNumber();
//                                final String userId = mFirebaseUser.getUid();
//
//                                FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
//                                    @Override
//                                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
//                                        if (!task.isSuccessful()) {
//                                            mProgressDialog.dismiss();
//
//                                            Toast.makeText(LoginScreenActivity.this, R.string.something_wrong, Toast.LENGTH_SHORT).show();
//                                        } else {
////                                            String tokenId = Objects.requireNonNull(task.getResult()).getToken();
////
////                                            DateFormat dfDate = new SimpleDateFormat("dd-MM-yyyy");
////                                            final String date = dfDate.format(Calendar.getInstance().getTime());
//
//                                            mProgressDialog.dismiss();
//
//                                            startActivity(new Intent(LoginScreenActivity.this, RegistrationTemporayScreenActivity.class));
//                                            finish();
//
//
//                                        }
//                                    }
//
//                                });
//                            }
//
//                        } else {
//
//                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
//                                mProgressDialog.dismiss();
//                                Toast.makeText(LoginScreenActivity.this, getString(R.string.Invalid_Code), Toast.LENGTH_SHORT).show();
//                            }
//
//                        }
//                    }
//                });
//    }


//    private void clickEvent() {
//        mActivityLoginScreenBinding.btnSendOtp.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String phone_no = mActivityLoginScreenBinding.edtPhoneNo.getText().toString();
//
//                if (phone_no.isEmpty() || phone_no.length() < 10) {
//                    mActivityLoginScreenBinding.edtPhoneNo.setError("Valid phone is required");
//                    mActivityLoginScreenBinding.edtPhoneNo.requestFocus();
//                    return;
//                }
//
//                Intent intent = new Intent(LoginScreenActivity.this, OtpActivity.class);
//                intent.putExtra(Constant.PHONE_NO, phone_no);
//                startActivity(intent);
//            }
//        });
//    }
//
//    @Override
//    protected void onStart() {
//        super.onStart();
//
//        if(FirebaseAuth.getInstance().getCurrentUser() != null){
//            Intent intent = new Intent(LoginScreenActivity.this, RegistrationTemporayScreenActivity.class);
//            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//            startActivity(intent);
//        }
//    }


//            FirebaseFirestore.getInstance().collection(Constant.USER_LIST_REF)
//                    .whereEqualTo(Constant.PHONE_NOUMBER_KEY, "+91" + mActivityLoginScreenBinding.edtPhoneNo.getText().toString())
//                    .get()
//                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
//@Override
//public void onComplete(@NonNull Task<QuerySnapshot> task) {
//        if (task.isSuccessful()) {
//
//        for (QueryDocumentSnapshot document : task.getResult()) {
//
//        Log.d(Constant.TAG, document.getId() + " => " + document.getData());
//
//        AddUser mAddUser = document.toObject(AddUser.class);
//
//        phoneNumber = mAddUser.getPhone_number();
//        deviceId = mAddUser.getDeviceId();
//        String deviceIdCurrent = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
//
//        if (deviceId.equals(deviceIdCurrent)) {
//
////                                            PhoneAuthProvider.getInstance().verifyPhoneNumber(
////                                                    "+91" + mActivityLoginScreenBinding.edtPhoneNo.getText().toString(),
////                                                    60,
////                                                    TimeUnit.SECONDS,
////                                                    LoginScreenActivity.this,
////                                                    new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
////                                                        @Override
////                                                        public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
////                                                            Toast.makeText(LoginScreenActivity.this, Constant.PROGRESS_DIALOG_MESSAGE, Toast.LENGTH_SHORT).show();
////                                                            mProgressDialog.dismiss();
////                                                        }
////                                                        @Override
////                                                        public void onVerificationFailed(@NonNull FirebaseException e) {
////                                                            Toast.makeText(LoginScreenActivity.this, "Error: " + e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
////                                                            mProgressDialog.dismiss();
////                                                        }
////                                                        @Override
////                                                        public void onCodeSent(@NonNull String verificationID, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
////                                                            Intent intent = new Intent(getApplicationContext(), OtpActivity.class);
////                                                            intent.putExtra(Constant.PHONE_NO, mActivityLoginScreenBinding.edtPhoneNo.getText().toString());
////                                                            intent.putExtra(Constant.VERIFICATION_ID, verificationID);
////                                                            startActivity(intent);
////                                                            mProgressDialog.dismiss();
////                                                        }
////                                                    }
////                                            );
//        Toast.makeText(LoginScreenActivity.this, "Welcome again!!", Toast.LENGTH_SHORT).show();
//
//        } else {
//        Toast.makeText(LoginScreenActivity.this, "can't login", Toast.LENGTH_SHORT).show();
//        Toast.makeText(LoginScreenActivity.this, "Device is not same!!", Toast.LENGTH_LONG).show();
//        mActivityLoginScreenBinding.edtPhoneNo.setText(null);
//        mProgressDialog.dismiss();
//        }
//
//
//        }
//
//
//
//        } else {
//        Log.d(Constant.TAG, "Error getting documents: ", task.getException());
//        }
//
//        }
//        });
