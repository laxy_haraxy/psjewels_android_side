package com.psjewels.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.psjewels.R;
import com.psjewels.adapter.CatWiseProductListAdapter;
import com.psjewels.databinding.ActivityCategoryWiseProductShowBinding;
import com.psjewels.databinding.DialogBankDetailsBinding;
import com.psjewels.model.ProductDeatils;
import com.psjewels.utils.AutoCompleteSearchAdapterForCategoryWiseProList;
import com.psjewels.utils.AutoCompleteSearchAdapterForSearchActivity;
import com.psjewels.utils.Constant;

import java.util.ArrayList;
import java.util.List;

public class CategoryWiseProductShowActivity extends AppCompatActivity {

    ActivityCategoryWiseProductShowBinding mActivityCategoryWiseProductShowBinding;
    String catId, catName, subCatName,subCatId,backSubCatId;
    CatWiseProductListAdapter mCatWiseProductListAdapter;
    CollectionReference mCollectionReferenceProductList;


    //-normal recycler view
    List<DocumentSnapshot> mDocumentChangeList;

    ProgressDialog mProgressDialog;

    private int limit = 15;
    boolean isScrolling =false;
    List<ProductDeatils> list = new ArrayList<>();

    boolean isLastItemReached = false;
    DocumentSnapshot lastVisible ;


    List<ProductDeatils> subCategoryList;
    String docId, mKey;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_category_wise_product_show);
        binding();
        initDb();
        checkConnection();
        setCatName();
        getData();
        clickEvent();
        back();
        searchView();


//-----------------program
//        mActivityCategoryWiseProductShowBinding.asd.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//            }
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//            }
//            @Override
//            public void afterTextChanged(Editable s) {
//                mCollectionReferenceProductList.whereArrayContains("keyword_list_name",s.toString()).get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
//                    @Override
//                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
//
//                        String data="";

//                        for(QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
//                            ProductDeatils mProductDeatils = documentSnapshot.toObject(ProductDeatils.class);
//
//                            String docId = mProductDeatils.getProduct_id();
//
//
//
//                            for (String keyWord : mProductDeatils.getKeyword_list_name()) {
//                                data += "\n-" + keyWord;
//                            }
//
////                    data += "\n\n";
//
//                            Log.d("asd","asd:- " + data);
//                            Log.d("asd","docID:- " + docId);
//
//
//                        }
//                    }
//                });
//
//            }
//        });


//        FirebaseFirestore.getInstance().collection(Constant.PRODUCT_LIST_REF).whereEqualTo(Constant.SUB_CAT_NAME,subCatName).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
//            @Override
//            public void onComplete(@NonNull Task<QuerySnapshot> task) {
//
//                for (final QueryDocumentSnapshot document : task.getResult()) {
//                    ProductDeatils mSearchKeyword = document.toObject(ProductDeatils.class);
//
//                    mProductDetailsList.add(mSearchKeyword.getPro_name());
//                    Log.d(Constant.TAG, "product name sahil: " + mSearchKeyword.getPro_name());
//
//                }
//
//
//
//
//            }
//        });




        bankDetails();







    }



    private void bankDetails() {

        mActivityCategoryWiseProductShowBinding.iconBankDeatils.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final DialogBankDetailsBinding mDialogBankDetailsBinding;
                final Dialog dialog = new Dialog(CategoryWiseProductShowActivity.this,R.style.MyAlertDialogTheme);
                mDialogBankDetailsBinding = DataBindingUtil.inflate(LayoutInflater.from(CategoryWiseProductShowActivity.this), R.layout.dialog_bank_details, null, false);
                mDialogBankDetailsBinding.imgclose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                dialog.setContentView(mDialogBankDetailsBinding.getRoot());
                dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);

                dialog.show();

            }
        });
    }

//    @SuppressLint("ClickableViewAccessibility")
//    private void searchView() {
//
////        mProductDetailsList = new ArrayList<>();
////
////
////        FirebaseFirestore.getInstance().collection(Constant.SEARCH_KEYWORD_LIST_COLL).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
////            @Override
////            public void onComplete(@NonNull Task<QuerySnapshot> task) {
////
////                for (final QueryDocumentSnapshot document : task.getResult()) {
////                    SearchKeyword mSearchKeyword = document.toObject(SearchKeyword.class);
////
////                    mProductDetailsList.add(mSearchKeyword.getSearch_keyword());
////                    Log.d(Constant.TAG, "product name sahil: " + mSearchKeyword.getSearch_keyword());
////
////                }
////
////                adapter = new ArrayAdapter<String>(CategoryWiseProductShowActivity.this, android.R.layout.select_dialog_item, mProductDetailsList);
////                //Getting the instance of AutoCompleteTextView
////                mActivityCategoryWiseProductShowBinding.autoCompleteText.setThreshold(1);//will start working from first character
////                mActivityCategoryWiseProductShowBinding.autoCompleteText.setAdapter(adapter);
////
////
////            }
////        });
////
////
////
////        mActivityCategoryWiseProductShowBinding.autoCompleteText.setOnItemClickListener(new AdapterView.OnItemClickListener() {
////            @Override
////            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
////                String keyword = adapter.getItem(position);
////
////                Query mQuery =  FirebaseFirestore.getInstance().collection(Constant.PRODUCT_LIST_REF).whereArrayContains(Constant.SEARCH_KEYWORD_ARRAY,keyword);
////
////
////                FirestoreRecyclerOptions<ProductDeatils> mProductDetailsFirestoreRecyclerOptions = new FirestoreRecyclerOptions
////                        .Builder<ProductDeatils>()
////                        .setQuery(mQuery, ProductDeatils.class).build();
////                mCatWiseProductListAdapter = new CatWiseProductListAdapter(mProductDetailsFirestoreRecyclerOptions, CategoryWiseProductShowActivity.this);
////                mActivityCategoryWiseProductShowBinding.productListRecyclerView.setLayoutManager(new GridLayoutManager(CategoryWiseProductShowActivity.this, 2));
////                mActivityCategoryWiseProductShowBinding.productListRecyclerView.setAdapter(mCatWiseProductListAdapter);
////                onStart();
////
////                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
////                imm.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
////            }
////        });
////
////
////
////
////        mActivityCategoryWiseProductShowBinding.close.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View v) {
////
////                InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
////                in.hideSoftInputFromWindow(v.getWindowToken(), 0);
////                mActivityCategoryWiseProductShowBinding.autoCompleteText.setText(null);
////getData();
////                onStart();
////                mActivityCategoryWiseProductShowBinding.close.setVisibility(View.GONE);
////            }
////        });
//
//        mActivityCategoryWiseProductShowBinding.autoCompleteText.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                mActivityCategoryWiseProductShowBinding.close.setVisibility(View.VISIBLE);
//                return false;
//            }
//        });
//
//
//
//        mActivityCategoryWiseProductShowBinding.close.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                in.hideSoftInputFromWindow(v.getWindowToken(), 0);
//                mActivityCategoryWiseProductShowBinding.autoCompleteText.setText(null);
////                getData();
//                onStart();
//                mActivityCategoryWiseProductShowBinding.close.setVisibility(View.GONE);
//                mActivityCategoryWiseProductShowBinding.emptyView.setVisibility(View.GONE);
//                mActivityCategoryWiseProductShowBinding.subCatHadder.setVisibility(View.VISIBLE);
//            }
//        });
//
//
////            mActivityCategoryWiseProductShowBinding.autoCompleteText.addTextChangedListener(new TextWatcher() {
////                @Override
////                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
////
////                }
////
////                @Override
////                public void onTextChanged(CharSequence s, int start, int before, int count) {
////
////                }
////
////                @Override
////                public void afterTextChanged(Editable s) {
////                    filter(s.toString());
////                }
////            });
//        }



//        private void filter(String text) {
//
////            ArrayList<DocumentSnapshot> filteredList = new ArrayList<>();
////
////            for (DocumentSnapshot mSubCategoryDS : mDocumentChangeList) {
////                ProductDeatils mProductDeatils = mSubCategoryDS.toObject(ProductDeatils.class);
////
////                if ((mProductDeatils.getPro_code()+mProductDeatils.getPro_code_number()+mProductDeatils.getKeyword_list_name()).toLowerCase().contains(text.toLowerCase())) {
////
////                    filteredList.add(mSubCategoryDS);
////                }else {
////                    mActivityCategoryWiseProductShowBinding.emptyView.setVisibility(View.VISIBLE);
////                    mActivityCategoryWiseProductShowBinding.subCatHadder.setVisibility(View.GONE);
////
////                }
////
////
////
////
////            }
////
////            mCatWiseProductListAdapter.filterList(filteredList);
////
//
//
//
//    }



    private void searchView() {
//        mActivityCategoryWiseProductShowBinding.autoCompleteText.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                mActivityCategoryWiseProductShowBinding.close.setVisibility(View.VISIBLE);
//                return false;
//            }
//        });
//
//
//        mActivityCategoryWiseProductShowBinding.close.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                in.hideSoftInputFromWindow(v.getWindowToken(), 0);
//                mActivityCategoryWiseProductShowBinding.autoCompleteText.setText(null);
//                mActivityCategoryWiseProductShowBinding.productListRecyclerView.setVisibility(View.GONE);
//
////                getData();
//                onStart();
//                mActivityCategoryWiseProductShowBinding.close.setVisibility(View.GONE);
//                mActivityCategoryWiseProductShowBinding.allProductSearch.setVisibility(View.GONE);
//            }
//        });
//
//
////        mActivityDashboardBinding.autoCompleteText.addTextChangedListener(new TextWatcher() {
////            @Override
////            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
////
////            }
////
////            @Override
////            public void onTextChanged(CharSequence s, int start, int before, int count) {
////
////            }
////
////            @Override
////            public void afterTextChanged(Editable s) {
////
////                mActivityDashboardBinding.productListRecyclerView.setVisibility(View.VISIBLE);
////                filter(s.toString());
////
////            }
////        });
//
//
//        //---------------
//
//        CollectionReference mCollectionReferenceSubCatName = FirebaseFirestore.getInstance().collection(Constant.PRODUCT_LIST_REF);
//
//        subCategoryList = new ArrayList<>();
//        mCollectionReferenceSubCatName.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
//            @Override
//            public void onComplete(@NonNull Task<QuerySnapshot> task) {
//
//                for (final QueryDocumentSnapshot documentSnapshot : task.getResult()) {
//                    ProductDeatils mSubCategory = documentSnapshot.toObject(ProductDeatils.class);
//                    docId = documentSnapshot.getId();
//                    subCategoryList.add(mSubCategory);
//                }
//                AutoCompleteSearchAdapterForCategoryWiseProList mCustomAdapter = new AutoCompleteSearchAdapterForCategoryWiseProList(CategoryWiseProductShowActivity.this, subCategoryList);
//                mActivityCategoryWiseProductShowBinding.autoCompleteText.setAdapter(mCustomAdapter);
//                mActivityCategoryWiseProductShowBinding.autoCompleteText.setThreshold(1);
//                onStart();
//
//            }
//        });
//
//        //------------
//        mActivityCategoryWiseProductShowBinding.autoCompleteText.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//
//                String keyword = mActivityCategoryWiseProductShowBinding.autoCompleteText.getText().toString();
//
//                Toast.makeText(CategoryWiseProductShowActivity.this, "asda:- " + keyword, Toast.LENGTH_SHORT).show();
////                Intent i = new Intent(CategoryWiseProductShowActivity.this, SearchActivity.class);
////                i.putExtra("keyword", keyword);
////                startActivity(i);
//
//
//                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                imm.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
//            }
//        });
//        //------------
//
//
//        //---------------




        mActivityCategoryWiseProductShowBinding.autoCompleteText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mActivityCategoryWiseProductShowBinding.close.setVisibility(View.VISIBLE);
                return false;
            }
        });


        mActivityCategoryWiseProductShowBinding.close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                in.hideSoftInputFromWindow(v.getWindowToken(), 0);


                Intent i = new Intent(CategoryWiseProductShowActivity.this,CategoryWiseProductShowActivity.class);
                overridePendingTransition(0,0);
                overridePendingTransition(0,0);
                overridePendingTransition(0,0);
                overridePendingTransition(0,0);
//                i.putExtra("keyword", subCatId);
                i.putExtra(Constant.SUB_CAT_ID,backSubCatId);
                startActivity(i);
                finish();



            }
        });



        mActivityCategoryWiseProductShowBinding.autoCompleteText.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

//                Toast.makeText(SearchActivity.this, "asd_: " + mActivitySearchBinding.autoCompleteText.getText().toString(), Toast.LENGTH_SHORT).show();

                String searchData = mActivityCategoryWiseProductShowBinding.autoCompleteText.getText().toString();

                getDataSearch(searchData);

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);


            }
        });
        //------------


        mActivityCategoryWiseProductShowBinding.autoCompleteText.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
                String keyword2 = s.toString();


//                Intent i = new Intent(CategoryWiseProductShowActivity.this, SearchActivity.class);
//                i.putExtra("keyword", keyword);
//                startActivity(i);
                mActivityCategoryWiseProductShowBinding.progressBar.setVisibility(View.VISIBLE);
                Query mCollectionReferenceSubCatName = FirebaseFirestore.getInstance().collection(Constant.PRODUCT_LIST_REF).
                        whereEqualTo("sub_cat_id",subCatId).orderBy("pro_concet_name").startAt(keyword2).endAt(keyword2 + "\uf8ff").limit(limit);

                subCategoryList = new ArrayList<>();
                mCollectionReferenceSubCatName.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {

                        for (final QueryDocumentSnapshot documentSnapshot : task.getResult()) {
                            ProductDeatils mSubCategory = documentSnapshot.toObject(ProductDeatils.class);
                            Log.d(Constant.TAG, "onComplete: DETAILS  " + mSubCategory.getPro_name());
                            docId = documentSnapshot.getId();
                            subCategoryList.add(mSubCategory);
                        }

                        AutoCompleteSearchAdapterForCategoryWiseProList mCustomAdapter = new AutoCompleteSearchAdapterForCategoryWiseProList(CategoryWiseProductShowActivity.this, subCategoryList);
                mActivityCategoryWiseProductShowBinding.autoCompleteText.setAdapter(mCustomAdapter);
                mActivityCategoryWiseProductShowBinding.autoCompleteText.setThreshold(1);
                        mActivityCategoryWiseProductShowBinding.progressBar.setVisibility(View.GONE);

//                onStart();

                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d(Constant.TAG, "onFailure search  : " + e.getLocalizedMessage());
                    }
                });


            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // you can call or do what you want with your EditText here

                // yourEditText...


            }
        });

        //---------------



    }

    private void getDataSearch(String searchData) {
        mProgressDialog = new ProgressDialog(this, R.style.ProgressDialogTheme);

        mProgressDialog.setMessage(Constant.PROGRESS_DIALOG_MESSAGE);
        mProgressDialog.show();
//____________________Real_Time_Code__________________________________________________________________________________________________________________________________________________________________________________________________
        if (!((Activity) this).isFinishing()) {
//            Toast.makeText(this, Constant.PROGRESS_DIALOG_MESSAGE, Toast.LENGTH_SHORT).show();
        }
        mDocumentChangeList = new ArrayList<>();


        mCatWiseProductListAdapter = new CatWiseProductListAdapter(mDocumentChangeList, CategoryWiseProductShowActivity.this);
        mActivityCategoryWiseProductShowBinding.productListRecyclerView.setLayoutManager(new GridLayoutManager(CategoryWiseProductShowActivity.this, 2));
//        mActivityCategoryWiseProductShowBinding.productListRecyclerView.setLayoutManager(new LinearLayoutManager(CategoryWiseProductShowActivity.this));
        mActivityCategoryWiseProductShowBinding.productListRecyclerView.setAdapter(mCatWiseProductListAdapter);
//        mActivitySearchBinding.nestedScrollViewDashboard.setNestedScrollingEnabled(false);


//        Query mQuery = mCollectionReferenceProductList.orderBy(Constant.TIME_STAMP_KEY, Query.Direction.DESCENDING).whereEqualTo(Constant.SUB_CAT_ID, subCatId).limit(50);
        Query mQuery = mCollectionReferenceProductList.whereEqualTo("pro_concet_name", searchData).limit(limit);


        mQuery.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (DocumentSnapshot document : task.getResult()) {
                        ProductDeatils productModel = document.toObject(ProductDeatils.class);
                        mDocumentChangeList.add(document);
                    }


                    mProgressDialog.dismiss();
                    mCatWiseProductListAdapter.notifyDataSetChanged();
                    lastVisible = task.getResult().getDocuments().get(task.getResult().size() - 1);

                    RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                            super.onScrollStateChanged(recyclerView, newState);
                            if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                                isScrolling = true;

                            }
                        }

                        @Override
                        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);

                            LinearLayoutManager linearLayoutManager = ((LinearLayoutManager) recyclerView.getLayoutManager());
                            int firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition();
                            int visibleItemCount = linearLayoutManager.getChildCount();
                            int totalItemCount = linearLayoutManager.getItemCount();


                            Log.d("asdasd", "firstVisibleItemPosition: "+firstVisibleItemPosition);
                            Log.d("asdasd", "visibleItemCount: "+visibleItemCount);
                            Log.d("asdasd", "totalItemCount: "+totalItemCount);
                            Log.d("asdasd", "isLastItemReached: "+isLastItemReached);
                            Log.d("asdasd", "lastVisible: "+lastVisible);
                            Log.d("asdasd", "isScrolling: "+isScrolling);

                            if (isScrolling && (firstVisibleItemPosition + visibleItemCount == totalItemCount) && !isLastItemReached) {



                                isScrolling = false;
                                Query nextQuery = mCollectionReferenceProductList.whereEqualTo(Constant.SUB_CAT_ID, subCatId).startAfter(lastVisible).limit(limit);
                                nextQuery.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                    @Override
                                    public void onComplete(@NonNull Task<QuerySnapshot> t) {
                                        if (t.isSuccessful()) {
                                            for (DocumentSnapshot d : t.getResult()) {
                                                ProductDeatils productModel = d.toObject(ProductDeatils.class);
                                                mDocumentChangeList.add(d);

                                            }
                                            mCatWiseProductListAdapter.notifyDataSetChanged();
                                            lastVisible = t.getResult().getDocuments().get(t.getResult().size() - 1);

                                            if (t.getResult().size() < limit) {
                                                isLastItemReached = true;

                                            }
                                        }
                                    }
                                });
                            }else {

                            }
                        }
                    };
                    mActivityCategoryWiseProductShowBinding.productListRecyclerView.addOnScrollListener(onScrollListener);
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d("asdasdasd", "onFailure: "+e.getLocalizedMessage());

            }
        });

    }


    private void back() {

        mActivityCategoryWiseProductShowBinding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void clickEvent() {
        mActivityCategoryWiseProductShowBinding.iconCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), CartActivity.class));
            }
        });
        mActivityCategoryWiseProductShowBinding.iconOrderDeatils.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), MyOrderActivity.class));
            }
        });
        mActivityCategoryWiseProductShowBinding.iconBankDeatils.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        mActivityCategoryWiseProductShowBinding.iconHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });
    }


    private void getData() {

//        mProgressDialog.setMessage(Constant.PROGRESS_DIALOG_MESSAGE);
//        mProgressDialog.show();
////____________________Real_Time_Code__________________________________________________________________________________________________________________________________________________________________________________________________
//        if (!((Activity) this).isFinishing()) {
////            Toast.makeText(this, Constant.PROGRESS_DIALOG_MESSAGE, Toast.LENGTH_SHORT).show();
//        }
//        mDocumentChangeList = new ArrayList<>();
//
//
//        mCatWiseProductListAdapter = new CatWiseProductListAdapter(mDocumentChangeList, CategoryWiseProductShowActivity.this);
//        mActivityCategoryWiseProductShowBinding.productListRecyclerView.setLayoutManager(new GridLayoutManager(CategoryWiseProductShowActivity.this,2));
////        mActivityCategoryWiseProductShowBinding.productListRecyclerView.setLayoutManager(new LinearLayoutManager(CategoryWiseProductShowActivity.this));
//        mActivityCategoryWiseProductShowBinding.productListRecyclerView.setAdapter(mCatWiseProductListAdapter);
//
////        //-------------------------
////        mActivityCategoryWiseProductShowBinding.productListRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
////            @Override
////            public void onScrollStateChanged(@NonNull  RecyclerView recyclerView, int newState) {
////                super.onScrollStateChanged(recyclerView, newState);
////
////                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL){
////                    isScrolling = true;
////                }
////
////            }
////
////            @Override
////            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
////                super.onScrolled(recyclerView, dx, dy);
////            }
////        });
////        //-------------------------
//
//        Query mQuery = mCollectionReferenceProductList.orderBy(Constant.TIME_STAMP_KEY, Query.Direction.DESCENDING).whereEqualTo(Constant.SUB_CAT_ID, subCatId).limit(50);
//
//        mQuery.addSnapshotListener(new EventListener<QuerySnapshot>() {
//            @Override
//            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
//                if (!((Activity) CategoryWiseProductShowActivity.this).isFinishing()) {
//                    Toast.makeText(this, Constant.PROGRESS_DIALOG_MESSAGE, Toast.LENGTH_SHORT).show();
//                }
//
//                if (queryDocumentSnapshots != null) {
//                    ArrayList<DocumentSnapshot> mDocumentSnapshotsBookingListArray = new ArrayList<>();
////                    mMangeBookingBinding.txtTodaysAppoin.setText(String.valueOf(queryDocumentSnapshots.size()));
//                    mDocumentChangeList.clear();
//                    mDocumentChangeList.addAll(queryDocumentSnapshots.getDocuments());
//                    mCatWiseProductListAdapter.notifyDataSetChanged();
//                } else {
//                    Log.d(Constants.TAG, "onEvent: NULL");
//                    mProgressDialog.dismiss();
//                }
//            }
//        });



//        Query mQuery =mCollectionReferenceProductList.orderBy(Constant.TIME_STAMP_KEY, Query.Direction.DESCENDING).whereEqualTo(Constant.SUB_CAT_ID, subCatId);
//        FirestoreRecyclerOptions<ProductDeatils> mProductDetailsFirestoreRecyclerOptions = new FirestoreRecyclerOptions
//                .Builder<ProductDeatils>()
//                .setQuery(mQuery, ProductDeatils.class).build();
//        mCatWiseProductListAdapter = new CatWiseProductListAdapter(mProductDetailsFirestoreRecyclerOptions, this);
//
//        mActivityCategoryWiseProductShowBinding.productListRecyclerView.setLayoutManager(new GridLayoutManager(CategoryWiseProductShowActivity.this, 2));
//
//        mActivityCategoryWiseProductShowBinding.productListRecyclerView.setAdapter(mCatWiseProductListAdapter);


        mProgressDialog = new ProgressDialog(this, R.style.ProgressDialogTheme);

        mProgressDialog.setMessage(Constant.PROGRESS_DIALOG_MESSAGE);
        mProgressDialog.show();
//____________________Real_Time_Code__________________________________________________________________________________________________________________________________________________________________________________________________
        if (!((Activity) this).isFinishing()) {
//            Toast.makeText(this, Constant.PROGRESS_DIALOG_MESSAGE, Toast.LENGTH_SHORT).show();
        }
        mDocumentChangeList = new ArrayList<>();


        mCatWiseProductListAdapter = new CatWiseProductListAdapter(mDocumentChangeList, CategoryWiseProductShowActivity.this);
        mActivityCategoryWiseProductShowBinding.productListRecyclerView.setLayoutManager(new GridLayoutManager(CategoryWiseProductShowActivity.this, 2));
//        mActivityCategoryWiseProductShowBinding.productListRecyclerView.setLayoutManager(new LinearLayoutManager(CategoryWiseProductShowActivity.this));
        mActivityCategoryWiseProductShowBinding.productListRecyclerView.setAdapter(mCatWiseProductListAdapter);
//        mActivitySearchBinding.nestedScrollViewDashboard.setNestedScrollingEnabled(false);


//        Query mQuery = mCollectionReferenceProductList.orderBy(Constant.TIME_STAMP_KEY, Query.Direction.DESCENDING).whereEqualTo(Constant.SUB_CAT_ID, subCatId).limit(50);
        Query mQuery = mCollectionReferenceProductList.orderBy(Constant.TIME_STAMP_KEY, Query.Direction.DESCENDING).whereEqualTo(Constant.SUB_CAT_ID, subCatId).limit(limit);


        mQuery.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if (queryDocumentSnapshots != null) {
                    int count = queryDocumentSnapshots.size();
//                            Toast.makeText(ProductListActivity.this, "count: " + count, Toast.LENGTH_SHORT).show();
                    String counts = String.valueOf(count);
                    counts.toString();


                    if (count == 0) {
                        mActivityCategoryWiseProductShowBinding.emptyView.setVisibility(View.VISIBLE);
                        mActivityCategoryWiseProductShowBinding.productListRecyclerView.setVisibility(View.GONE);


                    } else {
                        mActivityCategoryWiseProductShowBinding.emptyView.setVisibility(View.GONE);
                        mActivityCategoryWiseProductShowBinding.productListRecyclerView.setVisibility(View.VISIBLE);
                    }
                }
            }
        });


        mQuery.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (DocumentSnapshot document : task.getResult()) {
                        ProductDeatils productModel = document.toObject(ProductDeatils.class);
                        mDocumentChangeList.add(document);
                    }


                    mProgressDialog.dismiss();
                    mCatWiseProductListAdapter.notifyDataSetChanged();
                    try {
                        lastVisible = task.getResult().getDocuments().get(task.getResult().size() - 1);
                    }catch (Exception e) {

                    }


                    RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                            super.onScrollStateChanged(recyclerView, newState);
                            if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                                isScrolling = true;

                            }
                        }

                        @Override
                        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);

                            LinearLayoutManager linearLayoutManager = ((LinearLayoutManager) recyclerView.getLayoutManager());
                            int firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition();
                            int visibleItemCount = linearLayoutManager.getChildCount();
                            int totalItemCount = linearLayoutManager.getItemCount();


                            Log.d("asdasd", "firstVisibleItemPosition: "+firstVisibleItemPosition);
                            Log.d("asdasd", "visibleItemCount: "+visibleItemCount);
                            Log.d("asdasd", "totalItemCount: "+totalItemCount);
                            Log.d("asdasd", "isLastItemReached: "+isLastItemReached);
                            Log.d("asdasd", "lastVisible: "+lastVisible);
                            Log.d("asdasd", "isScrolling: "+isScrolling);

                            if (isScrolling && (firstVisibleItemPosition + visibleItemCount == totalItemCount) && !isLastItemReached) {



                                isScrolling = false;
                                Query nextQuery = mCollectionReferenceProductList.orderBy(Constant.TIME_STAMP_KEY, Query.Direction.DESCENDING).whereEqualTo(Constant.SUB_CAT_ID, subCatId).startAfter(lastVisible).limit(limit);
                                nextQuery.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                    @Override
                                    public void onComplete(@NonNull Task<QuerySnapshot> t) {
                                        if (t.isSuccessful()) {
                                            for (DocumentSnapshot d : t.getResult()) {
                                                ProductDeatils productModel = d.toObject(ProductDeatils.class);
                                                mDocumentChangeList.add(d);

                                            }
                                            mCatWiseProductListAdapter.notifyDataSetChanged();
                                            lastVisible = t.getResult().getDocuments().get(t.getResult().size() - 1);

                                            if (t.getResult().size() < limit) {
                                                isLastItemReached = true;

                                            }
                                        }
                                    }
                                });
                            }else {

                            }
                        }
                    };
                    mActivityCategoryWiseProductShowBinding.productListRecyclerView.addOnScrollListener(onScrollListener);
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d("asdasdasd", "onFailure: "+e.getLocalizedMessage());

            }
        });

    }


    private void getDataSearchData() {

//        mProgressDialog.setMessage(Constant.PROGRESS_DIALOG_MESSAGE);
//        mProgressDialog.show();
////____________________Real_Time_Code__________________________________________________________________________________________________________________________________________________________________________________________________
//        if (!((Activity) this).isFinishing()) {
////            Toast.makeText(this, Constant.PROGRESS_DIALOG_MESSAGE, Toast.LENGTH_SHORT).show();
//        }
//        mDocumentChangeList = new ArrayList<>();
//
//
//        mCatWiseProductListAdapter = new CatWiseProductListAdapter(mDocumentChangeList, CategoryWiseProductShowActivity.this);
//        mActivityCategoryWiseProductShowBinding.productListRecyclerView.setLayoutManager(new GridLayoutManager(CategoryWiseProductShowActivity.this,2));
////        mActivityCategoryWiseProductShowBinding.productListRecyclerView.setLayoutManager(new LinearLayoutManager(CategoryWiseProductShowActivity.this));
//        mActivityCategoryWiseProductShowBinding.productListRecyclerView.setAdapter(mCatWiseProductListAdapter);
//
////        //-------------------------
////        mActivityCategoryWiseProductShowBinding.productListRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
////            @Override
////            public void onScrollStateChanged(@NonNull  RecyclerView recyclerView, int newState) {
////                super.onScrollStateChanged(recyclerView, newState);
////
////                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL){
////                    isScrolling = true;
////                }
////
////            }
////
////            @Override
////            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
////                super.onScrolled(recyclerView, dx, dy);
////            }
////        });
////        //-------------------------
//
//        Query mQuery = mCollectionReferenceProductList.orderBy(Constant.TIME_STAMP_KEY, Query.Direction.DESCENDING).whereEqualTo(Constant.SUB_CAT_ID, subCatId).limit(50);
//
//        mQuery.addSnapshotListener(new EventListener<QuerySnapshot>() {
//            @Override
//            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
//                if (!((Activity) CategoryWiseProductShowActivity.this).isFinishing()) {
//                    Toast.makeText(this, Constant.PROGRESS_DIALOG_MESSAGE, Toast.LENGTH_SHORT).show();
//                }
//
//                if (queryDocumentSnapshots != null) {
//                    ArrayList<DocumentSnapshot> mDocumentSnapshotsBookingListArray = new ArrayList<>();
////                    mMangeBookingBinding.txtTodaysAppoin.setText(String.valueOf(queryDocumentSnapshots.size()));
//                    mDocumentChangeList.clear();
//                    mDocumentChangeList.addAll(queryDocumentSnapshots.getDocuments());
//                    mCatWiseProductListAdapter.notifyDataSetChanged();
//                } else {
//                    Log.d(Constants.TAG, "onEvent: NULL");
//                    mProgressDialog.dismiss();
//                }
//            }
//        });



//        Query mQuery =mCollectionReferenceProductList.orderBy(Constant.TIME_STAMP_KEY, Query.Direction.DESCENDING).whereEqualTo(Constant.SUB_CAT_ID, subCatId);
//        FirestoreRecyclerOptions<ProductDeatils> mProductDetailsFirestoreRecyclerOptions = new FirestoreRecyclerOptions
//                .Builder<ProductDeatils>()
//                .setQuery(mQuery, ProductDeatils.class).build();
//        mCatWiseProductListAdapter = new CatWiseProductListAdapter(mProductDetailsFirestoreRecyclerOptions, this);
//
//        mActivityCategoryWiseProductShowBinding.productListRecyclerView.setLayoutManager(new GridLayoutManager(CategoryWiseProductShowActivity.this, 2));
//
//        mActivityCategoryWiseProductShowBinding.productListRecyclerView.setAdapter(mCatWiseProductListAdapter);


        mProgressDialog = new ProgressDialog(this, R.style.ProgressDialogTheme);

        mProgressDialog.setMessage(Constant.PROGRESS_DIALOG_MESSAGE);
        mProgressDialog.show();
//____________________Real_Time_Code__________________________________________________________________________________________________________________________________________________________________________________________________
        if (!((Activity) this).isFinishing()) {
//            Toast.makeText(this, Constant.PROGRESS_DIALOG_MESSAGE, Toast.LENGTH_SHORT).show();
        }
        mDocumentChangeList = new ArrayList<>();


        mCatWiseProductListAdapter = new CatWiseProductListAdapter(mDocumentChangeList, CategoryWiseProductShowActivity.this);
        mActivityCategoryWiseProductShowBinding.productListRecyclerView.setLayoutManager(new GridLayoutManager(CategoryWiseProductShowActivity.this, 2));
//        mActivityCategoryWiseProductShowBinding.productListRecyclerView.setLayoutManager(new LinearLayoutManager(CategoryWiseProductShowActivity.this));
        mActivityCategoryWiseProductShowBinding.productListRecyclerView.setAdapter(mCatWiseProductListAdapter);
//        mActivitySearchBinding.nestedScrollViewDashboard.setNestedScrollingEnabled(false);


//        Query mQuery = mCollectionReferenceProductList.orderBy(Constant.TIME_STAMP_KEY, Query.Direction.DESCENDING).whereEqualTo(Constant.SUB_CAT_ID, subCatId).limit(50);
        Query mQuery = mCollectionReferenceProductList.whereEqualTo(Constant.SUB_CAT_ID, subCatId).limit(limit);


        mQuery.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (DocumentSnapshot document : task.getResult()) {
                        ProductDeatils productModel = document.toObject(ProductDeatils.class);
                        mDocumentChangeList.add(document);
                    }


                    mProgressDialog.dismiss();
                    mCatWiseProductListAdapter.notifyDataSetChanged();
                    lastVisible = task.getResult().getDocuments().get(task.getResult().size() - 1);

                    RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                            super.onScrollStateChanged(recyclerView, newState);
                            if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                                isScrolling = true;

                            }
                        }

                        @Override
                        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);

                            LinearLayoutManager linearLayoutManager = ((LinearLayoutManager) recyclerView.getLayoutManager());
                            int firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition();
                            int visibleItemCount = linearLayoutManager.getChildCount();
                            int totalItemCount = linearLayoutManager.getItemCount();


                            Log.d("asdasd", "firstVisibleItemPosition: "+firstVisibleItemPosition);
                            Log.d("asdasd", "visibleItemCount: "+visibleItemCount);
                            Log.d("asdasd", "totalItemCount: "+totalItemCount);
                            Log.d("asdasd", "isLastItemReached: "+isLastItemReached);
                            Log.d("asdasd", "lastVisible: "+lastVisible);
                            Log.d("asdasd", "isScrolling: "+isScrolling);

                            if (isScrolling && (firstVisibleItemPosition + visibleItemCount == totalItemCount) && !isLastItemReached) {



                                isScrolling = false;
                                Query nextQuery = mCollectionReferenceProductList.whereEqualTo(Constant.SUB_CAT_ID, subCatId).startAfter(lastVisible).limit(limit);
                                nextQuery.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                    @Override
                                    public void onComplete(@NonNull Task<QuerySnapshot> t) {
                                        if (t.isSuccessful()) {
                                            for (DocumentSnapshot d : t.getResult()) {
                                                ProductDeatils productModel = d.toObject(ProductDeatils.class);
                                                mDocumentChangeList.add(d);

                                            }
                                            mCatWiseProductListAdapter.notifyDataSetChanged();
                                            lastVisible = t.getResult().getDocuments().get(t.getResult().size() - 1);

                                            if (t.getResult().size() < limit) {
                                                isLastItemReached = true;

                                            }
                                        }
                                    }
                                });
                            }else {

                            }
                        }
                    };
                    mActivityCategoryWiseProductShowBinding.productListRecyclerView.addOnScrollListener(onScrollListener);
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d("asdasdasd", "onFailure: "+e.getLocalizedMessage());

            }
        });

    }


    public boolean checkConnection() {

        ConnectivityManager manager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = manager.getActiveNetworkInfo();

        if (null != activeNetwork) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
//                Toast.makeText(this, "Wifi Enabled", Toast.LENGTH_SHORT).show();
                return true;
            }
            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
//                Toast.makeText(this, "Data Network Enabled", Toast.LENGTH_SHORT).show();
                return true;
            }
        } else {
            Toast.makeText(this, "No Internet Connection", Toast.LENGTH_SHORT).show();
            return false;
        }
        return false;
    }


    private void initDb() {
        mCollectionReferenceProductList = FirebaseFirestore.getInstance().collection(Constant.PRODUCT_LIST_REF);
        catId = getIntent().getStringExtra(Constant.CAT_ID_KEY);
        catName = getIntent().getStringExtra(Constant.CAT_NAME);
        subCatName = getIntent().getStringExtra(Constant.SUB_CAT_NAME);
        subCatId = getIntent().getStringExtra(Constant.SUB_CAT_ID);
        backSubCatId=getIntent().getStringExtra(Constant.SUB_CAT_ID);


        mProgressDialog = new ProgressDialog(this, R.style.ProgressDialogTheme);

//        Toast.makeText(this, "asd: " +subCatId, Toast.LENGTH_SHORT).show();

    }

    private void setCatName() {

        mActivityCategoryWiseProductShowBinding.catName.setText(subCatName);

    }

    private void binding() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        mActivityCategoryWiseProductShowBinding = DataBindingUtil.setContentView(this, R.layout.activity_category_wise_product_show);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);


    }


//    @Override
//    protected void onStart() {
//        super.onStart();
//        mCatWiseProductListAdapter.startListening();
//    }
//
//    @Override
//    protected void onStop() {
//        super.onStop();
//        mCatWiseProductListAdapter.stopListening();
//    }
}