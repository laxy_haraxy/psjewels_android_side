package com.psjewels.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.arch.core.executor.TaskExecutor;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.psjewels.R;
import com.psjewels.databinding.ActivityOtpBinding;
import com.psjewels.utils.Constant;

import java.util.concurrent.TimeUnit;

public class OtpActivity extends AppCompatActivity {
    ActivityOtpBinding mActivityOtpBinding;

    String currentLoginPhoneNumber;
    private String verificationId;
    private FirebaseAuth mAuth;
    private ProgressBar mProgressBar;
    ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_otp);
        binding();
        checkConnection();
        clickEvent();
        call();


    }

    private void call() {

        mActivityOtpBinding.contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String phone = "9426416723";
                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel:" + phone));
                startActivity(callIntent);

            }
        });

        }

    public boolean checkConnection() {

        ConnectivityManager manager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = manager.getActiveNetworkInfo();

        if (null != activeNetwork) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
//                Toast.makeText(this, "Wifi Enabled", Toast.LENGTH_SHORT).show();
                return true;
            }
            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
//                Toast.makeText(this, "Data Network Enabled", Toast.LENGTH_SHORT).show();
                return true;
            }
        } else {
            Toast.makeText(this, "No Internet Connection", Toast.LENGTH_SHORT).show();
            return false;
        }
        return false;
    }

    private void clickEvent() {
        verificationId = getIntent().getStringExtra(Constant.VERIFICATION_ID);

        currentLoginPhoneNumber =getIntent().getStringExtra(Constant.VERIFICATION_ID);

        mActivityOtpBinding.btnLogin.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (checkConnection()) {


                    mProgressDialog.setMessage(Constant.PROGRESS_DIALOG_MESSAGE);
                    mProgressDialog.show();
                    InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(v.getApplicationWindowToken(), 0);


                    if (checkConnection()) {
                        String code = mActivityOtpBinding.edtOtp.getText().toString();
                        if (TextUtils.isEmpty( code)) {
                            Toast.makeText(OtpActivity.this, "Please enter valid code", Toast.LENGTH_SHORT).show();
                            mProgressDialog.dismiss();
                            return;
                        }




                        if (verificationId != null) {


                            PhoneAuthCredential phoneAuthCredential = PhoneAuthProvider.getCredential(verificationId, code);

                            FirebaseAuth.getInstance().signInWithCredential(phoneAuthCredential)
                                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                        @Override
                                        public void onComplete(@NonNull Task<AuthResult> task) {

                                            if (task.isSuccessful()) {
                                                Intent intent = new Intent(getApplicationContext(), CollectUserDetailsActivity.class);
//                                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                intent.putExtra(Constant.VERIFICATION_ID,currentLoginPhoneNumber);
                                                startActivity(intent);
                                                finish();
                                                mProgressDialog.dismiss();

                                            } else {
                                                Toast.makeText(OtpActivity.this, "The verification code entered was invalid", Toast.LENGTH_SHORT).show();
                                                mProgressDialog.dismiss();

                                            }
                                        }
                                    });

                        }
                    }


                }
            }
        });


        mActivityOtpBinding.resendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mProgressDialog.setMessage(Constant.PROGRESS_DIALOG_MESSAGE);
                mProgressDialog.show();
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(v.getApplicationWindowToken(), 0);

                if (checkConnection()) {
                    PhoneAuthProvider.getInstance().verifyPhoneNumber(
                            "+91" + getIntent().getStringExtra(Constant.PHONE_NO),
                            60,
                            TimeUnit.SECONDS,
                            OtpActivity.this,
                            new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {


                                @Override
                                public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
                                    mProgressDialog.dismiss();
                                }

                                @Override
                                public void onVerificationFailed(@NonNull FirebaseException e) {
//                            Toast.makeText(OtpActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                                    Toast.makeText(OtpActivity.this, "please valid OTP", Toast.LENGTH_SHORT).show();
                                    mProgressDialog.dismiss();


                                }

                                @Override
                                public void onCodeSent(@NonNull String newVerificationID, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {

                                    verificationId = newVerificationID;
                                    Toast.makeText(OtpActivity.this, "OTP sent", Toast.LENGTH_SHORT).show();
                                    mProgressDialog.dismiss();


//
                                }
                            }
                    );
                }

            }
        });

    }


    private void binding() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mActivityOtpBinding = DataBindingUtil.setContentView(this, R.layout.activity_otp);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);

        mProgressDialog = new ProgressDialog(this, R.style.ProgressDialogTheme);

    }
}


//
//
//
//
//    private void clickEvent() {
//
//        String phone_no = getIntent().getStringExtra(Constant.PHONE_NO);
//        sentVerification(phone_no);
//
//
//        mActivityOtpBinding.btnLogin.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//
//
//
//
//                String otp = mActivityOtpBinding.edtOtp.getText().toString();
//
//                if(otp.isEmpty() || otp.length() < 6){
//                    mActivityOtpBinding.edtOtp.setError("OTP is required");
//                    mActivityOtpBinding.edtOtp.requestFocus();
//                    return;
//                }
//
//                verifyCode(otp);
//
//
//            }
//        });
//    }
//
//    private void verifyCode(String otp) {
//
//        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId,otp);
//        signInWithCredential(credential);
//    }
//
//    private void signInWithCredential(PhoneAuthCredential credential) {
//        mAuth.signInWithCredential(credential)
//                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
//                    @Override
//                    public void onComplete(@NonNull Task<AuthResult> task) {
//                        if(task.isSuccessful()){
//                            Intent intent = new Intent(OtpActivity.this, RegistrationTemporayScreenActivity.class);
//                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                            startActivity(intent);
//                        }else {
//                            Toast.makeText(OtpActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                });
//    }
//
//
//    private void sentVerification(String phone_no) {
//
//        PhoneAuthProvider.getInstance().verifyPhoneNumber(
//                phone_no,
//                60,
//                TimeUnit.SECONDS,
//                TaskExecutors.MAIN_THREAD,
//                mCallback
//        );
//    }
//
//    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallback = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
//        @Override
//        public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
//
//            String otp = phoneAuthCredential.getSmsCode();
//            if(otp != null){
//                mProgressBar.setVisibility(View.VISIBLE);
//                verifyCode(otp);
//            }
//
//        }
//
//        @Override
//        public void onVerificationFailed(@NonNull FirebaseException e) {
//            Toast.makeText(OtpActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
//        }
//    };