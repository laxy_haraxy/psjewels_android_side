package com.psjewels.activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.psjewels.R;
import com.psjewels.adapter.MyOrderAdapter;
import com.psjewels.databinding.ActivityMyOrderBinding;
import com.psjewels.databinding.DialogBankDetailsBinding;
import com.psjewels.model.MyOrder;
import com.psjewels.utils.Constant;


import java.util.Calendar;

public class MyOrderActivity extends AppCompatActivity {
    ActivityMyOrderBinding mActivityMyOrderBinding;
    CollectionReference mCollectionReferenceMyOrder;
    Query mQuery;
    FirebaseUser mFirebaseUser;
    MyOrderAdapter mMyOrderAdapter;

    private int mYear, mMonth, mDay;
    String currentDate;






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding();
        checkConnection();
        initDb();
        getData();
        dateWiseOrderShow();
        badgeCount();
        clickEvents();
        back();


        bankDetails();

    }

    private void bankDetails() {

        mActivityMyOrderBinding.iconBankDeatils.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final DialogBankDetailsBinding mDialogBankDetailsBinding;
                final Dialog dialog = new Dialog(MyOrderActivity.this,R.style.MyAlertDialogTheme);
                mDialogBankDetailsBinding = DataBindingUtil.inflate(LayoutInflater.from(MyOrderActivity.this), R.layout.dialog_bank_details, null, false);
                mDialogBankDetailsBinding.imgclose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                dialog.setContentView(mDialogBankDetailsBinding.getRoot());
                dialog.show();

            }
        });
    }

    private void back() {
        mActivityMyOrderBinding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void clickEvents() {

        mActivityMyOrderBinding.iconCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),CartActivity.class));

            }
        });
//        mActivityMyOrderBinding.iconOrderDeatils.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                Toast.makeText(MyOrderActivity.this, "\uD83D\uDE0A", Toast.LENGTH_SHORT).show();
//            }
//        });
        mActivityMyOrderBinding.iconBankDeatils.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        mActivityMyOrderBinding.iconHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });
    }

    private void badgeCount() {

        if (checkConnection()) {


            mQuery.addSnapshotListener(new EventListener<QuerySnapshot>() {
                @Override
                public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                    if (queryDocumentSnapshots != null) {
                        int count = queryDocumentSnapshots.size();
//                            Toast.makeText(ProductListActivity.this, "count: " + count, Toast.LENGTH_SHORT).show();
                        String counts = String.valueOf(count);
                        counts.toString();
                        mActivityMyOrderBinding.totalCartItem.setText(counts);


                        if (count == 0) {
                            mActivityMyOrderBinding.emptyView.setVisibility(View.VISIBLE);
//                            mActivityMyOrderBinding.recyclerViewMyOrder.setVisibility(View.GONE);
                        } else {
                            mActivityMyOrderBinding.emptyView.setVisibility(View.GONE);
//                            mActivityMyOrderBinding.recyclerViewMyOrder.setVisibility(View.VISIBLE);

                        }
                    }
                }
            });
        }
    }

    private void dateWiseOrderShow() {

        mActivityMyOrderBinding.date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(MyOrderActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                monthOfYear = monthOfYear + 1;

                                String monthConverted = "" + monthOfYear;
                                if (monthOfYear < 10) {
                                    monthConverted = "0" + monthConverted;
                                }
                                String daysConverted = "" + dayOfMonth;

                                if (dayOfMonth < 10) {
                                    daysConverted = "0" + daysConverted;
                                }

                                currentDate = daysConverted + "-" + monthConverted + "-" + year;

//                                        Toast.makeText(MyOrderActivity.this, "asasd :- " + currentDate, Toast.LENGTH_SHORT).show();

//                                        if(currentDate != null){
//                                            mActivityMyOrderBinding.btnClear.setVisibility(View.VISIBLE);
//
//
//                                        }

                                getDataDateWise();
                            }
                        }, mYear, mMonth, mDay);
                mActivityMyOrderBinding.date.setVisibility(View.GONE);
                mActivityMyOrderBinding.close.setVisibility(View.VISIBLE);
                mActivityMyOrderBinding.close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(MyOrderActivity.this, MyOrderActivity.class));
                        overridePendingTransition(0, 0);
                        overridePendingTransition(0, 0);
                        overridePendingTransition(0, 0);
                        finish();
                    }
                });

                datePickerDialog.show();


            }
        });

    }

    private void  getDataDateWise() {

//            Toast.makeText(this, "getDataasdasd ", Toast.LENGTH_SHORT).show();

        Query query = mQuery.whereEqualTo(Constant.ORDER_DATE_KEY, currentDate);
        FirestoreRecyclerOptions<MyOrder> mOrderDetailsFirestoreRecyclerOptions = new FirestoreRecyclerOptions
                .Builder<MyOrder>()
                .setQuery(query, MyOrder.class).build();
        mMyOrderAdapter = new MyOrderAdapter(mOrderDetailsFirestoreRecyclerOptions, this);
        mActivityMyOrderBinding.recyclerViewMyOrder.setLayoutManager(new LinearLayoutManager(this));
        mActivityMyOrderBinding.recyclerViewMyOrder.setAdapter(mMyOrderAdapter);


        query.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if (queryDocumentSnapshots != null) {

                    if (queryDocumentSnapshots.size() > 0) {

                        mActivityMyOrderBinding.recyclerViewMyOrder.setVisibility(View.VISIBLE);
                        mActivityMyOrderBinding.emptyView.setVisibility(View.GONE);

                        mActivityMyOrderBinding.orderInfoCount.setVisibility(View.VISIBLE);
                        mActivityMyOrderBinding.tagLine.setVisibility(View.VISIBLE);



                    }
                    else
                    {
                        mActivityMyOrderBinding.emptyView.setVisibility(View.VISIBLE);
                        mActivityMyOrderBinding.recyclerViewMyOrder.setVisibility(View.GONE);

                        mActivityMyOrderBinding.orderInfoCount.setVisibility(View.GONE);
                        mActivityMyOrderBinding.tagLine.setVisibility(View.GONE);


                    }
                }
                else {
//                    Toast.makeText(MyOrderActivity.this, "snap null", Toast.LENGTH_SHORT).show();

                    mActivityMyOrderBinding.emptyView.setVisibility(View.VISIBLE);
                    mActivityMyOrderBinding.recyclerViewMyOrder.setVisibility(View.GONE);

                    mActivityMyOrderBinding.orderInfoCount.setVisibility(View.GONE);
                    mActivityMyOrderBinding.tagLine.setVisibility(View.GONE);

                }
            }
        });


        onStart();


    }

    public boolean checkConnection() {

        ConnectivityManager manager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = manager.getActiveNetworkInfo();

        if (null != activeNetwork) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
//                Toast.makeText(this, "Wifi Enabled", Toast.LENGTH_SHORT).show();
                return true;
            }
            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
//                Toast.makeText(this, "Data Network Enabled", Toast.LENGTH_SHORT).show();
                return true;
            }
        } else {
            Toast.makeText(this, "No Internet Connection", Toast.LENGTH_SHORT).show();
            return false;
        }
        return false;
    }

    private void getData() {



//        Toast.makeText(this, "getData", Toast.LENGTH_SHORT).show();
        FirestoreRecyclerOptions<MyOrder> mMyOrderFirestoreRecyclerOptions = new FirestoreRecyclerOptions
                .Builder<MyOrder>()
                .setQuery(mQuery.orderBy(Constant.TIME_STAMP_KEY, Query.Direction.DESCENDING), MyOrder.class).build();
        mMyOrderAdapter = new MyOrderAdapter(mMyOrderFirestoreRecyclerOptions, this);
//        mActivityMyOrderBinding.recyclerViewMyOrder.setLayoutManager(new LinearLayoutManager(this));
        mActivityMyOrderBinding.recyclerViewMyOrder.setLayoutManager(new GridLayoutManager(this,2));
        mActivityMyOrderBinding.recyclerViewMyOrder.setAdapter(mMyOrderAdapter);
        checkEmptyView(mQuery);
    }

    private void initDb() {
        mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        mCollectionReferenceMyOrder = FirebaseFirestore.getInstance().collection(Constant.ORDER_LIST_REF);
        mQuery = mCollectionReferenceMyOrder.whereEqualTo(Constant.USER_ID_KEY,mFirebaseUser.getUid());
    }


    private void checkEmptyView(Query mQuery) {
        mQuery.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if (queryDocumentSnapshots != null) {
                    if (queryDocumentSnapshots.size() > 0) {
                        mActivityMyOrderBinding.emptyView.setVisibility(View.GONE);
                        mActivityMyOrderBinding.recyclerViewMyOrder.setVisibility(View.VISIBLE);
                    }else {

                        mActivityMyOrderBinding.emptyView.setVisibility(View.VISIBLE);
                        mActivityMyOrderBinding.recyclerViewMyOrder.setVisibility(View.GONE);
                    }
                } else {

//                    mActivityMyOrderBinding.emptyView.setVisibility(View.VISIBLE);
//                    mActivityMyOrderBinding.recyclerViewMyOrder.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        mMyOrderAdapter.startListening();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mMyOrderAdapter.stopListening();
    }

    private void binding() {

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mActivityMyOrderBinding = DataBindingUtil.setContentView(this, R.layout.activity_my_order);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,WindowManager.LayoutParams.FLAG_SECURE);

    }


}
