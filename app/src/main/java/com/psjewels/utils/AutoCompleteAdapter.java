package com.psjewels.utils;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.psjewels.R;
import com.psjewels.activity.CategoryWiseProductShowActivity;
import com.psjewels.databinding.ActivityCategoryWiseProductShowBinding;
import com.psjewels.model.ProductDeatils;
import com.psjewels.model.SearchKeyword;


import java.util.ArrayList;
import java.util.List;

public class AutoCompleteAdapter extends ArrayAdapter<ProductDeatils> {

    private List<ProductDeatils> subCategoryListFull;

    ActivityCategoryWiseProductShowBinding mActivityCategoryWiseProductShowBinding;




    public AutoCompleteAdapter(CategoryWiseProductShowActivity context, List<ProductDeatils> subCategoryList) {
        super(context,0,subCategoryList);
    }


    @NonNull
    @Override
    public Filter getFilter() {
        return subCategotyFilter;
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.custom_searchview,parent,false);
        }

        TextView textViewName = convertView.findViewById(R.id.text_view_name);


        ProductDeatils subCategory = getItem(position);


        if(subCategory != null){




                        String data="";
                            String docId = subCategory.getProduct_id();
                            for (String keyWord : subCategory.getKeyword_list_name()) {
                                data += "\n-" + keyWord;
                            }

//                    data += "\n\n";

                            Log.d("asd","asd:- " + data);
                            Log.d("asd","docID:- " + docId);






            textViewName.setText(subCategory.getPro_name());
        }


        return convertView;
    }

    private Filter subCategotyFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            List<ProductDeatils> suggestions = new ArrayList<>();

            if(constraint == null || constraint.length() == 0){
                suggestions.addAll(subCategoryListFull);
            }else {
                String filterPatten = constraint.toString().toLowerCase().trim();

                for (ProductDeatils item : subCategoryListFull){
                    if(item.getPro_name().toLowerCase().contains(filterPatten)){
                        suggestions.add(item);
                    }
                }
            }


            results.values = suggestions;
            results.count = suggestions.size();

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            clear();
            addAll((List)results.values);
            notifyDataSetChanged();
        }

        @Override
        public CharSequence convertResultToString(Object resultValue) {
            return ((SearchKeyword) resultValue).getSearch_keyword();
        }
    };

}
//package com.psjewels.utils;
//
//import android.content.Context;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ArrayAdapter;
//import android.widget.Filter;
//import android.widget.TextView;
//
//import androidx.annotation.NonNull;
//import androidx.annotation.Nullable;
//
//import com.google.firebase.firestore.QueryDocumentSnapshot;
//import com.psjewels.R;
//import com.psjewels.databinding.ActivityCategoryWiseProductShowBinding;
//import com.psjewels.model.ProductDeatils;
//
//
//import java.util.ArrayList;
//import java.util.List;
//
//public class AutoCompleteAdapter extends ArrayAdapter<ProductDeatils> {
//
//    private List<ProductDeatils> subCategoryListFull;
//
//    ActivityCategoryWiseProductShowBinding mActivityCategoryWiseProductShowBinding;
//
//
//    public AutoCompleteAdapter(@NonNull Context context, @NonNull List<ProductDeatils> subCategoryList) {
//        super(context, 0, subCategoryList);
//
//        subCategoryListFull = new ArrayList<>(subCategoryList);
//    }
//
//
//    @NonNull
//    @Override
//    public Filter getFilter() {
//        return subCategotyFilter;
//    }
//
//
//    @NonNull
//    @Override
//    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
//        if(convertView == null){
//            convertView = LayoutInflater.from(getContext()).inflate(R.layout.custom_searchview,parent,false);
//        }
//
//        TextView textViewName = convertView.findViewById(R.id.text_view_name);
//
//
//        ProductDeatils subCategory = getItem(position);
//
//
//        if(subCategory != null){
//
//
//
//
//                        String data="";
//                            String docId = subCategory.getProduct_id();
//                            for (String keyWord : subCategory.getKeyword_list_name()) {
//                                data += "\n-" + keyWord;
//                            }
//
////                    data += "\n\n";
//
//                            Log.d("asd","asd:- " + data);
//                            Log.d("asd","docID:- " + docId);
//
//
//
//
//
//
//            textViewName.setText(data);
//        }
//
//
//        return convertView;
//    }
//
//    private Filter subCategotyFilter = new Filter() {
//        @Override
//        protected FilterResults performFiltering(CharSequence constraint) {
//            FilterResults results = new FilterResults();
//            List<ProductDeatils> suggestions = new ArrayList<>();
//
//            if(constraint == null || constraint.length() == 0){
//                suggestions.addAll(subCategoryListFull);
//            }else {
//                String filterPatten = constraint.toString().toLowerCase().trim();
//
//                for (ProductDeatils item : subCategoryListFull){
//                    if(item.getPro_name().toLowerCase().contains(filterPatten)){
//                        suggestions.add(item);
//                    }
//                }
//            }
//
//
//            results.values = suggestions;
//            results.count = suggestions.size();
//
//            return results;
//        }
//
//        @Override
//        protected void publishResults(CharSequence constraint, FilterResults results) {
//
//            clear();
//            addAll((List)results.values);
//            notifyDataSetChanged();
//        }
//
//        @Override
//        public CharSequence convertResultToString(Object resultValue) {
//            return ((ProductDeatils) resultValue).getPro_name();
//        }
//    };
//
//}
