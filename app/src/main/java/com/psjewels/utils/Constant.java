package com.psjewels.utils;

public class Constant {


    public static final String BANNER_LIST_COLLE = "banner_img_list";
    public static final String TAG = "tag_log";

    public static final String TIME_STAMP_KEY = "time_stamp";

    public static final String CAT_LIST_REF = "cat_list";
    public static final String SUB_CAT_LIST_REF = "sub_cat_list";
    public static final String SUB_CAT_NAME = "sub_cat_name";
    public static final String SUB_CAT_ID = "sub_cat_id";

    public static final String PRODUCT_LIST_REF = "product_list";
    public static final String SEARCH_KEYWORD_LIST_COLL = "search_keyword_list";
    public static final String SEARCH_KEYWORD_ARRAY = "keyword_list_name";

    public static final String PRODUCT_QUANTITY = "product_quantity";


    public static final String FRONT_IMG = "front_img";
    public static final String BACK_IMG = "back_img";

    public static final String PUSH_KEY = "push_key";
    public static final String IMG_LIST = "img_list";







    public static final String PHONE_NO = "phone_no";
    public static final String VERIFICATION_ID = "verificationId";
    public static final String PROGRESS_DIALOG_MESSAGE = "Please Wait...";
    public static final String USER_LIST_REF = "user_list";



    public static final String CREATED_ACC_KEY = "token_id";
    public static final String TOKEN_ID_KEY = "token_id";
    public static final String  USER_ID_KEY= "user_id";
    public static final String  COMPANY_NAME_KEY= "company_name";
    public static final String  CONTACT_PERSON_KEY= "contact_person";
    public static final String  PHONE_NOUMBER_KEY= "phone_number";
    public static final String  ADDRESS_KEY= "address";
    public static final String  STATE_KEY= "state";
    public static final String  CITY_KEY= "city";
    public static final String  DATE= "date";
    public static final String  PHONE_NUMBER_KEY = "phone_number";

    public static final String VISITING_CARD = "visiting_cards_img";
    public static final String IMG_KEY = "img";
    public static final String IMG_COLLECTION_CARDS = "card_img";
    public static final String USER_DETAILS = "user_details";
    public static final String CARDS_IMG_LIST_STROAGE = "card_img_list";

    public static final String BUSINESS_CARDS = "Business cards";



    public static final String CAT_ID_KEY ="cat_id" ;
    public static final String CAT_NAME ="cat_name" ;


    public static final String MY_CART_REF = "my_cart";
    public static final String ITEM_LIST_REF = "item_list";


    public static final String PRODUCT_QUANTITY_KEY ="product_quantity";


    public static final String ORDER_LIST_REF = "order_list";



    public static final String PRO_TOTAL_PRICE = "pro_total_price";


    public static final String ORDER_DATE_KEY = "order_date";














}
