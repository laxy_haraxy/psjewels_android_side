package com.psjewels.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.psjewels.R;
import com.psjewels.model.SearchKeyword;


import java.util.ArrayList;
import java.util.List;

public class AutoCompleteSubCatNameAdapter extends ArrayAdapter<SearchKeyword> {



        private List<SearchKeyword> subCategoryListFull;

//        ActivitySearchBinding mActivitySearchBinding;


        public AutoCompleteSubCatNameAdapter(@NonNull Context context, @NonNull List<SearchKeyword> subCategoryList) {
            super(context, 0, subCategoryList);

            subCategoryListFull = new ArrayList<>(subCategoryList);
        }


        @NonNull
        @Override
        public Filter getFilter() {
            return subCategotyFilter;
        }


        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            if(convertView == null){
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.custom_searchview,parent,false);
            }

            TextView textViewName = convertView.findViewById(R.id.text_view_name);


            SearchKeyword subCategory = getItem(position);


            if(subCategory != null){

                textViewName.setText(subCategory.getSearch_keyword());
            }


            return convertView;
        }

        private Filter subCategotyFilter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                List<SearchKeyword> suggestions = new ArrayList<>();

                if(constraint == null || constraint.length() == 0){
                    suggestions.addAll(subCategoryListFull);
                }else {
                    String filterPatten = constraint.toString().toLowerCase().trim();

                    for (SearchKeyword item : subCategoryListFull){
                        if(item.getSearch_keyword().toLowerCase().contains(filterPatten)){
                            suggestions.add(item);
                        }
                    }
                }


                results.values = suggestions;
                results.count = suggestions.size();

                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

                clear();
                addAll((List)results.values);
                notifyDataSetChanged();
            }

            @Override
            public CharSequence convertResultToString(Object resultValue) {
                return ((SearchKeyword) resultValue).getSearch_keyword();
            }
        };


    }
