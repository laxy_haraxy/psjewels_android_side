package com.psjewels.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.psjewels.R;
import com.psjewels.model.ProductDeatils;

import java.util.ArrayList;
import java.util.List;

public class AutoCompleteSearchAdapterForSearchActivity extends ArrayAdapter<ProductDeatils> {



        private List<ProductDeatils> subCategoryListFull;

//        ActivitySearchBinding mActivitySearchBinding;


        public AutoCompleteSearchAdapterForSearchActivity(@NonNull Context context, @NonNull List<ProductDeatils> subCategoryList) {
            super(context, 0, subCategoryList);

            subCategoryListFull = new ArrayList<>(subCategoryList);
        }


        @NonNull
        @Override
        public Filter getFilter() {
            return subCategotyFilter;
        }


        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            if(convertView == null){
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.custom_searchview,parent,false);
            }

            TextView textViewName = convertView.findViewById(R.id.text_view_name);


            ProductDeatils subCategory = getItem(position);


            if(subCategory != null){

                textViewName.setText(subCategory.getPro_concet_name());
            }


            return convertView;
        }

        private Filter subCategotyFilter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                List<ProductDeatils> suggestions = new ArrayList<>();

                if(constraint == null || constraint.length() == 0){
                    suggestions.addAll(subCategoryListFull);
                }else {
                    String filterPatten = constraint.toString().toLowerCase().trim();

                    for (ProductDeatils item : subCategoryListFull){
                        if(item.getPro_concet_name().toLowerCase().contains(filterPatten)){
                            suggestions.add(item);
                        }
                    }
                }


                results.values = suggestions;
                results.count = suggestions.size();

                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

                clear();
                addAll((List)results.values);
                notifyDataSetChanged();
            }

            @Override
            public CharSequence convertResultToString(Object resultValue) {
                return ((ProductDeatils) resultValue).getPro_concet_name();
            }
        };


    }
