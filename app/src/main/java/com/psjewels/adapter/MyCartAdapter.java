package com.psjewels.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.psjewels.R;
import com.psjewels.databinding.DialogAddNotesBinding;
import com.psjewels.databinding.DialogProductOpenBinding;
import com.psjewels.databinding.RowLayoutMyCartBinding;
import com.psjewels.model.MyCart;
import com.psjewels.utils.Constant;
import com.squareup.picasso.Picasso;

public class MyCartAdapter extends FirestoreRecyclerAdapter<MyCart, MyCartAdapter.MyCartViewHolder> {
    private static final String TAG = "mycart";
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private FirebaseUser mFirebaseUser;
    private CollectionReference mCollectionReference;
    private DocumentReference mDocumentReferenceMyCart;


    /**
     * Create a new RecyclerView adapter that listens to a Firestore Query.  See {@link
     * FirestoreRecyclerOptions} for configuration options.
     *
     * @param options
     * @param
     */

    public MyCartAdapter(@NonNull FirestoreRecyclerOptions<MyCart> options, Context mContext) {
        super(options);
        this.mContext = mContext;

    }

    @Override
    public void onDataChanged() {
        super.onDataChanged();
        notifyDataSetChanged();
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onBindViewHolder(@NonNull final MyCartViewHolder myCartViewHolder, final int i, @NonNull final MyCart myCart) {
        mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        mCollectionReference = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF);
        mDocumentReferenceMyCart = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid());

//        myCartViewHolder.mRowLayoutMyCartBinding.proQty.setText(myCart.getProduct_quantity());
        myCartViewHolder.mRowLayoutMyCartBinding.proName.setText(myCart.getPro_name());
        myCartViewHolder.mRowLayoutMyCartBinding.proId.setText(myCart.getPro_code());
        myCartViewHolder.mRowLayoutMyCartBinding.proWT.setText(myCart.getPro_weight());
        myCartViewHolder.mRowLayoutMyCartBinding.proSize.setText(myCart.getPro_size());
        myCartViewHolder.mRowLayoutMyCartBinding.psc.setText(String.valueOf(myCart.getProduct_quantity()));

        myCartViewHolder.mRowLayoutMyCartBinding.path.setText( myCart.getCat_name() + " > " + myCart.getPro_name());


        myCartViewHolder.mRowLayoutMyCartBinding.txtNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final DialogAddNotesBinding mDialogAddNotesBinding;
                final Dialog dialog = new Dialog(mContext,R.style.MyAlertDialogTheme);
                mDialogAddNotesBinding = DataBindingUtil.inflate(LayoutInflater.from(mContext), R.layout.dialog_add_notes, null, false);


                mDialogAddNotesBinding.imgclose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        InputMethodManager imm1 = (InputMethodManager) mContext.getSystemService(Activity.INPUT_METHOD_SERVICE);
                        imm1.hideSoftInputFromWindow(v.getWindowToken(), 0);
                        dialog.dismiss();


                    }
                });
                mDialogAddNotesBinding.edtNotes.setText(myCart.getNote());
                mDialogAddNotesBinding.btnSubmit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF)
                                .document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF)
                                .document(myCart.getProduct_id()).update("notes", mDialogAddNotesBinding.edtNotes.getText().toString());
                        Toast.makeText(mContext, "saved", Toast.LENGTH_SHORT).show();

                        InputMethodManager imm1 = (InputMethodManager) mContext.getSystemService(Activity.INPUT_METHOD_SERVICE);
                        imm1.hideSoftInputFromWindow(v.getWindowToken(), 0);
                        dialog.dismiss();



                    }
                });

                dialog.setContentView(mDialogAddNotesBinding.getRoot());
                dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
                dialog.show();
            }
        });

        Picasso.get().load(myCart.getImg()).placeholder(R.drawable.placeholder).fit().centerInside().into(myCartViewHolder.mRowLayoutMyCartBinding.productImg);

        myCartViewHolder.mRowLayoutMyCartBinding.close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


//                Toast.makeText(mContext, "asd :- " +getSnapshots().size(), Toast.LENGTH_SHORT).show();

                if(getSnapshots().size() == 1){




                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

                    builder.setTitle("Confirm To clear cart?");
                    builder.setMessage("Sure you wants to remove this product from cart?");

                    builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int which) {
                            // Do nothing but close the dialog



                            mCollectionReference.document(getSnapshots().getSnapshot(i).getId()).delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                }
                            });

                            dialog.dismiss();
                        }
                    });

                    builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            // Do nothing
                            dialog.dismiss();
                        }
                    });

                    AlertDialog alert = builder.create();
                    alert.show();




                }else{
                mCollectionReference.document(getSnapshots().getSnapshot(i).getId()).delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                    }
                });

                }



            }


        });





        //---------------------qty--update--with--plus--&--minus--&---usingEditText----[start]------------------------
        myCartViewHolder.mRowLayoutMyCartBinding.psc.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                myCartViewHolder.mRowLayoutMyCartBinding.minus.setVisibility(View.GONE);
                myCartViewHolder.mRowLayoutMyCartBinding.plus.setVisibility(View.GONE);

                myCartViewHolder.mRowLayoutMyCartBinding.btnUpdateQty.setVisibility(View.VISIBLE);


                myCartViewHolder.mRowLayoutMyCartBinding.btnUpdateQty.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

if(myCartViewHolder.mRowLayoutMyCartBinding.psc.getText().toString().length()==0){
    Toast.makeText(mContext, "Please enter qty", Toast.LENGTH_SHORT).show();
}else {

    int updateQty = Integer.parseInt( myCartViewHolder.mRowLayoutMyCartBinding.psc.getText().toString());
    InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Activity.INPUT_METHOD_SERVICE);
    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

    if (updateQty == 0) {
        Toast.makeText(mContext, "Please enter zero above quantity ", Toast.LENGTH_SHORT).show();
        InputMethodManager imm1 = (InputMethodManager) mContext.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm1.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }
    else{
        FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF).document(myCart.getProduct_id()).update(Constant.PRODUCT_QUANTITY_KEY, updateQty);
        Toast.makeText(mContext, "Updated", Toast.LENGTH_SHORT).show();
        myCartViewHolder.mRowLayoutMyCartBinding.minus.setVisibility(View.VISIBLE);
        myCartViewHolder.mRowLayoutMyCartBinding.plus.setVisibility(View.VISIBLE);
        myCartViewHolder.mRowLayoutMyCartBinding.btnUpdateQty.setVisibility(View.GONE);
        InputMethodManager imm1 = (InputMethodManager) mContext.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm1.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }
}
                        
                    }
                });
                return false;
            }
        });
        //---------------------qty--update--with--plus--&--minus--&---usingEditText----[end]------------------------

        plusAndMinusQuantity(myCartViewHolder, i, myCart);
    }

    private void plusAndMinusQuantity(MyCartViewHolder myCartHolder, final int i, final MyCart myCart) {
        myCartHolder.mRowLayoutMyCartBinding.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                int productQuantity = myCart.getProduct_quantity();
                productQuantity = productQuantity - 1;
                mCollectionReference.document(getSnapshots().getSnapshot(i).getId()).update(Constant.PRODUCT_QUANTITY_KEY, productQuantity);

                if (productQuantity == 0) {
                    //REMOVE FROM CART
                    final int finalProductQuantity = productQuantity;
                    mCollectionReference.document(getSnapshots().getSnapshot(i).getId()).delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
//                            mCollectionReference.document(getSnapshots().getSnapshot(i).getId()).update(Constant.PRODUCT_QUANTITY_KEY, finalProductQuantity);

                        }
                    });

                } else {


                }
            }
        });

        myCartHolder.mRowLayoutMyCartBinding.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int productQuantity = myCart.getProduct_quantity();
                productQuantity = productQuantity + 1;
                mCollectionReference.document(getSnapshots().getSnapshot(i).getId()).update(Constant.PRODUCT_QUANTITY_KEY, productQuantity);
            }
        });
    }


    @NonNull
    @Override
    public MyCartViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (mLayoutInflater == null) {
            mLayoutInflater = LayoutInflater.from(parent.getContext());
        }
        RowLayoutMyCartBinding myCartBinding = DataBindingUtil.inflate(mLayoutInflater, R.layout.row_layout_my_cart, parent, false);
        return new MyCartViewHolder(myCartBinding);
    }

    class MyCartViewHolder extends RecyclerView.ViewHolder {

        RowLayoutMyCartBinding mRowLayoutMyCartBinding;

        MyCartViewHolder(@NonNull RowLayoutMyCartBinding itemView) {
            super(itemView.getRoot());
            mRowLayoutMyCartBinding = itemView;
        }
    }
}
