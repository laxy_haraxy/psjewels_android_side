package com.psjewels.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.psjewels.R;
import com.psjewels.model.BannerImg;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ViewpagerAdapter extends PagerAdapter {

    private List<BannerImg> models;
    private LayoutInflater layoutInflater;
    private Context context;
    private int pos = 0;

    public ViewpagerAdapter(List<BannerImg> models, Context context) {
        this.models = models;
        this.context = context;
    }

    public boolean onTouchEvent(MotionEvent event) {
        // Never allow swiping to switch between pages
        return false;
    }

    @Override
    public int getCount() {
        return models.size();

//        return Integer.MAX_VALUE;

    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.viewpager_item, container, false);
        BannerImg mBannerImg = models.get(pos);
        ImageView mImageView = view.findViewById(R.id.images);
        Picasso.get().load(mBannerImg.getImage_url()).fit().placeholder(R.drawable.placeholder).into(mImageView);


        //-------------
        int finalHeight, finalWidth;

        ViewTreeObserver vto = mImageView.getViewTreeObserver();
        vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            public boolean onPreDraw() {
                mImageView.getViewTreeObserver().removeOnPreDrawListener(this);


                Log.d("asdasd", "finalHeight: "+ mImageView.getMeasuredHeight());
                Log.d("asdasd", "finalWidth: "+  mImageView.getMeasuredWidth());
                return true;
            }
        });
        //-------------


//        Log.d(Constant.MY_TAG, "instantiateItem: "+mBannerImg.getImage_url());

        container.addView(view, 0);
        if (pos >= models.size() - 1)
            pos = 0;
        else
            ++pos;
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }


}