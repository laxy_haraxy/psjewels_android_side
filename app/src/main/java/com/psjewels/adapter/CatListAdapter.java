package com.psjewels.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.psjewels.R;
import com.psjewels.activity.CategoryWiseProductShowActivity;
import com.psjewels.activity.DashboardActivity;
import com.psjewels.activity.RegistrationTemporayScreenActivity;
import com.psjewels.activity.SubCatListActivity;
import com.psjewels.databinding.RowLayoutCatListBinding;
import com.psjewels.model.AddCategory;
import com.psjewels.model.AddUser;
import com.psjewels.utils.Constant;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;


public class CatListAdapter extends FirestoreRecyclerAdapter<AddCategory, CatListAdapter.CatViewHolder> {

    private Context mContext;
    private LayoutInflater mLayoutInflater;

    /**
     * Create a new RecyclerView adapter that listens to a Firestore Query.  See {@link
     * FirestoreRecyclerOptions} for configuration options.
     *
     * @param options
     */

    public CatListAdapter(@NonNull FirestoreRecyclerOptions<AddCategory> options, Context mContext) {
        super(options);
        this.mContext = mContext;
    }




    @Override
    protected void onBindViewHolder(@NonNull CatViewHolder catViewHolder, final int i, @NonNull final AddCategory addCategory) {
        catViewHolder.mRowLayoutCatListBinding.txtCatName.setText(addCategory.getCat_name());



        if(addCategory.getCat_img() != null){

            Picasso.get().load(addCategory.getCat_img()).placeholder(R.drawable.placeholder)
                    .fit().centerInside()
                    .into(catViewHolder.mRowLayoutCatListBinding.imgViewCat, new Callback() {
                        @Override
                        public void onSuccess() {
                            Log.d(Constant.TAG, " picasso success:  ");

                        }

                        @Override
                        public void onError(Exception e) {
                            Log.d(Constant.TAG, " picasso onError: ERROR:  "+e.getLocalizedMessage());
                        }
                    });

        }


    catViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {

//            Toast.makeText(mContext, "asd "+getSnapshots().getSnapshot(i).getId(), Toast.LENGTH_SHORT).show();
//
//
//           FirebaseUser mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
//            DocumentReference mDocumentReference = FirebaseFirestore.getInstance().collection(Constant.USER_LIST_REF).document(mFirebaseUser.getUid());
//
//            mDocumentReference.addSnapshotListener(new EventListener<DocumentSnapshot>() {
//                @Override
//                public void onEvent(@Nullable DocumentSnapshot snapshot,
//                                    @Nullable FirebaseFirestoreException e) {
//                    if (e != null) {
//                        Log.w(Constant.TAG, "Listen failed.", e);
//                        return;
//                    }
//
//                    String source = snapshot != null && snapshot.getMetadata().hasPendingWrites()
//                            ? "Local" : "Server";
//
//                    if (snapshot != null && snapshot.exists()) {
//                        Log.d(Constant.TAG, source + " data: " + snapshot.getData());
//
//                        AddUser mAddUser = snapshot.toObject(AddUser.class);
////                    Toast.makeText(RegistrationTemporayScreenActivity.this, "status :- " +mAddUser.isStatus(), Toast.LENGTH_SHORT).show();
//
//
//
//                        if(mAddUser.isStatus() == true){
//
//                        }else {
////                            Toast.makeText(RegistrationTemporayScreenActivity.this, "Please wait for the admin access!", Toast.LENGTH_SHORT).show();
//                        }
//
//
//                    } else {
//                        Log.d(Constant.TAG, source + " data: null");
//                    }
//                }
//            });





            Intent mIntent = new Intent(mContext, SubCatListActivity.class);
            mIntent.putExtra(Constant.CAT_ID_KEY,getSnapshots().getSnapshot(i).getId());
            mIntent.putExtra(Constant.CAT_NAME,addCategory.getCat_name());

            mContext.startActivity(mIntent);
        }
    });


    }

    public boolean checkConnection() {

        ConnectivityManager manager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = manager.getActiveNetworkInfo();

        if (null != activeNetwork) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
//                Toast.makeText(this, "Wifi Enabled", Toast.LENGTH_SHORT).show();
                return true;
            }
            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
//                Toast.makeText(this, "Data Network Enabled", Toast.LENGTH_SHORT).show();
                return true;
            }
        } else {
            Toast.makeText(mContext, "No Internet Connection", Toast.LENGTH_SHORT).show();
            return false;
        }
        return false;
    }

    @NonNull
    @Override
    public CatViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (mLayoutInflater == null) {
            mLayoutInflater = LayoutInflater.from(parent.getContext());
        }
        RowLayoutCatListBinding mRowLayoutCatListBinding = DataBindingUtil.inflate(mLayoutInflater, R.layout.row_layout_cat_list, parent, false);
        return new CatViewHolder(mRowLayoutCatListBinding);


    }

    public class CatViewHolder extends RecyclerView.ViewHolder {
        RowLayoutCatListBinding mRowLayoutCatListBinding;

        public CatViewHolder(@NonNull RowLayoutCatListBinding itemView) {
            super(itemView.getRoot());
            mRowLayoutCatListBinding = itemView;
        }
    }


}
