package com.psjewels.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.psjewels.R;
import com.psjewels.activity.MyEditOrderProductListActivity;
import com.psjewels.activity.MyOrderActivity;
import com.psjewels.databinding.DialogAddNotesBinding;
import com.psjewels.databinding.RowLayoutMyEditOrderProductListBinding;
import com.psjewels.databinding.RowLayoutMyOrderProductListBinding;
import com.psjewels.model.MyCart;
import com.psjewels.utils.Constant;
import com.squareup.picasso.Picasso;

import static android.content.Context.INPUT_METHOD_SERVICE;


public class MyEditOrderProductListAdapter extends FirestoreRecyclerAdapter<MyCart, MyEditOrderProductListAdapter.MyOrderProductListHolder> {

    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private FirebaseUser mFirebaseUser;
    private String pushKey;
    private CollectionReference mCollectionReference;


    public MyEditOrderProductListAdapter(@NonNull FirestoreRecyclerOptions<MyCart> options, Context mContext, String pushKey) {
        super(options);
        this.mContext = mContext;
        this.pushKey = pushKey;


    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onBindViewHolder(@NonNull final MyOrderProductListHolder myOrderProductListHolder, final int i, @NonNull final MyCart myCart) {
        mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        mCollectionReference = FirebaseFirestore.getInstance().collection(Constant.ORDER_LIST_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF);

        myOrderProductListHolder.mRowLayoutMyEditOrderProductListBinding.productCountRow.setText(String.valueOf(myCart.getProduct_quantity()));
        myOrderProductListHolder.mRowLayoutMyEditOrderProductListBinding.psc.setText(String.valueOf(myCart.getProduct_quantity()));

        myOrderProductListHolder.mRowLayoutMyEditOrderProductListBinding.proWT.setText(myCart.getPro_weight());
        myOrderProductListHolder.mRowLayoutMyEditOrderProductListBinding.txtSize.setText(myCart.getPro_size());
        String productImg = myCart.getImg();
        myOrderProductListHolder.mRowLayoutMyEditOrderProductListBinding.proId.setText(myCart.getPro_code());
        myOrderProductListHolder.mRowLayoutMyEditOrderProductListBinding.path.setText(myCart.getCat_name() + " > " + myCart.getSub_cat_name());
        if (!TextUtils.isEmpty(productImg)) {
            Picasso.get().load(productImg).fit().centerInside().placeholder(R.drawable.placeholder).into(myOrderProductListHolder.mRowLayoutMyEditOrderProductListBinding.imageViewRowLayout);
        }
        myOrderProductListHolder.mRowLayoutMyEditOrderProductListBinding.txtNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final DialogAddNotesBinding mDialogAddNotesBinding;
                final Dialog dialog = new Dialog(mContext,R.style.MyAlertDialogTheme);
                mDialogAddNotesBinding = DataBindingUtil.inflate(LayoutInflater.from(mContext), R.layout.dialog_add_notes, null, false);


                mDialogAddNotesBinding.imgclose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        InputMethodManager imm1 = (InputMethodManager) mContext.getSystemService(Activity.INPUT_METHOD_SERVICE);
                        imm1.hideSoftInputFromWindow(v.getWindowToken(), 0);
                        dialog.dismiss();


                    }
                });
                mDialogAddNotesBinding.edtNotes.setText(myCart.getNote());
                mDialogAddNotesBinding.btnSubmit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF)
                                .document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF)
                                .document(myCart.getProduct_id()).update("notes", mDialogAddNotesBinding.edtNotes.getText().toString());
                        Toast.makeText(mContext, "saved", Toast.LENGTH_SHORT).show();

                        InputMethodManager imm1 = (InputMethodManager) mContext.getSystemService(Activity.INPUT_METHOD_SERVICE);
                        imm1.hideSoftInputFromWindow(v.getWindowToken(), 0);
                        dialog.dismiss();



                    }
                });

                dialog.setContentView(mDialogAddNotesBinding.getRoot());
                dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
                dialog.show();
            }
        });

        myOrderProductListHolder.mRowLayoutMyEditOrderProductListBinding.deleteOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FirebaseFirestore.getInstance().collection(Constant.ORDER_LIST_REF).document(pushKey).collection(Constant.ITEM_LIST_REF).document(myCart.getProduct_id()).delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(mContext, "Succefully Removed", Toast.LENGTH_SHORT).show();

                        if (getSnapshots().size() == 0) {
                            FirebaseFirestore.getInstance().collection(Constant.ORDER_LIST_REF).document(pushKey).delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    Log.d(Constant.TAG, "onSuccess: deleted");
                                }
                            });
                        }
                    }
                });

            }
        });

        myOrderProductListHolder.mRowLayoutMyEditOrderProductListBinding.psc.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                myOrderProductListHolder.mRowLayoutMyEditOrderProductListBinding.minus.setVisibility(View.GONE);
                myOrderProductListHolder.mRowLayoutMyEditOrderProductListBinding.plus.setVisibility(View.GONE);

                myOrderProductListHolder.mRowLayoutMyEditOrderProductListBinding.btnUpdateQty.setVisibility(View.VISIBLE);


                myOrderProductListHolder.mRowLayoutMyEditOrderProductListBinding.btnUpdateQty.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        int updateQty = Integer.parseInt(myOrderProductListHolder.mRowLayoutMyEditOrderProductListBinding.psc.getText().toString());
                        InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Activity.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                            if (updateQty == 0) {
                                Toast.makeText(mContext, "Please enter zero above quantity ", Toast.LENGTH_SHORT).show();
                                InputMethodManager imm1 = (InputMethodManager) mContext.getSystemService(Activity.INPUT_METHOD_SERVICE);
                                imm1.hideSoftInputFromWindow(v.getWindowToken(), 0);
                            }
                            else{
                                FirebaseFirestore.getInstance().collection(Constant.ORDER_LIST_REF).document(pushKey).collection(Constant.ITEM_LIST_REF).document(myCart.getProduct_id()).update(Constant.PRODUCT_QUANTITY_KEY, updateQty);
                                Toast.makeText(mContext, "Updated", Toast.LENGTH_SHORT).show();
                                myOrderProductListHolder.mRowLayoutMyEditOrderProductListBinding.minus.setVisibility(View.VISIBLE);
                                myOrderProductListHolder.mRowLayoutMyEditOrderProductListBinding.plus.setVisibility(View.VISIBLE);
                                myOrderProductListHolder.mRowLayoutMyEditOrderProductListBinding.btnUpdateQty.setVisibility(View.GONE);
                                InputMethodManager imm1 = (InputMethodManager) mContext.getSystemService(Activity.INPUT_METHOD_SERVICE);
                                imm1.hideSoftInputFromWindow(v.getWindowToken(), 0);
                            }
                        }
                });
                return false;
            }
        });


        plusAndMinusQuantity(myOrderProductListHolder, i, myCart);


    }

    private void plusAndMinusQuantity(MyOrderProductListHolder myOrderProductListHolder, final int i, final MyCart myCart) {
        myOrderProductListHolder.mRowLayoutMyEditOrderProductListBinding.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                int productQuantity = myCart.getProduct_quantity();
                productQuantity = productQuantity - 1;
                FirebaseFirestore.getInstance().collection(Constant.ORDER_LIST_REF).document(pushKey).collection(Constant.ITEM_LIST_REF).document(myCart.getProduct_id()).update(Constant.PRODUCT_QUANTITY_KEY, productQuantity);

                if (productQuantity == 0) {


                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

                    builder.setTitle("Confirm To clear ?");
                    builder.setMessage("Sure you wants to remove this product?");

                    builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int which) {
                            // Do nothing but close the dialog


                            FirebaseFirestore.getInstance().collection(Constant.ORDER_LIST_REF).document(pushKey).collection(Constant.ITEM_LIST_REF).document(myCart.getProduct_id()).delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    Toast.makeText(mContext, "Succefully Removed", Toast.LENGTH_SHORT).show();

                                    if (getSnapshots().size() == 0) {
                                        FirebaseFirestore.getInstance().collection(Constant.ORDER_LIST_REF).document(pushKey).delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                Log.d(Constant.TAG, "onSuccess: deleted");
                                            }
                                        });
                                    }
                                }
                            });

                            dialog.dismiss();
                        }
                    });

                    builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            // Do nothing
                            dialog.dismiss();
                        }
                    });

                    AlertDialog alert = builder.create();
                    alert.show();


                } else {


                }
            }
        });

        myOrderProductListHolder.mRowLayoutMyEditOrderProductListBinding.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int productQuantity = myCart.getProduct_quantity();
                productQuantity = productQuantity + 1;

//                Log.d(Constant.TAG, "sahil: ");
//                Log.d(Constant.TAG, "sahil: " + FirebaseFirestore.getInstance().collection(Constant.ORDER_LIST_REF).document(pushKey).collection(Constant.ITEM_LIST_REF).document(myCart.getProduct_id()));


                FirebaseFirestore.getInstance().collection(Constant.ORDER_LIST_REF).document(pushKey).collection(Constant.ITEM_LIST_REF).document(myCart.getProduct_id()).update(Constant.PRODUCT_QUANTITY_KEY, productQuantity);
            }
        });
    }


    @NonNull
    @Override
    public MyOrderProductListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (mLayoutInflater == null) {
            mLayoutInflater = LayoutInflater.from(parent.getContext());
        }
        RowLayoutMyEditOrderProductListBinding myCartBinding = DataBindingUtil.inflate(mLayoutInflater, R.layout.row_layout_my_edit_order_product_list, parent, false);
        return new MyOrderProductListHolder(myCartBinding);
    }

    public class MyOrderProductListHolder extends RecyclerView.ViewHolder {
        RowLayoutMyEditOrderProductListBinding mRowLayoutMyEditOrderProductListBinding;

        public MyOrderProductListHolder(@NonNull RowLayoutMyEditOrderProductListBinding itemView) {
            super(itemView.getRoot());
            mRowLayoutMyEditOrderProductListBinding = itemView;
        }
    }
}
