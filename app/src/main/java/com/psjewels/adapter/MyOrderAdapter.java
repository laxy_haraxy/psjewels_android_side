package com.psjewels.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.psjewels.R;
import com.psjewels.activity.CartActivity;
import com.psjewels.activity.CollectUserDetailsActivity;
import com.psjewels.activity.LoginScreenActivity;
import com.psjewels.activity.MyEditOrderProductListActivity;
import com.psjewels.activity.MyOrderActivity;
import com.psjewels.activity.MyOrderProductListActivity;
import com.psjewels.activity.RegistrationTemporayScreenActivity;
import com.psjewels.databinding.RowLayoutMyOrderBinding;
import com.psjewels.model.AddUser;
import com.psjewels.model.MyOrder;
import com.psjewels.model.ProductDeatils;
import com.psjewels.utils.Constant;

import java.util.Objects;


public class MyOrderAdapter extends FirestoreRecyclerAdapter<MyOrder, MyOrderAdapter.MyOrderViewHolder> {
    private Context mContext;
    private LayoutInflater mLayoutInflater;

    /**
     * Create a new RecyclerView adapter that listens to a Firestore Query.  See {@link
     * FirestoreRecyclerOptions} for configuration options.
     *
     * @param options
     */
    public MyOrderAdapter(@NonNull FirestoreRecyclerOptions<MyOrder> options, Context mContext) {
        super(options);
        this.mContext = mContext;
    }



    @SuppressLint("ResourceAsColor")
    @Override
    protected void onBindViewHolder(@NonNull final MyOrderAdapter.MyOrderViewHolder myOrderViewHolder, final int i, @NonNull final MyOrder myOrder) {

        myOrderViewHolder.mRowLayoutMyOrderBinding.txtData.setText(myOrder.getOrder_date());
        myOrderViewHolder.mRowLayoutMyOrderBinding.txtTime.setText(myOrder.getOrder_time());


        if (myOrder.isOrder_pending()) {
            myOrderViewHolder.mRowLayoutMyOrderBinding.txtOrderStatusMyOrder.setText("Pending");


        }


        if (myOrder.isOrder_accepted()) {
            myOrderViewHolder.mRowLayoutMyOrderBinding.txtOrderStatusMyOrder.setText("Accepted");
            myOrderViewHolder.mRowLayoutMyOrderBinding.txtOrderStatusMyOrder.setTextColor(ContextCompat.getColor(mContext, R.color.green));
            myOrderViewHolder.mRowLayoutMyOrderBinding.btnEdit.setVisibility(View.GONE);
            myOrderViewHolder.mRowLayoutMyOrderBinding.btnCancleOreder.setVisibility(View.GONE);
            myOrderViewHolder.mRowLayoutMyOrderBinding.btnViewOrder.setVisibility(View.VISIBLE);


        }



        myOrderViewHolder.mRowLayoutMyOrderBinding.txtOrderId.setText(String.valueOf(myOrder.getOrder_id()));


//-------------------------------------------------------------------------------------------------------------

        DocumentReference mDocumentReference = FirebaseFirestore.getInstance().collection(Constant.ORDER_LIST_REF).document(getSnapshots().getSnapshot(i).getId());

//        Toast.makeText(mContext, "asd id :-" + getSnapshots().getSnapshot(i).getId(), Toast.LENGTH_SHORT).show();
        mDocumentReference.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot snapshot,
                                @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    Log.w(Constant.TAG, "Listen failed.", e);
                    return;
                }

                String source = snapshot != null && snapshot.getMetadata().hasPendingWrites()
                        ? "Local" : "Server";

                if (snapshot != null && snapshot.exists()) {
                    Log.d(Constant.TAG, source + " data: " + snapshot.getData());

                    MyOrder myOrder1 = snapshot.toObject(MyOrder.class);


//                    Toast.makeText(mContext, "asd: - " +myOrder1.isOrder_accepted(), Toast.LENGTH_SHORT).show();


                    if(myOrder1.isOrder_accepted() == true){


                        myOrderViewHolder.mRowLayoutMyOrderBinding.txtOrderStatusMyOrder.setText("Accepted");
                        myOrderViewHolder.mRowLayoutMyOrderBinding.txtOrderStatusMyOrder.setTextColor(ContextCompat.getColor(mContext, R.color.green));
                        myOrderViewHolder.mRowLayoutMyOrderBinding.btnEdit.setVisibility(View.GONE);
                        myOrderViewHolder.mRowLayoutMyOrderBinding.btnCancleOreder.setVisibility(View.GONE);
                        myOrderViewHolder.mRowLayoutMyOrderBinding.btnViewOrder.setVisibility(View.VISIBLE);
                    }else {


                        myOrderViewHolder.mRowLayoutMyOrderBinding.txtOrderStatusMyOrder.setText("Pending");
                        myOrderViewHolder.mRowLayoutMyOrderBinding.txtOrderStatusMyOrder.setTextColor(ContextCompat.getColor(mContext, R.color.red));

                        myOrderViewHolder.mRowLayoutMyOrderBinding.btnEdit.setVisibility(View.VISIBLE);
                        myOrderViewHolder.mRowLayoutMyOrderBinding.btnCancleOreder.setVisibility(View.VISIBLE);
                        myOrderViewHolder.mRowLayoutMyOrderBinding.btnViewOrder.setVisibility(View.GONE);

                    }







                } else {
                    Log.d(Constant.TAG, source + " data: null");
                }
            }
        });

//-------------------------------------------------------------------------------------------------------------

        myOrderViewHolder.mRowLayoutMyOrderBinding.btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent mIntent = new Intent(mContext, MyEditOrderProductListActivity.class);
                mIntent.putExtra(Constant.PUSH_KEY, getSnapshots().getSnapshot(i).getId());
                mContext.startActivity(mIntent);
            }
        });


        myOrderViewHolder.mRowLayoutMyOrderBinding.btnViewOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(mContext, MyOrderProductListActivity.class);
                mIntent.putExtra(Constant.PUSH_KEY, getSnapshots().getSnapshot(i).getId());
                mContext.startActivity(mIntent);
            }
        });


        myOrderViewHolder.mRowLayoutMyOrderBinding.btnCancleOreder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {





                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

                builder.setTitle("Confirm Delete You Order?");
                builder.setMessage("Are you sure wants to cancel this order?");

                builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        // Do nothing but close the dialog


                        //------delete---document-----------------------start------------------------------------------------------------------------

                        FirebaseFirestore.getInstance().collection(Constant.ORDER_LIST_REF).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> task) {

                                if (task.isSuccessful()) {
                                    final String documentIDs = getSnapshots().getSnapshot(i).getId();
//                            Toast.makeText(mContext, "documentIDs: " + documentIDs, Toast.LENGTH_SHORT).show();


                                    CollectionReference mCollectionReferenceMyorder = FirebaseFirestore.getInstance().collection(Constant.ORDER_LIST_REF).document(documentIDs).collection(Constant.ITEM_LIST_REF);
                                    mCollectionReferenceMyorder.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                        @Override
                                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                            for (final QueryDocumentSnapshot document : task.getResult()) {
                                                FirebaseFirestore.getInstance().collection(Constant.ORDER_LIST_REF).document(documentIDs).collection(Constant.ITEM_LIST_REF).document(document.getId()).delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        DocumentReference mDocumentReference = FirebaseFirestore.getInstance().collection(Constant.ORDER_LIST_REF).document(documentIDs);
                                                        mDocumentReference.delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                                                            @Override
                                                            public void onComplete(@NonNull Task<Void> task) {
//                                                                Toast.makeText(mContext, "delete succ", Toast.LENGTH_SHORT).show();
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        }
                                    });


                                }
                            }
                        });


//------delete---document-----------------------end------------------------------------------------------------------------


                        dialog.dismiss();
                    }
                });

                builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        // Do nothing
                        dialog.dismiss();
                    }
                });

                AlertDialog alert = builder.create();
                alert.show();




            }
        });


    }

    @NonNull
    @Override
    public MyOrderAdapter.MyOrderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (mLayoutInflater == null) {
            mLayoutInflater = LayoutInflater.from(parent.getContext());
        }
        RowLayoutMyOrderBinding mRowLayoutMyOrderBinding = DataBindingUtil.inflate(mLayoutInflater, R.layout.row_layout_my_order, parent, false);
        return new MyOrderViewHolder(mRowLayoutMyOrderBinding);
    }

    public class MyOrderViewHolder extends RecyclerView.ViewHolder {
        RowLayoutMyOrderBinding mRowLayoutMyOrderBinding;

        public MyOrderViewHolder(@NonNull RowLayoutMyOrderBinding itemView) {
            super(itemView.getRoot());
            mRowLayoutMyOrderBinding = itemView;
        }
    }
}