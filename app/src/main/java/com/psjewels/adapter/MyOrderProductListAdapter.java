package com.psjewels.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Build;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.psjewels.R;
import com.psjewels.databinding.RowLayoutMyOrderProductListBinding;
import com.psjewels.model.MyCart;
import com.psjewels.utils.Constant;
import com.squareup.picasso.Picasso;



public class MyOrderProductListAdapter extends FirestoreRecyclerAdapter<MyCart, MyOrderProductListAdapter.MyOrderProductListHolder> {

    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private FirebaseUser mFirebaseUser;
    String colorReorder;

    public MyOrderProductListAdapter(@NonNull FirestoreRecyclerOptions<MyCart> options, Context mContext) {
        super(options);
        this.mContext = mContext;
    }

    @Override
    protected void onBindViewHolder(@NonNull MyOrderProductListHolder myOrderProductListHolder, int i, @NonNull MyCart myCart) {
        mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        myOrderProductListHolder.mRowLayoutMyOrderListBinding.productCountRow.setText(String.valueOf(myCart.getProduct_quantity()));

        myOrderProductListHolder.mRowLayoutMyOrderListBinding.proWT.setText(myCart.getPro_weight());

        myOrderProductListHolder.mRowLayoutMyOrderListBinding.txtSize.setText(myCart.getPro_size());
        String productImg = myCart.getImg();
        myOrderProductListHolder.mRowLayoutMyOrderListBinding.proId.setText(myCart.getPro_code());
        myOrderProductListHolder.mRowLayoutMyOrderListBinding.path.setText(myCart.getCat_name() + " > " + myCart.getSub_cat_name());
        if (!TextUtils.isEmpty(productImg)) {
            Picasso.get().load(productImg).fit().centerInside().placeholder(R.drawable.placeholder).into(myOrderProductListHolder.mRowLayoutMyOrderListBinding.imageViewRowLayout);
        }


    }




    @NonNull
    @Override
    public MyOrderProductListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (mLayoutInflater == null) {
            mLayoutInflater = LayoutInflater.from(parent.getContext());
        }
        RowLayoutMyOrderProductListBinding myCartBinding = DataBindingUtil.inflate(mLayoutInflater, R.layout.row_layout_my_order_product_list, parent, false);
        return new MyOrderProductListHolder(myCartBinding);
    }

    public class MyOrderProductListHolder extends RecyclerView.ViewHolder {
        RowLayoutMyOrderProductListBinding mRowLayoutMyOrderListBinding;

        public MyOrderProductListHolder(@NonNull RowLayoutMyOrderProductListBinding itemView) {
            super(itemView.getRoot());
            mRowLayoutMyOrderListBinding = itemView;
        }
    }
}
