package com.psjewels.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.psjewels.R;
import com.psjewels.activity.CartActivity;
import com.psjewels.databinding.DialogAddNotesBinding;
import com.psjewels.databinding.DialogProductOpenBinding;
import com.psjewels.databinding.RowLayoutNewArrivalBinding;
import com.psjewels.model.ImgList;
import com.psjewels.model.MyCart;
import com.psjewels.model.ProductDeatils;
import com.psjewels.utils.Constant;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class NewlyArrivalAdapter extends FirestoreRecyclerAdapter<ProductDeatils, NewlyArrivalAdapter.NewlyArrivalHolder> {
    private RowLayoutNewArrivalBinding mRowLayoutNewArrivalBinding;
    private Context mContext;
    private FirebaseUser mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();


    private LayoutInflater mLayoutInflater;


    ArrayList<ImgList> mImgLists;
    int id = 0;



    /**
     * Create a new RecyclerView adapter that listens to a Firestore Query.  See {@link
     * FirestoreRecyclerOptions} for configuration options.
     *
     * @param options
     */
    public NewlyArrivalAdapter(@NonNull FirestoreRecyclerOptions<ProductDeatils> options, Context mContext) {
        super(options);
        this.mContext = mContext;

    }

    @Override
    protected void onBindViewHolder(@NonNull final NewlyArrivalHolder newlyArrivalHolder, final int i, @NonNull final ProductDeatils productDetails) {


        final String cat_id, cat_name, pro_code_final, pro_code, pro_name, pro_size, pro_weight, sub_cat_id, sub_cat_name, img, product_id;
        final int product_quantity, pro_code_number;


        Picasso
                .get()
                .load(productDetails.getImg())
                .placeholder(R.drawable.placeholder)
                .fit().centerInside()
                .into(newlyArrivalHolder.mRowLayoutNewArrivalBinding.productImg);
        newlyArrivalHolder.mRowLayoutNewArrivalBinding.proId.setText(productDetails.getPro_code() + productDetails.getPro_code_number());
        newlyArrivalHolder.mRowLayoutNewArrivalBinding.proName.setText(productDetails.getPro_name());
        newlyArrivalHolder.mRowLayoutNewArrivalBinding.proSize.setText(productDetails.getPro_size());
        newlyArrivalHolder.mRowLayoutNewArrivalBinding.proWT.setText(productDetails.getPro_weight());

        cat_id = productDetails.getCat_id();
        cat_name = productDetails.getCat_name();
        pro_code = productDetails.getPro_code();
        pro_code_number = productDetails.getPro_code_number();
        pro_code_final = pro_code + pro_code_number;
        pro_name = productDetails.getPro_name();
        pro_size = productDetails.getPro_size();
        pro_weight = productDetails.getPro_weight();
        sub_cat_id = productDetails.getSub_cat_id();
        sub_cat_name = productDetails.getSub_cat_name();
        img = productDetails.getImg();
        product_id = productDetails.getProduct_id();


        newlyArrivalHolder.mRowLayoutNewArrivalBinding.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String product_id = productDetails.getProduct_id();
                DocumentReference myCartRef = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF).document(product_id);
                myCartRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if (documentSnapshot.exists()) {
                            MyCart myCart1 = documentSnapshot.toObject(MyCart.class);
                            MyCart myCart = new MyCart(myCart1.getProduct_quantity() + 1, cat_id, cat_name, pro_code_final, pro_name, pro_size, pro_weight, sub_cat_id, sub_cat_name, img, product_id,null);
                            CollectionReference mCollectionReferenceCart = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF);
                            mCollectionReferenceCart.document(product_id).set(myCart).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @RequiresApi(api = Build.VERSION_CODES.M)
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isComplete()) {
//                                            Toast.makeText(mContext, "Added to Cart", Toast.LENGTH_LONG).show();
                                    }
                                }
                            });
                        } else {
                            MyCart myCart = new MyCart(1, cat_id, cat_name, pro_code_final, pro_name, pro_size, pro_weight, sub_cat_id, sub_cat_name, img, product_id,null);
                            CollectionReference mCollectionReferenceCart = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF);
                            mCollectionReferenceCart.document(product_id).set(myCart).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @RequiresApi(api = Build.VERSION_CODES.M)
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isComplete()) {
//                                        Toast.makeText(mContext, "Added to Cart", Toast.LENGTH_LONG).show();
                                    }
                                }
                            });
                        }
                    }
                });

            }
        });

        newlyArrivalHolder.mRowLayoutNewArrivalBinding.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String product_id = productDetails.getProduct_id();
                DocumentReference myCartRef = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF).document(product_id);
                myCartRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {

                        if (documentSnapshot.exists()) {
                            final MyCart myCart1 = documentSnapshot.toObject(MyCart.class);
                            MyCart myCart = new MyCart(myCart1.getProduct_quantity() - 1, cat_id, cat_name, pro_code_final, pro_name, pro_size, pro_weight, sub_cat_id, sub_cat_name, img, product_id,null);
                            CollectionReference mCollectionReferenceCart = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF);
                            mCollectionReferenceCart.document(product_id).set(myCart).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @RequiresApi(api = Build.VERSION_CODES.M)
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isComplete()) {
                                    }
                                }
                            });

                            if (myCart1.getProduct_quantity() - 1 == 0) {
                                mCollectionReferenceCart.document(myCart1.getProduct_id()).delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                    }
                                });
                            }

                        } else {

                        }


                    }
                });

            }
        });


        //------------------------REAL TIME COUNT CART ---------START----------------------------------------
        FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF).document(product_id).addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                if (documentSnapshot != null) {
                    MyCart myCart = documentSnapshot.toObject(MyCart.class);
                    if (myCart != null) {

                        int qty = myCart.getProduct_quantity();
                        newlyArrivalHolder.mRowLayoutNewArrivalBinding.psc.setText(String.valueOf(qty));
                    }
                }
            }
        });

        //------------------------REAL TIME COUNT CART ---------END----------------------------------------


//        newlyArrivalHolder.mRowLayoutNewArrivalBinding.btnOrderNow.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//
////------------------this--code---when--order--now--press---than---cart--data---store-----start--------
////                final String product_id = productDetails.getProduct_id();
////                DocumentReference myCartRef = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF).document(product_id);
////                myCartRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
////                    @Override
////                    public void onSuccess(DocumentSnapshot documentSnapshot) {
////                        if (documentSnapshot.exists()){
////                            MyCart myCart1 = documentSnapshot.toObject(MyCart.class);
////
////                            int currentPsc = Integer.parseInt(newlyArrivalHolder.mRowLayoutNewArrivalBinding.psc.getText().toString());
////                            MyCart myCart = new MyCart(currentPsc, cat_id, cat_name, pro_code, pro_name, pro_size, pro_weight, sub_cat_id, sub_cat_name, img, product_id);
////                            CollectionReference mCollectionReferenceCart = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF);
////                            mCollectionReferenceCart.document(product_id).set(myCart).addOnCompleteListener(new OnCompleteListener<Void>() {
////                                @RequiresApi(api = Build.VERSION_CODES.M)
////                                @Override
////                                public void onComplete(@NonNull Task<Void> task) {
////                                    if (task.isComplete()) {
//////                                            Toast.makeText(mContext, "Added to Cart", Toast.LENGTH_LONG).show();
////                                    }
////                                }
////                            });
////                        }   else {
////                            MyCart myCart = new MyCart(1, cat_id, cat_name, pro_code, pro_name, pro_size, pro_weight, sub_cat_id, sub_cat_name, img, product_id);
////                            CollectionReference mCollectionReferenceCart = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF);
////                            mCollectionReferenceCart.document(product_id).set(myCart).addOnCompleteListener(new OnCompleteListener<Void>() {
////                                @RequiresApi(api = Build.VERSION_CODES.M)
////                                @Override
////                                public void onComplete(@NonNull Task<Void> task) {
////                                    if (task.isComplete()) {
////                                        Toast.makeText(mContext, "Added to Cart", Toast.LENGTH_LONG).show();
////                                    }
////                                }
////                            });
////                        }
////                    }
////                });
//
//                //------------------this--code---when--order--now--press---than---cart--data---store-----end--------
//
//
//
//
//
//
//            }
//        });


        //------------------------------dialog----start------------------------------------------------------
        newlyArrivalHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.d(Constant.TAG, "docIddddddddddddddddd: "+getSnapshots().getSnapshot(i).getId());

                final Dialog dialog = new Dialog(mContext,R.style.MyAlertDialogTheme);
                DialogProductOpenBinding mDialogProductOpenBinding = DataBindingUtil.inflate(LayoutInflater.from(mContext), R.layout.dialog_product_open, null, false);
                mDialogProductOpenBinding.proWhereComes.setText(productDetails.getCat_name() + " > " + productDetails.getSub_cat_name());
                mDialogProductOpenBinding.proId.setText(productDetails.getPro_code() + productDetails.getPro_code_number());
//                mDialogProductOpenBinding.proName.setText(productDetails.getPro_name());
                mDialogProductOpenBinding.proSize.setText(productDetails.getPro_size());
                mDialogProductOpenBinding.proWT.setText(productDetails.getPro_weight());



//                Picasso.get().load(productDetails.getImg()).placeholder(R.drawable.placeholder).fit().centerInside().into(mDialogProductOpenBinding.proImg);



                //--------------------------------------------------------------------------------------------------------------------------------------
                mImgLists = new ArrayList<>();

                FirebaseFirestore.getInstance().collection(Constant.PRODUCT_LIST_REF).document(product_id).collection(Constant.IMG_LIST).addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                        if (queryDocumentSnapshots != null) {
                            for (final DocumentChange dc : queryDocumentSnapshots.getDocumentChanges()) {
                                switch (dc.getType()) {
                                    case ADDED:
                                        String img = dc.getDocument().getString(Constant.IMG_KEY);
//                                        Toast.makeText(mContext, "img :- " +img, Toast.LENGTH_SHORT).show();
                                        Log.d(Constant.TAG, "ID:-" + dc.getDocument().getId());



                                        ImgList mImgList = new ImgList(img, id);
                                        mImgLists.add(mImgList);

                                        id++;


                                        break;
                                    case MODIFIED:
                                        break;
                                    case REMOVED:
                                        break;
                                }
                            }

//                            ImageAdapter adapter = new ImageAdapter(mContext);
//                            mDialogProductOpenBinding.viewPager.setAdapter(adapter);

                            MultiImgListAdapter  mMultiImgListAdapter = new MultiImgListAdapter(mContext, mImgLists);
                            mDialogProductOpenBinding.viewPager.setAdapter(mMultiImgListAdapter);
                            if(mImgLists.size() == 1 ){

                            }else {
                                mDialogProductOpenBinding.dotsIndicator.setViewPager(mDialogProductOpenBinding.viewPager);

                            }
                        }
                    }
                });
                //--------------------------------------------------------------------------------------------------------------------------------------

//                ImageAdapter adapter = new ImageAdapter(mContext);
//                mDialogProductOpenBinding.viewPager.setAdapter(adapter);


                mDialogProductOpenBinding.imgclose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                //---------------------qty--update--with--plus--&--minus--&---usingEditText----[start]------------------------
                mDialogProductOpenBinding.psc.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        mDialogProductOpenBinding.minus.setVisibility(View.GONE);
                        mDialogProductOpenBinding.plus.setVisibility(View.GONE);

                        mDialogProductOpenBinding.btnUpdateQty.setVisibility(View.VISIBLE);


                        mDialogProductOpenBinding.btnUpdateQty.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if (mDialogProductOpenBinding.psc.getText().toString().length() == 0) {
                                    Toast.makeText(mContext, "Please enter qty", Toast.LENGTH_SHORT).show();
                                } else {

                                    int updateQty = Integer.parseInt(mDialogProductOpenBinding.psc.getText().toString());
                                    InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Activity.INPUT_METHOD_SERVICE);
                                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                                    if (updateQty == 0) {
                                        Toast.makeText(mContext, "Please enter zero above quantity ", Toast.LENGTH_SHORT).show();
                                        InputMethodManager imm1 = (InputMethodManager) mContext.getSystemService(Activity.INPUT_METHOD_SERVICE);
                                        imm1.hideSoftInputFromWindow(v.getWindowToken(), 0);
                                    } else {
                                        FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF).document(productDetails.getProduct_id()).update(Constant.PRODUCT_QUANTITY_KEY, updateQty);
                                        Toast.makeText(mContext, "Updated", Toast.LENGTH_SHORT).show();
                                        mDialogProductOpenBinding.minus.setVisibility(View.VISIBLE);
                                        mDialogProductOpenBinding.plus.setVisibility(View.VISIBLE);
                                        mDialogProductOpenBinding.btnUpdateQty.setVisibility(View.GONE);
                                        InputMethodManager imm1 = (InputMethodManager) mContext.getSystemService(Activity.INPUT_METHOD_SERVICE);
                                        imm1.hideSoftInputFromWindow(v.getWindowToken(), 0);
                                    }
                                }
                            }
                        });
                        return false;
                    }
                });
                //---------------------qty--update--with--plus--&--minus--&---usingEditText----[end]------------------------





                mDialogProductOpenBinding.btnOrderNow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {




                        String note = mDialogProductOpenBinding.edtNote.getText().toString();
                        InputMethodManager imm1 = (InputMethodManager) mContext.getSystemService(Activity.INPUT_METHOD_SERVICE);
                        imm1.hideSoftInputFromWindow(v.getWindowToken(), 0);
//                        dialog.dismiss();
                        final String product_id = productDetails.getProduct_id();
                        DocumentReference myCartRef = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF).document(product_id);
                        myCartRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                            @Override
                            public void onSuccess(DocumentSnapshot documentSnapshot) {
                                if (TextUtils.isEmpty(mDialogProductOpenBinding.psc.getText().toString())){
                                    return;
                                }
                                int currentPsc = Integer.parseInt(mDialogProductOpenBinding.psc.getText().toString());
                                if (TextUtils.isEmpty(mDialogProductOpenBinding.psc.getText().toString())|| currentPsc == 0 ){
                                    currentPsc =1;
                                }

                                if (documentSnapshot.exists()) {
                                    MyCart myCart1 = documentSnapshot.toObject(MyCart.class);


                                    MyCart myCart = new MyCart(currentPsc, cat_id, cat_name, pro_code_final, pro_name, pro_size, pro_weight, sub_cat_id, sub_cat_name, img, product_id,note);
                                    CollectionReference mCollectionReferenceCart = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF);
                                    mCollectionReferenceCart.document(product_id).set(myCart).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @RequiresApi(api = Build.VERSION_CODES.M)
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isComplete()) {
                                            Toast.makeText(mContext, "Added to Cart", Toast.LENGTH_LONG).show();
//                                                mContext.startActivity(new Intent(mContext, CartActivity.class));

                                                long time_stamp= System.currentTimeMillis();
                                                FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid())
                                                        .collection(Constant.ITEM_LIST_REF).document(product_id)
                                                        .update("cart_timestamp",time_stamp);

                                            }
                                        }
                                    });
                                } else {
                                    MyCart myCart = new MyCart(currentPsc, cat_id, cat_name, pro_code_final, pro_name, pro_size, pro_weight, sub_cat_id, sub_cat_name, img, product_id,note);
                                    CollectionReference mCollectionReferenceCart = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF);
                                    mCollectionReferenceCart.document(product_id).set(myCart).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @RequiresApi(api = Build.VERSION_CODES.M)
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isComplete()) {
                                                Toast.makeText(mContext, "Added to Cart", Toast.LENGTH_LONG).show();
//                                                mContext.startActivity(new Intent(mContext, CartActivity.class));

                                                long time_stamp= System.currentTimeMillis();
                                                FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid())
                                                        .collection(Constant.ITEM_LIST_REF).document(product_id)
                                                        .update("cart_timestamp",time_stamp);

                                            }
                                        }
                                    });
                                }
                            }
                        });
                    }
                });


                //------------------------REAL TIME COUNT CART ---------START----------------------------------------
                FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF).document(product_id).addSnapshotListener(new EventListener<DocumentSnapshot>() {
                    @Override
                    public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                        if (documentSnapshot != null) {
                            MyCart myCart = documentSnapshot.toObject(MyCart.class);
                            if (myCart != null) {

                                int qty = myCart.getProduct_quantity();
                                mDialogProductOpenBinding.psc.setText(String.valueOf(qty));
                            }
                        }
                    }
                });

                //------------------------REAL TIME COUNT CART ---------END----------------------------------------

                mDialogProductOpenBinding.plus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        final String product_id = productDetails.getProduct_id();
                        DocumentReference myCartRef = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF).document(product_id);
                        myCartRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                            @Override
                            public void onSuccess(DocumentSnapshot documentSnapshot) {
                                if (documentSnapshot.exists()) {
                                    MyCart myCart1 = documentSnapshot.toObject(MyCart.class);
                                    MyCart myCart = new MyCart(myCart1.getProduct_quantity() + 1, cat_id, cat_name, pro_code_final, pro_name, pro_size, pro_weight, sub_cat_id, sub_cat_name, img, product_id,null);
                                    CollectionReference mCollectionReferenceCart = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF);
                                    mCollectionReferenceCart.document(product_id).set(myCart).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @RequiresApi(api = Build.VERSION_CODES.M)
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isComplete()) {
//                                            Toast.makeText(mContext, "Added to Cart", Toast.LENGTH_LONG).show();

                                                long time_stamp= System.currentTimeMillis();
                                                FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid())
                                                        .collection(Constant.ITEM_LIST_REF).document(product_id)
                                                        .update("cart_timestamp",time_stamp);
                                            }
                                        }
                                    });
                                } else {
                                    MyCart myCart = new MyCart(1, cat_id, cat_name, pro_code_final, pro_name, pro_size, pro_weight, sub_cat_id, sub_cat_name, img, product_id,null);
                                    CollectionReference mCollectionReferenceCart = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF);
                                    mCollectionReferenceCart.document(product_id).set(myCart).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @RequiresApi(api = Build.VERSION_CODES.M)
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isComplete()) {
//                                                Toast.makeText(mContext, "Added to Cart", Toast.LENGTH_LONG).show();


                                                long time_stamp= System.currentTimeMillis();
                                                FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid())
                                                        .collection(Constant.ITEM_LIST_REF).document(product_id)
                                                        .update("cart_timestamp",time_stamp);

                                            }
                                        }
                                    });
                                }
                            }
                        });

                    }
                });


                mDialogProductOpenBinding.minus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        final String product_id = productDetails.getProduct_id();
                        DocumentReference myCartRef = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF).document(product_id);
                        myCartRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                            @Override
                            public void onSuccess(DocumentSnapshot documentSnapshot) {

                                if (documentSnapshot.exists()) {
                                    final MyCart myCart1 = documentSnapshot.toObject(MyCart.class);
                                    MyCart myCart = new MyCart(myCart1.getProduct_quantity() - 1, cat_id, cat_name, pro_code_final, pro_name, pro_size, pro_weight, sub_cat_id, sub_cat_name, img, product_id,null);
                                    CollectionReference mCollectionReferenceCart = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF);
                                    mCollectionReferenceCart.document(product_id).set(myCart).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @RequiresApi(api = Build.VERSION_CODES.M)
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isComplete()) {
                                                long time_stamp= System.currentTimeMillis();
                                                FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid())
                                                        .collection(Constant.ITEM_LIST_REF).document(product_id)
                                                        .update("cart_timestamp",time_stamp);
                                            }
                                        }
                                    });

                                    if (myCart1.getProduct_quantity() - 1 == 0) {

                                        mCollectionReferenceCart.document(myCart1.getProduct_id()).delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {

                                            }
                                        });
                                    }

                                } else {

                                }


                            }
                        });

                    }
                });

                dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);

                dialog.setContentView(mDialogProductOpenBinding.getRoot());
                dialog.show();


            }
        });


        //------------------------------dialog----end------------------------------------------------------

    }

    @NonNull
    @Override
    public NewlyArrivalHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (mLayoutInflater == null) {
            mLayoutInflater = LayoutInflater.from(parent.getContext());
        }
        mRowLayoutNewArrivalBinding = DataBindingUtil.inflate(mLayoutInflater, R.layout.row_layout_new_arrival, parent, false);

        return new NewlyArrivalHolder(mRowLayoutNewArrivalBinding);

    }

    public class NewlyArrivalHolder extends RecyclerView.ViewHolder {
        RowLayoutNewArrivalBinding mRowLayoutNewArrivalBinding;

        public NewlyArrivalHolder(@NonNull RowLayoutNewArrivalBinding itemView) {
            super(itemView.getRoot());
            mRowLayoutNewArrivalBinding = itemView;
        }
    }
}
//package com.psjewels.adapter;
//
//import android.app.Dialog;
//import android.content.Context;
//import android.content.Intent;
//import android.os.Build;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.Toast;
//
//import androidx.annotation.NonNull;
//import androidx.annotation.Nullable;
//import androidx.annotation.RequiresApi;
//import androidx.databinding.DataBindingUtil;
//import androidx.recyclerview.widget.RecyclerView;
//
//import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
//import com.firebase.ui.firestore.FirestoreRecyclerOptions;
//import com.google.android.gms.tasks.OnCompleteListener;
//import com.google.android.gms.tasks.OnSuccessListener;
//import com.google.android.gms.tasks.Task;
//import com.google.firebase.auth.FirebaseAuth;
//import com.google.firebase.auth.FirebaseUser;
//import com.google.firebase.firestore.CollectionReference;
//import com.google.firebase.firestore.DocumentReference;
//import com.google.firebase.firestore.DocumentSnapshot;
//import com.google.firebase.firestore.EventListener;
//import com.google.firebase.firestore.FirebaseFirestore;
//import com.google.firebase.firestore.FirebaseFirestoreException;
//import com.google.firebase.firestore.QuerySnapshot;
//import com.psjewels.R;
//import com.psjewels.activity.CartActivity;
//import com.psjewels.activity.DashboardActivity;
//import com.psjewels.databinding.DialogProductOpenBinding;
//import com.psjewels.databinding.RowLayoutNewArrivalBinding;
//import com.psjewels.model.MyCart;
//import com.psjewels.model.ProductDeatils;
//import com.psjewels.utils.Constant;
//import com.squareup.picasso.Picasso;
//
//
//public class NewlyArrivalAdapter extends FirestoreRecyclerAdapter<ProductDeatils, NewlyArrivalAdapter.NewlyArrivalHolder> {
//    private RowLayoutNewArrivalBinding mRowLayoutNewArrivalBinding;
//    private Context mContext;
//    private FirebaseUser mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
//
//
//    private LayoutInflater mLayoutInflater;
//
//    /**
//     * Create a new RecyclerView adapter that listens to a Firestore Query.  See {@link
//     * FirestoreRecyclerOptions} for configuration options.
//     *
//     * @param options
//     */
//    public NewlyArrivalAdapter(@NonNull FirestoreRecyclerOptions<ProductDeatils> options, Context mContext) {
//        super(options);
//        this.mContext = mContext;
//
//    }
//
//    @Override
//    protected void onBindViewHolder(@NonNull final NewlyArrivalHolder newlyArrivalHolder, final int i, @NonNull final ProductDeatils productDetails) {
//
//
//        final String cat_id, cat_name, pro_code_final,pro_code, pro_name, pro_size, pro_weight, sub_cat_id, sub_cat_name, img, product_id;
//        final int product_quantity,pro_code_number;
//
//        Picasso
//                .get()
//                .load(productDetails.getImg())
//                .placeholder(R.drawable.placeholder)
//                .fit().centerInside()
//                .into(newlyArrivalHolder.mRowLayoutNewArrivalBinding.productImg);
//        newlyArrivalHolder.mRowLayoutNewArrivalBinding.proId.setText(productDetails.getPro_code() + productDetails.getPro_code_number());
//        newlyArrivalHolder.mRowLayoutNewArrivalBinding.proName.setText(productDetails.getPro_name());
//        newlyArrivalHolder.mRowLayoutNewArrivalBinding.proSize.setText(productDetails.getPro_size());
//        newlyArrivalHolder.mRowLayoutNewArrivalBinding.proWT.setText(productDetails.getPro_weight());
//
//        cat_id = productDetails.getCat_id();
//        cat_name = productDetails.getCat_name();
//        pro_code = productDetails.getPro_code();
//        pro_code_number = productDetails.getPro_code_number();
//        pro_code_final = pro_code+pro_code_number;
//        pro_name = productDetails.getPro_name();
//        pro_size = productDetails.getPro_size();
//        pro_weight = productDetails.getPro_weight();
//        sub_cat_id = productDetails.getSub_cat_id();
//        sub_cat_name = productDetails.getSub_cat_name();
//        img = productDetails.getImg();
//        product_id = productDetails.getProduct_id();
//
//
//
//
//        newlyArrivalHolder.mRowLayoutNewArrivalBinding.plus.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//               final String product_id = productDetails.getProduct_id();
//                DocumentReference myCartRef = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF).document(product_id);
//                myCartRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
//                    @Override
//                    public void onSuccess(DocumentSnapshot documentSnapshot) {
//                            if (documentSnapshot.exists()){
//                                MyCart myCart1 = documentSnapshot.toObject(MyCart.class);
//                                MyCart myCart = new MyCart(myCart1.getProduct_quantity()+1, cat_id, cat_name, pro_code_final, pro_name, pro_size, pro_weight, sub_cat_id, sub_cat_name, img, product_id);
//                                CollectionReference mCollectionReferenceCart = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF);
//                                mCollectionReferenceCart.document(product_id).set(myCart).addOnCompleteListener(new OnCompleteListener<Void>() {
//                                    @RequiresApi(api = Build.VERSION_CODES.M)
//                                    @Override
//                                    public void onComplete(@NonNull Task<Void> task) {
//                                        if (task.isComplete()) {
////                                            Toast.makeText(mContext, "Added to Cart", Toast.LENGTH_LONG).show();
//                                        }
//                                    }
//                                });
//                            }   else {
//                                MyCart myCart = new MyCart(1, cat_id, cat_name, pro_code_final, pro_name, pro_size, pro_weight, sub_cat_id, sub_cat_name, img, product_id);
//                                CollectionReference mCollectionReferenceCart = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF);
//                                mCollectionReferenceCart.document(product_id).set(myCart).addOnCompleteListener(new OnCompleteListener<Void>() {
//                                    @RequiresApi(api = Build.VERSION_CODES.M)
//                                    @Override
//                                    public void onComplete(@NonNull Task<Void> task) {
//                                        if (task.isComplete()) {
//                                            Toast.makeText(mContext, "Added to Cart", Toast.LENGTH_LONG).show();
//                                        }
//                                    }
//                                });
//                            }
//                    }
//                });
//
//            }
//        });
//
//        newlyArrivalHolder.mRowLayoutNewArrivalBinding.minus.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//               final String product_id = productDetails.getProduct_id();
//                DocumentReference myCartRef = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF).document(product_id);
//                myCartRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
//                    @Override
//                    public void onSuccess(DocumentSnapshot documentSnapshot) {
//
//                            if (documentSnapshot.exists()){
//                                final MyCart myCart1 = documentSnapshot.toObject(MyCart.class);
//                                MyCart myCart = new MyCart(myCart1.getProduct_quantity()-1, cat_id, cat_name, pro_code_final, pro_name, pro_size, pro_weight, sub_cat_id, sub_cat_name, img, product_id);
//                                CollectionReference mCollectionReferenceCart = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF);
//                                mCollectionReferenceCart.document(product_id).set(myCart).addOnCompleteListener(new OnCompleteListener<Void>() {
//                                    @RequiresApi(api = Build.VERSION_CODES.M)
//                                    @Override
//                                    public void onComplete(@NonNull Task<Void> task) {
//                                        if (task.isComplete()) {
//                                        }
//                                    }
//                                });
//
//                                if(myCart1.getProduct_quantity()-1 ==  0){
//                                    mCollectionReferenceCart.document(myCart1.getProduct_id()).delete().addOnSuccessListener(new OnSuccessListener<Void>() {
//                                        @Override
//                                        public void onSuccess(Void aVoid) {
//                                        }
//                                    });
//                                }
//
//                            }   else {
//
//                            }
//
//
//                    }
//                });
//
//            }
//        });
//
//
//
//        //------------------------REAL TIME COUNT CART ---------START----------------------------------------
//        FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF).document(product_id).addSnapshotListener(new EventListener<DocumentSnapshot>() {
//            @Override
//            public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
//                if (documentSnapshot != null) {
//                    MyCart myCart = documentSnapshot.toObject(MyCart.class);
//                    if (myCart != null) {
//
//                        int qty = myCart.getProduct_quantity();
//                        newlyArrivalHolder.mRowLayoutNewArrivalBinding.psc.setText(String.valueOf(qty));
//                    }
//                }
//            }
//        });
//
//        //------------------------REAL TIME COUNT CART ---------END----------------------------------------
//
//
//
//        newlyArrivalHolder.mRowLayoutNewArrivalBinding.btnOrderNow.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                mContext.startActivity(new Intent(mContext, CartActivity.class));
//
//                MyCart myCart = new MyCart(1, cat_id, cat_name, pro_code_final, pro_name, pro_size, pro_weight, sub_cat_id, sub_cat_name, img, product_id);
//                CollectionReference mCollectionReferenceCart = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF);
//                mCollectionReferenceCart.document(product_id).set(myCart).addOnCompleteListener(new OnCompleteListener<Void>() {
//                    @RequiresApi(api = Build.VERSION_CODES.M)
//                    @Override
//                    public void onComplete(@NonNull Task<Void> task) {
//                        if (task.isComplete()) {
//                            Toast.makeText(mContext, "Added to Cart", Toast.LENGTH_LONG).show();
//                        }
//                    }
//                });
//            }
//        });
//
//
//
//        //------------------------------dialog----start------------------------------------------------------
//        newlyArrivalHolder.itemView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                final DialogProductOpenBinding mDialogProductOpenBinding;
//                final Dialog dialog = new Dialog(mContext);
//                mDialogProductOpenBinding = DataBindingUtil.inflate(LayoutInflater.from(mContext), R.layout.dialog_product_open, null, false);
//                mDialogProductOpenBinding.proWhereComes.setText(productDetails.getCat_name() + " > " + productDetails.getSub_cat_name());
//                mDialogProductOpenBinding.proId.setText(productDetails.getPro_code() + productDetails.getPro_code_number());
//                mDialogProductOpenBinding.proName.setText(productDetails.getPro_name());
//                mDialogProductOpenBinding.proSize.setText(productDetails.getPro_size());
//                mDialogProductOpenBinding.proWT.setText(productDetails.getPro_weight());
//                Picasso.get().load(productDetails.getImg()).placeholder(R.drawable.placeholder).fit().centerInside().into(mDialogProductOpenBinding.proImg);
//                mDialogProductOpenBinding.imgclose.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        dialog.dismiss();
//                    }
//                });
//                mDialogProductOpenBinding.btnOrderNow.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//
//
//                        MyCart myCart = new MyCart(1, cat_id, cat_name, pro_code_final, pro_name, pro_size, pro_weight, sub_cat_id, sub_cat_name, img, product_id);
//                        CollectionReference mCollectionReferenceCart = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF);
//                        mCollectionReferenceCart.document(product_id).set(myCart).addOnCompleteListener(new OnCompleteListener<Void>() {
//                            @RequiresApi(api = Build.VERSION_CODES.M)
//                            @Override
//                            public void onComplete(@NonNull Task<Void> task) {
//                                if (task.isComplete()) {
//                                    Toast.makeText(mContext, "Added to Cart", Toast.LENGTH_LONG).show();
//                                }
//                            }
//                        });
//                    }
//                });
//
//
//                //------------------------REAL TIME COUNT CART ---------START----------------------------------------
//                FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF).document(product_id).addSnapshotListener(new EventListener<DocumentSnapshot>() {
//                    @Override
//                    public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
//                        if (documentSnapshot != null) {
//                            MyCart myCart = documentSnapshot.toObject(MyCart.class);
//                            if (myCart != null) {
//
//                                int qty = myCart.getProduct_quantity();
//                               mDialogProductOpenBinding.psc.setText(String.valueOf(qty));
//                            }
//                        }
//                    }
//                });
//
//                //------------------------REAL TIME COUNT CART ---------END----------------------------------------
//
//                mDialogProductOpenBinding.plus.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//
//                        final String product_id = productDetails.getProduct_id();
//                        DocumentReference myCartRef = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF).document(product_id);
//                        myCartRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
//                            @Override
//                            public void onSuccess(DocumentSnapshot documentSnapshot) {
//                                if (documentSnapshot.exists()){
//                                    MyCart myCart1 = documentSnapshot.toObject(MyCart.class);
//                                    MyCart myCart = new MyCart(myCart1.getProduct_quantity()+1, cat_id, cat_name, pro_code_final, pro_name, pro_size, pro_weight, sub_cat_id, sub_cat_name, img, product_id);
//                                    CollectionReference mCollectionReferenceCart = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF);
//                                    mCollectionReferenceCart.document(product_id).set(myCart).addOnCompleteListener(new OnCompleteListener<Void>() {
//                                        @RequiresApi(api = Build.VERSION_CODES.M)
//                                        @Override
//                                        public void onComplete(@NonNull Task<Void> task) {
//                                            if (task.isComplete()) {
////                                            Toast.makeText(mContext, "Added to Cart", Toast.LENGTH_LONG).show();
//                                            }
//                                        }
//                                    });
//                                }   else {
//                                    MyCart myCart = new MyCart(1, cat_id, cat_name, pro_code_final, pro_name, pro_size, pro_weight, sub_cat_id, sub_cat_name, img, product_id);
//                                    CollectionReference mCollectionReferenceCart = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF);
//                                    mCollectionReferenceCart.document(product_id).set(myCart).addOnCompleteListener(new OnCompleteListener<Void>() {
//                                        @RequiresApi(api = Build.VERSION_CODES.M)
//                                        @Override
//                                        public void onComplete(@NonNull Task<Void> task) {
//                                            if (task.isComplete()) {
//                                                Toast.makeText(mContext, "Added to Cart", Toast.LENGTH_LONG).show();
//                                            }
//                                        }
//                                    });
//                                }
//                            }
//                        });
//
//                    }
//                });
//
//
//
//
//               mDialogProductOpenBinding.minus.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//
//                        final String product_id = productDetails.getProduct_id();
//                        DocumentReference myCartRef = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF).document(product_id);
//                        myCartRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
//                            @Override
//                            public void onSuccess(DocumentSnapshot documentSnapshot) {
//
//                                if (documentSnapshot.exists()){
//                                    final MyCart myCart1 = documentSnapshot.toObject(MyCart.class);
//                                    MyCart myCart = new MyCart(myCart1.getProduct_quantity()-1, cat_id, cat_name, pro_code_final, pro_name, pro_size, pro_weight, sub_cat_id, sub_cat_name, img, product_id);
//                                    CollectionReference mCollectionReferenceCart = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF);
//                                    mCollectionReferenceCart.document(product_id).set(myCart).addOnCompleteListener(new OnCompleteListener<Void>() {
//                                        @RequiresApi(api = Build.VERSION_CODES.M)
//                                        @Override
//                                        public void onComplete(@NonNull Task<Void> task) {
//                                            if (task.isComplete()) {
//                                            }
//                                        }
//                                    });
//
//                                    if(myCart1.getProduct_quantity()-1 ==  0){
//
//                                        mCollectionReferenceCart.document(myCart1.getProduct_id()).delete().addOnSuccessListener(new OnSuccessListener<Void>() {
//                                            @Override
//                                            public void onSuccess(Void aVoid) {
//                                            }
//                                        });
//                                    }
//
//                                }   else {
//
//                                }
//
//
//                            }
//                        });
//
//                    }
//                });
//
//                dialog.setContentView(mDialogProductOpenBinding.getRoot());
//                dialog.show();
//            }
//        });
//
//
//        //------------------------------dialog----end------------------------------------------------------
//
//    }
//
//    @NonNull
//    @Override
//    public NewlyArrivalHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        if (mLayoutInflater == null) {
//            mLayoutInflater = LayoutInflater.from(parent.getContext());
//        }
//        mRowLayoutNewArrivalBinding = DataBindingUtil.inflate(mLayoutInflater, R.layout.row_layout_new_arrival, parent, false);
//
//        return new NewlyArrivalHolder(mRowLayoutNewArrivalBinding);
//
//    }
//
//    public class NewlyArrivalHolder extends RecyclerView.ViewHolder {
//        RowLayoutNewArrivalBinding mRowLayoutNewArrivalBinding;
//
//        public NewlyArrivalHolder(@NonNull RowLayoutNewArrivalBinding itemView) {
//            super(itemView.getRoot());
//            mRowLayoutNewArrivalBinding = itemView;
//        }
//    }
//}
