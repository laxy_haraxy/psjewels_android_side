package com.psjewels.adapter;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.psjewels.R;
import com.psjewels.activity.CollectUserDetailsActivity;

import java.util.List;

public class MultipleImageCollectDetailsAdapter extends RecyclerView.Adapter<MultipleImageCollectDetailsAdapter.ImageHolder> {

    private List<Uri> mListOfImg;
    private Context context;

    public MultipleImageCollectDetailsAdapter(List<Uri> mListOfImg, Context context) {
        this.mListOfImg = mListOfImg;
        this.context = context;
    }

    @NonNull
    @Override
    public ImageHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(context).inflate(R.layout.row_layout_collect_details_img, viewGroup, false);

        return new ImageHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ImageHolder imageHolder, final int i) {


        Uri multipleimgUri = mListOfImg.get(i);
        imageHolder.mImageView.setImageURI(multipleimgUri);
        imageHolder.mImageViewDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeAt(i);
            }
        });




        if(mListOfImg.size() == 2){
//            Toast.makeText(context, "asd :- " + mListOfImg.size(), Toast.LENGTH_SHORT).show();
            if (context instanceof CollectUserDetailsActivity) {
                ((CollectUserDetailsActivity)context).yourDesiredMethod();
            }
        }
    }

    @Override
    public int getItemCount() {
        return mListOfImg.size();
    }

    private void removeAt(int position) {
        mListOfImg.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mListOfImg.size());
    }

    class ImageHolder extends RecyclerView.ViewHolder {
        ImageView mImageView, mImageViewDelete;

        ImageHolder(@NonNull View itemView) {
            super(itemView);
            mImageView = itemView.findViewById(R.id.image_view_row_layout_multiple_img);
            mImageViewDelete = itemView.findViewById(R.id.image_view_delete_row_layout_multiple_img);
        }
    }

}