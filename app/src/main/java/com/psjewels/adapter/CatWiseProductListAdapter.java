//package com.psjewels.adapter;

//import android.annotation.SuppressLint;
//import android.app.Activity;
//import android.app.AlertDialog;
//import android.app.Dialog;
//import android.app.ProgressDialog;
//import android.content.Context;
//import android.content.Intent;
//import android.database.Cursor;
//import android.graphics.Bitmap;
//import android.graphics.BitmapFactory;
//import android.graphics.Typeface;
//import android.media.Image;
//import android.net.ConnectivityManager;
//import android.net.NetworkInfo;
//import android.net.Uri;
//import android.os.Build;
//import android.os.Environment;
//import android.os.Handler;
//import android.os.VibrationEffect;
//import android.os.Vibrator;
//import android.provider.MediaStore;
//import android.provider.OpenableColumns;
//import android.text.TextUtils;
//import android.util.Base64;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.MotionEvent;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.animation.AlphaAnimation;
//import android.view.animation.Animation;
//import android.view.animation.AnimationUtils;
//import android.view.inputmethod.InputMethodManager;
//import android.widget.ImageView;
//import android.widget.RadioButton;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import androidx.annotation.NonNull;
//import androidx.annotation.Nullable;
//import androidx.annotation.RequiresApi;
//import androidx.appcompat.view.menu.MenuView;
//import androidx.databinding.DataBindingUtil;
//import androidx.paging.RxPagedListBuilder;
//import androidx.recyclerview.widget.RecyclerView;
//
//import com.bumptech.glide.Glide;
//import com.bumptech.glide.request.RequestOptions;
//import com.bumptech.glide.request.target.SimpleTarget;
//import com.bumptech.glide.request.target.SizeReadyCallback;
//import com.bumptech.glide.request.transition.Transition;
//import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
//import com.firebase.ui.firestore.FirestoreRecyclerOptions;
//import com.firebase.ui.firestore.paging.FirestorePagingAdapter;
//import com.firebase.ui.firestore.paging.FirestorePagingOptions;
//import com.firebase.ui.firestore.paging.LoadingState;
//import com.google.android.gms.tasks.OnCompleteListener;
//import com.google.android.gms.tasks.OnFailureListener;
//import com.google.android.gms.tasks.OnSuccessListener;
//import com.google.android.gms.tasks.Task;
//import com.google.android.material.internal.NavigationMenuItemView;
//import com.google.firebase.auth.FirebaseAuth;
//import com.google.firebase.auth.FirebaseUser;
//import com.google.firebase.firestore.CollectionReference;
//import com.google.firebase.firestore.DocumentChange;
//import com.google.firebase.firestore.DocumentReference;
//import com.google.firebase.firestore.DocumentSnapshot;
//import com.google.firebase.firestore.EventListener;
//import com.google.firebase.firestore.FirebaseFirestore;
//import com.google.firebase.firestore.FirebaseFirestoreException;
//import com.google.firebase.firestore.QuerySnapshot;
//import com.google.firebase.messaging.Constants;
//import com.google.firebase.storage.FirebaseStorage;
//import com.google.firebase.storage.StorageMetadata;
//import com.google.firebase.storage.StorageReference;
//import com.psjewels.R;
//import com.psjewels.activity.CartActivity;
//import com.psjewels.activity.SearchActivity;
//import com.psjewels.databinding.DialogProductOpenBinding;
//import com.psjewels.databinding.RowCatWiseProductListBinding;
//import com.psjewels.model.ImgList;
//import com.psjewels.model.MyCart;
//import com.psjewels.model.ProductDeatils;
//import com.psjewels.utils.Constant;
//import com.squareup.picasso.Callback;
//import com.squareup.picasso.Picasso;
//import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;
//
//import java.io.ByteArrayInputStream;
//import java.io.ByteArrayOutputStream;
//import java.io.File;
//import java.io.IOException;
//import java.io.InputStream;
//import java.net.HttpURLConnection;
//import java.net.MalformedURLException;
//import java.net.URL;
//import java.util.ArrayList;
//import java.util.List;
//
//import static androidx.constraintlayout.widget.Constraints.TAG;
//import static java.security.AccessController.getContext;
//
//public class CatWiseProductListAdapter extends FirestorePagingAdapter<ProductDeatils, CatWiseProductListAdapter.ViewHolder>  {
//
//
//    private static final String TAG = "Adadpter";
//    LayoutInflater mLayoutInflater;
//    private Context mContext;
//    private List<DocumentSnapshot> mSubCategoryLists;
//    AlertDialog.Builder builder;
//    ProgressDialog mProgressDialog;
//
//    String CurrentId;
//    private FirebaseUser mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
//    int id = 0;
//    ArrayList<ImgList> mImgLists;
//
//    private int lastPostion = -1;
//
//
//    private static final long MiB = 1024 * 1024;
//    private static final long KiB = 1024;
//
//
//    /**
//     * Create a new RecyclerView adapter that listens to a Firestore Query.  See {@link
//     * FirestoreRecyclerOptions} for configuration options.
//     * <p>
//     * //     * @param options
//     */
////    public CatWiseProductListAdapter(@NonNull FirestoreRecyclerOptions<ProductDeatils> options, Context mContext) {
////        super(options);
////        this.mContext = mContext;
////
////    }
//    public CatWiseProductListAdapter(FirestorePagingOptions<ProductDeatils> mProductDetailsFirestoreRecyclerOptions, Context mContext) {
//        super(mProductDetailsFirestoreRecyclerOptions);
//        this.mContext = mContext;
//    }
//
////    @Override
////    public void onDataChanged() {
////        super.onDataChanged();
////        notifyDataSetChanged();
////    }
//
//
//    @Override
//    protected void onBindViewHolder(@NonNull final ViewHolder productListViewHolder, final int i, @NonNull final ProductDeatils productDetails) {
//
//        if (productListViewHolder instanceof CatWiseProductListAdapter.ViewHolder) {
//            if (productListViewHolder.getAdapterPosition() > lastPostion) {
//                Animation animation = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left);
//                ((ViewHolder) productListViewHolder).itemView.startAnimation(animation);
//                lastPostion = productListViewHolder.getAdapterPosition();
//
//            }
//        }
//
//
//        final String cat_id, cat_name, pro_code_final, pro_code, img, pro_name, pro_size, pro_weight, product_id, sub_cat_id, sub_cat_name;
//        final int product_qty, pro_price, pro_total_price, pro_code_number;
//
//
////        mCollectionReference = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF);
////        final ProductDeatils productDetails = mSubCategoryLists.get(i).toObject(ProductDeatils.class);
//
//        productListViewHolder.mRowCatWiseProductListBinding.proId.setText(productDetails.getPro_code() + productDetails.getPro_code_number());
////        productListViewHolder.mRowCatWiseProductListBinding.proName.setText(productDetails.getPro_name());
//        productListViewHolder.mRowCatWiseProductListBinding.proSize.setText(productDetails.getPro_size());
//        productListViewHolder.mRowCatWiseProductListBinding.proWT.setText(productDetails.getPro_weight());
//
//        Picasso
//                .get()
//                .load(productDetails.getImg())
////                .resize(10,10)
//                .placeholder(R.drawable.placeholder)
//                .fit().centerInside()
//                .into(productListViewHolder.mRowCatWiseProductListBinding.proImg);
//
//
//        //------------------
////        RequestOptions myOptions = new RequestOptions()
////                .override(100, 100);
////
////        Glide.with(mContext)
////                .asBitmap()
////                .apply(myOptions)
////                .load(productDetails.getImg())
////                .into(productListViewHolder.mRowCatWiseProductListBinding.proImg);
//
//
//        cat_id = productDetails.getCat_id();
//        cat_name = productDetails.getCat_name();
//        pro_code = productDetails.getPro_code() + productDetails.getPro_code_number();
//        pro_code_number = productDetails.getPro_code_number();
//
//        pro_code_final = pro_code + pro_code_number;
//        img = productDetails.getImg();
//        pro_name = productDetails.getPro_name();
//        pro_size = productDetails.getPro_size();
//        pro_weight = productDetails.getPro_weight();
//        product_qty = Integer.parseInt(productListViewHolder.mRowCatWiseProductListBinding.psc.getText().toString());
//        product_id = productDetails.getProduct_id();
//        sub_cat_id = productDetails.getSub_cat_id();
//        sub_cat_name = productDetails.getSub_cat_name();
//
//        productListViewHolder.mRowCatWiseProductListBinding.plus.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                final String product_id = productDetails.getProduct_id();
//                DocumentReference myCartRef = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF).document(product_id);
//                myCartRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
//                    @Override
//                    public void onSuccess(DocumentSnapshot documentSnapshot) {
//                        if (documentSnapshot.exists()) {
//                            MyCart myCart1 = documentSnapshot.toObject(MyCart.class);
//                            MyCart myCart = new MyCart(myCart1.getProduct_quantity() + 1, cat_id, cat_name, pro_code, pro_name, pro_size, pro_weight, sub_cat_id, sub_cat_name, img, product_id, null);
//                            CollectionReference mCollectionReferenceCart = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF);
//                            mCollectionReferenceCart.document(product_id).set(myCart).addOnCompleteListener(new OnCompleteListener<Void>() {
//                                @RequiresApi(api = Build.VERSION_CODES.M)
//                                @Override
//                                public void onComplete(@NonNull Task<Void> task) {
//                                    if (task.isComplete()) {
////                                            Toast.makeText(mContext, "Added to Cart", Toast.LENGTH_LONG).show();
//                                    }
//                                }
//                            });
//                        } else {
//                            MyCart myCart = new MyCart(1, cat_id, cat_name, pro_code, pro_name, pro_size, pro_weight, sub_cat_id, sub_cat_name, img, product_id, null);
//                            CollectionReference mCollectionReferenceCart = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF);
//                            mCollectionReferenceCart.document(product_id).set(myCart).addOnCompleteListener(new OnCompleteListener<Void>() {
//                                @RequiresApi(api = Build.VERSION_CODES.M)
//                                @Override
//                                public void onComplete(@NonNull Task<Void> task) {
//                                    if (task.isComplete()) {
////                                        Toast.makeText(mContext, "Added to Cart", Toast.LENGTH_LONG).show();
//                                    }
//                                }
//                            });
//                        }
//                    }
//                });
//
//            }
//        });
//
//        productListViewHolder.mRowCatWiseProductListBinding.minus.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                final String product_id = productDetails.getProduct_id();
//                DocumentReference myCartRef = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF).document(product_id);
//                myCartRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
//                    @Override
//                    public void onSuccess(DocumentSnapshot documentSnapshot) {
//
//                        if (documentSnapshot.exists()) {
//                            final MyCart myCart1 = documentSnapshot.toObject(MyCart.class);
//                            MyCart myCart = new MyCart(myCart1.getProduct_quantity() - 1, cat_id, cat_name, pro_code, pro_name, pro_size, pro_weight, sub_cat_id, sub_cat_name, img, product_id, null);
//                            CollectionReference mCollectionReferenceCart = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF);
//                            mCollectionReferenceCart.document(product_id).set(myCart).addOnCompleteListener(new OnCompleteListener<Void>() {
//                                @RequiresApi(api = Build.VERSION_CODES.M)
//                                @Override
//                                public void onComplete(@NonNull Task<Void> task) {
//                                    if (task.isComplete()) {
//                                    }
//                                }
//                            });
//
//                            if (myCart1.getProduct_quantity() - 1 == 0) {
//                                mCollectionReferenceCart.document(myCart1.getProduct_id()).delete().addOnSuccessListener(new OnSuccessListener<Void>() {
//                                    @Override
//                                    public void onSuccess(Void aVoid) {
//                                    }
//                                });
//                            }
//
//                        } else {
//
//                        }
//
//
//                    }
//                });
//
//            }
//        });
//
//
//        //------------------------REAL TIME COUNT CART ---------START----------------------------------------
//        FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF).document(product_id).addSnapshotListener(new EventListener<DocumentSnapshot>() {
//            @Override
//            public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
//                if (documentSnapshot != null) {
//                    MyCart myCart = documentSnapshot.toObject(MyCart.class);
//                    if (myCart != null) {
//
//                        int qty = myCart.getProduct_quantity();
//                        productListViewHolder.mRowCatWiseProductListBinding.psc.setText(String.valueOf(qty));
//                    }
//                }
//            }
//        });
//
//
//        productListViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                final Dialog dialog = new Dialog(mContext, R.style.MyAlertDialogTheme);
//                DialogProductOpenBinding mDialogProductOpenBinding = DataBindingUtil.inflate(LayoutInflater.from(mContext), R.layout.dialog_product_open, null, false);
//                mDialogProductOpenBinding.proWhereComes.setText(productDetails.getCat_name() + " > " + productDetails.getSub_cat_name());
//                mDialogProductOpenBinding.proId.setText(productDetails.getPro_code() + productDetails.getPro_code_number());
////                mDialogProductOpenBinding.proName.setText(productDetails.getPro_name());
//                mDialogProductOpenBinding.proSize.setText(productDetails.getPro_size());
//                mDialogProductOpenBinding.proWT.setText(productDetails.getPro_weight());
//
////                Picasso.get().load(productDetails.getImg()).placeholder(R.drawable.placeholder).fit().centerInside().into(mDialogProductOpenBinding.proImg);
//
//
//                //--------------------------------------------------------------------------------------------------------------------------------------
//                mImgLists = new ArrayList<>();
//
//                FirebaseFirestore.getInstance().collection(Constant.PRODUCT_LIST_REF).document(product_id).collection(Constant.IMG_LIST).addSnapshotListener(new EventListener<QuerySnapshot>() {
//                    @Override
//                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
//                        if (queryDocumentSnapshots != null) {
//                            for (final DocumentChange dc : queryDocumentSnapshots.getDocumentChanges()) {
//                                switch (dc.getType()) {
//                                    case ADDED:
//                                        String img = dc.getDocument().getString(Constant.IMG_KEY);
////                                        Toast.makeText(mContext, "img :- " +img, Toast.LENGTH_SHORT).show();
//                                        Log.d(Constant.TAG, "ID:-" + dc.getDocument().getId());
//
//
//                                        ImgList mImgList = new ImgList(img, id);
//                                        mImgLists.add(mImgList);
//
//                                        id++;
//
//
//                                        break;
//                                    case MODIFIED:
//                                        break;
//                                    case REMOVED:
//                                        break;
//                                }
//                            }
//
////                            ImageAdapter adapter = new ImageAdapter(mContext);
////                            mDialogProductOpenBinding.viewPager.setAdapter(adapter);
//
//                            MultiImgListAdapter mMultiImgListAdapter = new MultiImgListAdapter(mContext, mImgLists);
//                            mDialogProductOpenBinding.viewPager.setAdapter(mMultiImgListAdapter);
//                            if (mImgLists.size() == 1) {
//
//                            } else {
//                                mDialogProductOpenBinding.dotsIndicator.setViewPager(mDialogProductOpenBinding.viewPager);
//
//                            }
//                        }
//                    }
//                });
//                //--------------------------------------------------------------------------------------------------------------------------------------
//
//
//                mDialogProductOpenBinding.imgclose.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        dialog.dismiss();
//                    }
//                });
//
//                //---------------------qty--update--with--plus--&--minus--&---usingEditText----[start]------------------------
//                mDialogProductOpenBinding.psc.setOnTouchListener(new View.OnTouchListener() {
//                    @Override
//                    public boolean onTouch(View v, MotionEvent event) {
//                        mDialogProductOpenBinding.minus.setVisibility(View.GONE);
//                        mDialogProductOpenBinding.plus.setVisibility(View.GONE);
//
//                        mDialogProductOpenBinding.btnUpdateQty.setVisibility(View.VISIBLE);
//
//
//                        mDialogProductOpenBinding.btnUpdateQty.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//
//                                if (mDialogProductOpenBinding.psc.getText().toString().length() == 0) {
//                                    Toast.makeText(mContext, "Please enter qty", Toast.LENGTH_SHORT).show();
//                                } else {
//
//                                    int updateQty = Integer.parseInt(mDialogProductOpenBinding.psc.getText().toString());
//                                    InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Activity.INPUT_METHOD_SERVICE);
//                                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
//
//                                    if (updateQty == 0) {
//                                        Toast.makeText(mContext, "Please enter zero above quantity ", Toast.LENGTH_SHORT).show();
//                                        InputMethodManager imm1 = (InputMethodManager) mContext.getSystemService(Activity.INPUT_METHOD_SERVICE);
//                                        imm1.hideSoftInputFromWindow(v.getWindowToken(), 0);
//                                    } else {
//                                        FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF).document(productDetails.getProduct_id()).update(Constant.PRODUCT_QUANTITY_KEY, updateQty);
//                                        Toast.makeText(mContext, "Updated", Toast.LENGTH_SHORT).show();
//                                        mDialogProductOpenBinding.minus.setVisibility(View.VISIBLE);
//                                        mDialogProductOpenBinding.plus.setVisibility(View.VISIBLE);
//                                        mDialogProductOpenBinding.btnUpdateQty.setVisibility(View.GONE);
//                                        InputMethodManager imm1 = (InputMethodManager) mContext.getSystemService(Activity.INPUT_METHOD_SERVICE);
//                                        imm1.hideSoftInputFromWindow(v.getWindowToken(), 0);
//                                    }
//                                }
//                            }
//                        });
//                        return false;
//                    }
//                });
//                //---------------------qty--update--with--plus--&--minus--&---usingEditText----[end]------------------------
//
//
//                mDialogProductOpenBinding.btnOrderNow.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//
//                        String note = mDialogProductOpenBinding.edtNote.getText().toString();
//                        InputMethodManager imm1 = (InputMethodManager) mContext.getSystemService(Activity.INPUT_METHOD_SERVICE);
//                        imm1.hideSoftInputFromWindow(v.getWindowToken(), 0);
//                        final String product_id = productDetails.getProduct_id();
//                        DocumentReference myCartRef = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF).document(product_id);
//                        myCartRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
//                            @Override
//                            public void onSuccess(DocumentSnapshot documentSnapshot) {
//                                if (TextUtils.isEmpty(mDialogProductOpenBinding.psc.getText().toString())) {
//                                    return;
//                                }
//                                int currentPsc = Integer.parseInt(mDialogProductOpenBinding.psc.getText().toString());
//                                if (TextUtils.isEmpty(mDialogProductOpenBinding.psc.getText().toString()) || currentPsc == 0) {
//                                    currentPsc = 1;
//                                }
//
//                                String product_id_final = mDialogProductOpenBinding.proId.getText().toString();
//                                if (documentSnapshot.exists()) {
//                                    MyCart myCart1 = documentSnapshot.toObject(MyCart.class);
//
//
////                                    MyCart myCart = new MyCart(currentPsc, cat_id, cat_name, pro_code_final, pro_name, pro_size, pro_weight, sub_cat_id, sub_cat_name, img, product_id,note);
//                                    MyCart myCart = new MyCart(currentPsc, cat_id, cat_name, product_id_final, pro_name, pro_size, pro_weight, sub_cat_id, sub_cat_name, img, product_id, note);
//                                    CollectionReference mCollectionReferenceCart = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF);
//                                    mCollectionReferenceCart.document(product_id).set(myCart).addOnCompleteListener(new OnCompleteListener<Void>() {
//                                        @RequiresApi(api = Build.VERSION_CODES.M)
//                                        @Override
//                                        public void onComplete(@NonNull Task<Void> task) {
//                                            if (task.isComplete()) {
//                                                Toast.makeText(mContext, "Added to Cart", Toast.LENGTH_LONG).show();
////                                                mContext.startActivity(new Intent(mContext, CartActivity.class));
//                                            }
//                                        }
//                                    });
//                                } else {
////                                    MyCart myCart = new MyCart(currentPsc, cat_id, cat_name, pro_code_final, pro_name, pro_size, pro_weight, sub_cat_id, sub_cat_name, img, product_id,note);
//                                    MyCart myCart = new MyCart(currentPsc, cat_id, cat_name, product_id_final, pro_name, pro_size, pro_weight, sub_cat_id, sub_cat_name, img, product_id, note);
//                                    CollectionReference mCollectionReferenceCart = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF);
//                                    mCollectionReferenceCart.document(product_id).set(myCart).addOnCompleteListener(new OnCompleteListener<Void>() {
//                                        @RequiresApi(api = Build.VERSION_CODES.M)
//                                        @Override
//                                        public void onComplete(@NonNull Task<Void> task) {
//                                            if (task.isComplete()) {
//                                                Toast.makeText(mContext, "Added to Cart", Toast.LENGTH_LONG).show();
////                                                mContext.startActivity(new Intent(mContext, CartActivity.class));
//
//                                            }
//                                        }
//                                    });
//                                }
//                            }
//                        });
//                        //----------------------------------------
//                    }
//                });
//
//
//                //------------------------REAL TIME COUNT CART ---------START----------------------------------------
//                FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF).document(product_id).addSnapshotListener(new EventListener<DocumentSnapshot>() {
//                    @Override
//                    public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
//                        if (documentSnapshot != null) {
//                            MyCart myCart = documentSnapshot.toObject(MyCart.class);
//                            if (myCart != null) {
//
//                                int qty = myCart.getProduct_quantity();
//                                mDialogProductOpenBinding.psc.setText(String.valueOf(qty));
//                            }
//                        }
//                    }
//                });
//
//                //------------------------REAL TIME COUNT CART ---------END----------------------------------------
//
//                mDialogProductOpenBinding.plus.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//
//                        //--------------------------------
//                        final String product_id = productDetails.getProduct_id();
//                        String product_id_final = mDialogProductOpenBinding.proId.getText().toString();
//
//                        DocumentReference myCartRef = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF).document(product_id);
//                        myCartRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
//                            @Override
//                            public void onSuccess(DocumentSnapshot documentSnapshot) {
//                                if (documentSnapshot.exists()) {
//                                    MyCart myCart1 = documentSnapshot.toObject(MyCart.class);
////                                    MyCart myCart = new MyCart(myCart1.getProduct_quantity() + 1, cat_id, cat_name, pro_code_final, pro_name, pro_size, pro_weight, sub_cat_id, sub_cat_name, img, product_id,null);
//                                    MyCart myCart = new MyCart(myCart1.getProduct_quantity() + 1, cat_id, cat_name, product_id_final, pro_name, pro_size, pro_weight, sub_cat_id, sub_cat_name, img, product_id, null);
//                                    CollectionReference mCollectionReferenceCart = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF);
//                                    mCollectionReferenceCart.document(product_id).set(myCart).addOnCompleteListener(new OnCompleteListener<Void>() {
//                                        @RequiresApi(api = Build.VERSION_CODES.M)
//                                        @Override
//                                        public void onComplete(@NonNull Task<Void> task) {
//                                            if (task.isComplete()) {
////                                            Toast.makeText(mContext, "Added to Cart", Toast.LENGTH_LONG).show();
//                                            }
//                                        }
//                                    });
//                                } else {
////                                    MyCart myCart = new MyCart(1, cat_id, cat_name, pro_code_final, pro_name, pro_size, pro_weight, sub_cat_id, sub_cat_name, img, product_id,null);
//                                    MyCart myCart = new MyCart(1, cat_id, cat_name, product_id_final, pro_name, pro_size, pro_weight, sub_cat_id, sub_cat_name, img, product_id, null);
//                                    CollectionReference mCollectionReferenceCart = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF);
//                                    mCollectionReferenceCart.document(product_id).set(myCart).addOnCompleteListener(new OnCompleteListener<Void>() {
//                                        @RequiresApi(api = Build.VERSION_CODES.M)
//                                        @Override
//                                        public void onComplete(@NonNull Task<Void> task) {
//                                            if (task.isComplete()) {
////                                                Toast.makeText(mContext, "Added to Cart", Toast.LENGTH_LONG).show();
//                                            }
//                                        }
//                                    });
//                                }
//                            }
//                        });
//                        //--------------------------------
//                    }
//                });
//
//
//                mDialogProductOpenBinding.minus.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//
//
//                        //-------------------------
//                        final String product_id = productDetails.getProduct_id();
//                        String product_id_final = mDialogProductOpenBinding.proId.getText().toString();
//
//                        DocumentReference myCartRef = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF).document(product_id);
//                        myCartRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
//                            @Override
//                            public void onSuccess(DocumentSnapshot documentSnapshot) {
//
//                                if (documentSnapshot.exists()) {
//                                    final MyCart myCart1 = documentSnapshot.toObject(MyCart.class);
////                                    MyCart myCart = new MyCart(myCart1.getProduct_quantity() - 1, cat_id, cat_name, pro_code_final, pro_name, pro_size, pro_weight, sub_cat_id, sub_cat_name, img, product_id,null);
//                                    MyCart myCart = new MyCart(myCart1.getProduct_quantity() - 1, cat_id, cat_name, product_id_final, pro_name, pro_size, pro_weight, sub_cat_id, sub_cat_name, img, product_id, null);
//                                    CollectionReference mCollectionReferenceCart = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF);
//                                    mCollectionReferenceCart.document(product_id).set(myCart).addOnCompleteListener(new OnCompleteListener<Void>() {
//                                        @RequiresApi(api = Build.VERSION_CODES.M)
//                                        @Override
//                                        public void onComplete(@NonNull Task<Void> task) {
//                                            if (task.isComplete()) {
//                                            }
//                                        }
//                                    });
//
//                                    if (myCart1.getProduct_quantity() - 1 == 0) {
//
//                                        mCollectionReferenceCart.document(myCart1.getProduct_id()).delete().addOnSuccessListener(new OnSuccessListener<Void>() {
//                                            @Override
//                                            public void onSuccess(Void aVoid) {
//                                            }
//                                        });
//                                    }
//
//                                } else {
//
//                                }
//
//
//                            }
//                        });
//                        //-------------------------
//                    }
//                });
//
//
//                dialog.setContentView(mDialogProductOpenBinding.getRoot());
//                dialog.show();
//
//
//            }
//        });
//
//        //---------------dialog-----------------end--------------------------------------------------------
//
//
//    }
//
//
//    @Override
//    protected void onLoadingStateChanged(@NonNull LoadingState state) {
//        super.onLoadingStateChanged(state);
//
//        switch (state) {
//            case LOADING_INITIAL:
//                Log.d("PAGING_LOG", "Loading Initial Data");
//                break;
//
//            case LOADING_MORE:
//                Log.d("PAGING_LOG", "Loading Next Page");
//                break;
//
//            case FINISHED:
//                Log.d("PAGING_LOG", "All Data Loaded");
//                break;
//
//            case ERROR:
//                Log.d("PAGING_LOG", "Error Loading Data");
//                break;
//
//            case LOADED:
//                Log.d("PAGING_LOG", "Total Items Loaded :");
//                break;
//        }
//
//
//    }
//
//    @NonNull
//
//    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        if (mLayoutInflater == null) {
//            mLayoutInflater = LayoutInflater.from(parent.getContext());
//        }
//
//        RowCatWiseProductListBinding mRowCatWiseProductListBinding = DataBindingUtil.inflate(mLayoutInflater, R.layout.row_cat_wise_product_list, parent, false);
//        return new ViewHolder(mRowCatWiseProductListBinding);
//    }
//
//
//
//    class ViewHolder extends RecyclerView.ViewHolder {
//        RowCatWiseProductListBinding mRowCatWiseProductListBinding;
//
//        public ViewHolder(@NonNull RowCatWiseProductListBinding itemView) {
//            super(itemView.getRoot());
//            mRowCatWiseProductListBinding = itemView;
//        }
//
//    }
//}


////------old data
//
//
package com.psjewels.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.media.Image;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.view.menu.MenuView;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.SizeReadyCallback;
import com.bumptech.glide.request.transition.Transition;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.internal.NavigationMenuItemView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.messaging.Constants;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.psjewels.R;
import com.psjewels.activity.CartActivity;
import com.psjewels.databinding.DialogProductOpenBinding;
import com.psjewels.databinding.RowCatWiseProductListBinding;
import com.psjewels.model.ImgList;
import com.psjewels.model.MyCart;
import com.psjewels.model.ProductDeatils;
import com.psjewels.utils.Constant;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static androidx.constraintlayout.widget.Constraints.TAG;
import static java.security.AccessController.getContext;

public class CatWiseProductListAdapter extends RecyclerView.Adapter<CatWiseProductListAdapter.ViewHolder> {


    private static final String TAG = "Adadpter";
    LayoutInflater mLayoutInflater;
    private Context mContext;
    private List<DocumentSnapshot> mSubCategoryLists;
    AlertDialog.Builder builder;
    ProgressDialog mProgressDialog;

    String CurrentId;
    private FirebaseUser mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
    int id = 0;
    ArrayList<ImgList> mImgLists;

    private int lastPostion = -1;


    private static final long MiB = 1024 * 1024;
    private static final long KiB = 1024;


    public CatWiseProductListAdapter(List<DocumentSnapshot> mSubCategoryLists, Context mContext) {
        this.mSubCategoryLists = mSubCategoryLists;
        this.mContext = mContext;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        if (mLayoutInflater == null) {
            mLayoutInflater = LayoutInflater.from(parent.getContext());
        }

        RowCatWiseProductListBinding mRowCatWiseProductListBinding = DataBindingUtil.inflate(mLayoutInflater, R.layout.row_cat_wise_product_list, parent, false);
        return new ViewHolder(mRowCatWiseProductListBinding);

    }

    @Override
    public void onViewDetachedFromWindow(@NonNull ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.itemView.clearAnimation();
    }

//    setFadeAnimation(productListViewHolder.itemView); //----put in onBindViewHolder only---animation to show like blinking slow motions
//
//    public void setFadeAnimation(View view) {
//        AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
//        anim.setDuration(5000);
//        view.startAnimation(anim);
//    }


//    setAnimation(productListViewHolder.itemView); //---youtube vdo follow 3 mins
//    private void setAnimation(View view) {
//        Animation animation = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left);
//        view.setAnimation(animation);
//    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder productListViewHolder, final int i) {


        if (productListViewHolder instanceof CatWiseProductListAdapter.ViewHolder) {
            if (productListViewHolder.getAdapterPosition() > lastPostion) {
                Animation animation = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left);
                ((ViewHolder) productListViewHolder).itemView.startAnimation(animation);
                lastPostion = productListViewHolder.getAdapterPosition();

            }
        }


        final String cat_id, cat_name, pro_code_final, pro_code, img, pro_name, pro_size, pro_weight, product_id, sub_cat_id, sub_cat_name;
        final int product_qty, pro_price, pro_total_price, pro_code_number;


//        mCollectionReference = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF);
        final ProductDeatils productDetails = mSubCategoryLists.get(i).toObject(ProductDeatils.class);

        productListViewHolder.mRowCatWiseProductListBinding.proId.setText(productDetails.getPro_code() + productDetails.getPro_code_number());
//        productListViewHolder.mRowCatWiseProductListBinding.proName.setText(productDetails.getPro_name());
        productListViewHolder.mRowCatWiseProductListBinding.proSize.setText(productDetails.getPro_size());
        productListViewHolder.mRowCatWiseProductListBinding.proWT.setText(productDetails.getPro_weight());

        Picasso
                .get()
                .load(productDetails.getImg())
//                .resize(10,10)
                .placeholder(R.drawable.placeholder)
                .fit().centerInside()
                .into(productListViewHolder.mRowCatWiseProductListBinding.proImg);


        //------------------
//        RequestOptions myOptions = new RequestOptions()
//                .override(100, 100);
//
//        Glide.with(mContext)
//                .asBitmap()
//                .apply(myOptions)
//                .load(productDetails.getImg())
//                .into(productListViewHolder.mRowCatWiseProductListBinding.proImg);


        cat_id = productDetails.getCat_id();
        cat_name = productDetails.getCat_name();
        pro_code = productDetails.getPro_code() + productDetails.getPro_code_number();
        pro_code_number = productDetails.getPro_code_number();

        pro_code_final = pro_code + pro_code_number;
        img = productDetails.getImg();
        pro_name = productDetails.getPro_name();
        pro_size = productDetails.getPro_size();
        pro_weight = productDetails.getPro_weight();
        product_qty = Integer.parseInt(productListViewHolder.mRowCatWiseProductListBinding.psc.getText().toString());
        product_id = productDetails.getProduct_id();
        sub_cat_id = productDetails.getSub_cat_id();
        sub_cat_name = productDetails.getSub_cat_name();

        productListViewHolder.mRowCatWiseProductListBinding.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String product_id = productDetails.getProduct_id();
                DocumentReference myCartRef = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF).document(product_id);
                myCartRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if (documentSnapshot.exists()) {
                            MyCart myCart1 = documentSnapshot.toObject(MyCart.class);
                            MyCart myCart = new MyCart(myCart1.getProduct_quantity() + 1, cat_id, cat_name, pro_code, pro_name, pro_size, pro_weight, sub_cat_id, sub_cat_name, img, product_id, null);
                            CollectionReference mCollectionReferenceCart = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF);
                            mCollectionReferenceCart.document(product_id).set(myCart).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @RequiresApi(api = Build.VERSION_CODES.M)
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isComplete()) {
//                                            Toast.makeText(mContext, "Added to Cart", Toast.LENGTH_LONG).show();
                                    }
                                }
                            });
                        } else {
                            MyCart myCart = new MyCart(1, cat_id, cat_name, pro_code, pro_name, pro_size, pro_weight, sub_cat_id, sub_cat_name, img, product_id, null);
                            CollectionReference mCollectionReferenceCart = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF);
                            mCollectionReferenceCart.document(product_id).set(myCart).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @RequiresApi(api = Build.VERSION_CODES.M)
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isComplete()) {
//                                        Toast.makeText(mContext, "Added to Cart", Toast.LENGTH_LONG).show();
                                    }
                                }
                            });
                        }
                    }
                });

            }
        });

        productListViewHolder.mRowCatWiseProductListBinding.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String product_id = productDetails.getProduct_id();
                DocumentReference myCartRef = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF).document(product_id);
                myCartRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {

                        if (documentSnapshot.exists()) {
                            final MyCart myCart1 = documentSnapshot.toObject(MyCart.class);
                            MyCart myCart = new MyCart(myCart1.getProduct_quantity() - 1, cat_id, cat_name, pro_code, pro_name, pro_size, pro_weight, sub_cat_id, sub_cat_name, img, product_id, null);
                            CollectionReference mCollectionReferenceCart = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF);
                            mCollectionReferenceCart.document(product_id).set(myCart).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @RequiresApi(api = Build.VERSION_CODES.M)
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isComplete()) {
                                    }
                                }
                            });

                            if (myCart1.getProduct_quantity() - 1 == 0) {
                                mCollectionReferenceCart.document(myCart1.getProduct_id()).delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                    }
                                });
                            }

                        } else {

                        }


                    }
                });

            }
        });


        //------------------------REAL TIME COUNT CART ---------START----------------------------------------
        FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF).document(product_id).addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                if (documentSnapshot != null) {
                    MyCart myCart = documentSnapshot.toObject(MyCart.class);
                    if (myCart != null) {

                        int qty = myCart.getProduct_quantity();
                        productListViewHolder.mRowCatWiseProductListBinding.psc.setText(String.valueOf(qty));
                    }
                }
            }
        });

        //------------------------REAL TIME COUNT CART ---------END----------------------------------------


//        productListViewHolder.mRowCatWiseProductListBinding.btnOrderNow.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//
//                final DialogProductOpenBinding mDialogProductOpenBinding;
//                final Dialog dialog = new Dialog(mContext, R.style.MyAlertDialogTheme);
//                mDialogProductOpenBinding = DataBindingUtil.inflate(LayoutInflater.from(mContext), R.layout.dialog_product_open, null, false);
//                mDialogProductOpenBinding.proWhereComes.setText(productDetails.getCat_name() + " > " + productDetails.getSub_cat_name());
//                mDialogProductOpenBinding.proId.setText(productDetails.getPro_code() + String.valueOf(productDetails.getPro_code_number()));
////                mDialogProductOpenBinding.proName.setText(productDetails.getPro_name());
//                mDialogProductOpenBinding.proSize.setText(productDetails.getPro_size());
//                mDialogProductOpenBinding.proWT.setText(productDetails.getPro_weight());
//                Picasso.get().load(productDetails.getImg()).placeholder(R.drawable.placeholder).fit().centerInside().into(mDialogProductOpenBinding.proImg);
//
//
//                //--------------------------------------------------------------------------------------------------------------------------------------
//                mImgLists = new ArrayList<>();
//
//                FirebaseFirestore.getInstance().collection(Constant.PRODUCT_LIST_REF).document(product_id).collection(Constant.IMG_LIST).addSnapshotListener(new EventListener<QuerySnapshot>() {
//                    @Override
//                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
//                        if (queryDocumentSnapshots != null) {
//                            for (final DocumentChange dc : queryDocumentSnapshots.getDocumentChanges()) {
//                                switch (dc.getType()) {
//                                    case ADDED:
//                                        String img = dc.getDocument().getString(Constant.IMG_KEY);
////                                        Toast.makeText(mContext, "img :- " +img, Toast.LENGTH_SHORT).show();
//                                        Log.d(Constant.TAG, "ID:-" + dc.getDocument().getId());
//
//                                        ImgList mImgList = new ImgList(img, id);
//                                        mImgLists.add(mImgList);
//
//                                        id++;
//
//
//                                        break;
//                                    case MODIFIED:
//                                        break;
//                                    case REMOVED:
//                                        break;
//                                }
//                            }
//
////                            ImageAdapter adapter = new ImageAdapter(mContext);
////                            mDialogProductOpenBinding.viewPager.setAdapter(adapter);
//
//                            MultiImgListAdapter mMultiImgListAdapter = new MultiImgListAdapter(mContext, mImgLists);
//                            mDialogProductOpenBinding.viewPager.setAdapter(mMultiImgListAdapter);
//                            if (mImgLists.size() == 1) {
//
//                            } else {
//                                mDialogProductOpenBinding.dotsIndicator.setViewPager(mDialogProductOpenBinding.viewPager);
//
//                            }
//                        }
//                    }
//                });
//                //--------------------------------------------------------------------------------------------------------------------------------------
//
//
//                mDialogProductOpenBinding.imgclose.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        dialog.dismiss();
//                    }
//                });
//
//
//                //---------------------qty--update--with--plus--&--minus--&---usingEditText----[start]------------------------
//                mDialogProductOpenBinding.psc.setOnTouchListener(new View.OnTouchListener() {
//                    @Override
//                    public boolean onTouch(View v, MotionEvent event) {
//                        mDialogProductOpenBinding.minus.setVisibility(View.GONE);
//                        mDialogProductOpenBinding.plus.setVisibility(View.GONE);
//
//                        mDialogProductOpenBinding.btnUpdateQty.setVisibility(View.VISIBLE);
//
//
//                        mDialogProductOpenBinding.btnUpdateQty.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//
//                                if (mDialogProductOpenBinding.psc.getText().toString().length() == 0) {
//                                    Toast.makeText(mContext, "Please enter qty", Toast.LENGTH_SHORT).show();
//                                } else {
//                                    int updateQty = Integer.parseInt(mDialogProductOpenBinding.psc.getText().toString());
//                                    InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Activity.INPUT_METHOD_SERVICE);
//                                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
//
//                                    if (updateQty == 0) {
//                                        Toast.makeText(mContext, "Please enter zero above quantity ", Toast.LENGTH_SHORT).show();
//                                        InputMethodManager imm1 = (InputMethodManager) mContext.getSystemService(Activity.INPUT_METHOD_SERVICE);
//                                        imm1.hideSoftInputFromWindow(v.getWindowToken(), 0);
//                                    } else {
//                                        FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF).document(productDetails.getProduct_id()).update(Constant.PRODUCT_QUANTITY_KEY, updateQty);
//                                        Toast.makeText(mContext, "Updated", Toast.LENGTH_SHORT).show();
//                                        mDialogProductOpenBinding.minus.setVisibility(View.VISIBLE);
//                                        mDialogProductOpenBinding.plus.setVisibility(View.VISIBLE);
//                                        mDialogProductOpenBinding.btnUpdateQty.setVisibility(View.GONE);
//                                        InputMethodManager imm1 = (InputMethodManager) mContext.getSystemService(Activity.INPUT_METHOD_SERVICE);
//                                        imm1.hideSoftInputFromWindow(v.getWindowToken(), 0);
//                                    }
//                                }
//                            }
//                        });
//                        return false;
//                    }
//                });
//                //---------------------qty--update--with--plus--&--minus--&---usingEditText----[end]------------------------
//
//
//                mDialogProductOpenBinding.btnOrderNow.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//
//
//                        final String product_id = productDetails.getProduct_id();
//                        DocumentReference myCartRef = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF).document(product_id);
//                        myCartRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
//                            @Override
//                            public void onSuccess(DocumentSnapshot documentSnapshot) {
//                                if (documentSnapshot.exists()) {
//                                   mContext.startActivity(new Intent(mContext,CartActivity.class));
//                                } else {
//                                    MyCart myCart = new MyCart(1, cat_id, cat_name, pro_code_final, pro_name, pro_size, pro_weight, sub_cat_id, sub_cat_name, img, product_id);
//                                    CollectionReference mCollectionReferenceCart = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF);
//                                    mCollectionReferenceCart.document(product_id).set(myCart).addOnCompleteListener(new OnCompleteListener<Void>() {
//                                        @RequiresApi(api = Build.VERSION_CODES.M)
//                                        @Override
//                                        public void onComplete(@NonNull Task<Void> task) {
//                                            if (task.isComplete()) {
////                                                Toast.makeText(mContext, "Added to Cart", Toast.LENGTH_LONG).show();
//                                                mContext.startActivity(new Intent(mContext,CartActivity.class));
//
//                                            }
//                                        }
//                                    });
//                                }
//                            }
//                        });
//                    }
//                });
//
//
//                //------------------------REAL TIME COUNT CART ---------START----------------------------------------
//                FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF).document(product_id).addSnapshotListener(new EventListener<DocumentSnapshot>() {
//                    @Override
//                    public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
//                        if (documentSnapshot != null) {
//                            MyCart myCart = documentSnapshot.toObject(MyCart.class);
//                            if (myCart != null) {
//
//                                int qty = myCart.getProduct_quantity();
//                                mDialogProductOpenBinding.psc.setText(String.valueOf(qty));
//                            }
//                        }
//                    }
//                });
//
//                //------------------------REAL TIME COUNT CART ---------END----------------------------------------
//
//                mDialogProductOpenBinding.plus.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//
//                        final String product_id = productDetails.getProduct_id();
//                        DocumentReference myCartRef = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF).document(product_id);
//                        myCartRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
//                            @Override
//                            public void onSuccess(DocumentSnapshot documentSnapshot) {
//                                if (documentSnapshot.exists()) {
//                                    MyCart myCart1 = documentSnapshot.toObject(MyCart.class);
//                                    MyCart myCart = new MyCart(myCart1.getProduct_quantity() + 1, cat_id, cat_name, pro_code, pro_name, pro_size, pro_weight, sub_cat_id, sub_cat_name, img, product_id);
//                                    CollectionReference mCollectionReferenceCart = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF);
//                                    mCollectionReferenceCart.document(product_id).set(myCart).addOnCompleteListener(new OnCompleteListener<Void>() {
//                                        @RequiresApi(api = Build.VERSION_CODES.M)
//                                        @Override
//                                        public void onComplete(@NonNull Task<Void> task) {
//                                            if (task.isComplete()) {
////                                            Toast.makeText(mContext, "Added to Cart", Toast.LENGTH_LONG).show();
//                                            }
//                                        }
//                                    });
//                                } else {
//                                    MyCart myCart = new MyCart(1, cat_id, cat_name, pro_code, pro_name, pro_size, pro_weight, sub_cat_id, sub_cat_name, img, product_id);
//                                    CollectionReference mCollectionReferenceCart = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF);
//                                    mCollectionReferenceCart.document(product_id).set(myCart).addOnCompleteListener(new OnCompleteListener<Void>() {
//                                        @RequiresApi(api = Build.VERSION_CODES.M)
//                                        @Override
//                                        public void onComplete(@NonNull Task<Void> task) {
//                                            if (task.isComplete()) {
//                                                Toast.makeText(mContext, "Added to Cart", Toast.LENGTH_LONG).show();
//                                            }
//                                        }
//                                    });
//                                }
//                            }
//                        });
//
//                    }
//                });
//
//
//                mDialogProductOpenBinding.minus.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//
//                        final String product_id = productDetails.getProduct_id();
//                        DocumentReference myCartRef = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF).document(product_id);
//                        myCartRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
//                            @Override
//                            public void onSuccess(DocumentSnapshot documentSnapshot) {
//
//                                if (documentSnapshot.exists()) {
//                                    final MyCart myCart1 = documentSnapshot.toObject(MyCart.class);
//                                    MyCart myCart = new MyCart(myCart1.getProduct_quantity() - 1, cat_id, cat_name, pro_code, pro_name, pro_size, pro_weight, sub_cat_id, sub_cat_name, img, product_id);
//                                    CollectionReference mCollectionReferenceCart = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF);
//                                    mCollectionReferenceCart.document(product_id).set(myCart).addOnCompleteListener(new OnCompleteListener<Void>() {
//                                        @RequiresApi(api = Build.VERSION_CODES.M)
//                                        @Override
//                                        public void onComplete(@NonNull Task<Void> task) {
//                                            if (task.isComplete()) {
//                                            }
//                                        }
//                                    });
//
//                                    if (myCart1.getProduct_quantity() - 1 == 0) {
//
//                                        mCollectionReferenceCart.document(myCart1.getProduct_id()).delete().addOnSuccessListener(new OnSuccessListener<Void>() {
//                                            @Override
//                                            public void onSuccess(Void aVoid) {
//                                            }
//                                        });
//                                    }
//
//                                } else {
//
//                                }
//
//
//                            }
//                        });
//
//                    }
//                });
//
//                dialog.setContentView(mDialogProductOpenBinding.getRoot());
//                dialog.show();
//
//
////---------------------------------------cart---value---store-------start------------------------
////                final String product_id = productDetails.getProduct_id();
////                DocumentReference myCartRef = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF).document(product_id);
////                myCartRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
////                    @Override
////                    public void onSuccess(DocumentSnapshot documentSnapshot) {
////                        if (documentSnapshot.exists()) {
////                            MyCart myCart1 = documentSnapshot.toObject(MyCart.class);
////
////                            int currentPsc = Integer.parseInt(productListViewHolder.mRowCatWiseProductListBinding.psc.getText().toString());
////                            MyCart myCart = new MyCart(currentPsc, cat_id, cat_name, pro_code, pro_name, pro_size, pro_weight, sub_cat_id, sub_cat_name, img, product_id);
////                            CollectionReference mCollectionReferenceCart = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF);
////                            mCollectionReferenceCart.document(product_id).set(myCart).addOnCompleteListener(new OnCompleteListener<Void>() {
////                                @RequiresApi(api = Build.VERSION_CODES.M)
////                                @Override
////                                public void onComplete(@NonNull Task<Void> task) {
////                                    if (task.isComplete()) {
//////                                            Toast.makeText(mContext, "Added to Cart", Toast.LENGTH_LONG).show();
////                                    }
////                                }
////                            });
////                        } else {
////                            MyCart myCart = new MyCart(1, cat_id, cat_name, pro_code, pro_name, pro_size, pro_weight, sub_cat_id, sub_cat_name, img, product_id);
////                            CollectionReference mCollectionReferenceCart = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF);
////                            mCollectionReferenceCart.document(product_id).set(myCart).addOnCompleteListener(new OnCompleteListener<Void>() {
////                                @RequiresApi(api = Build.VERSION_CODES.M)
////                                @Override
////                                public void onComplete(@NonNull Task<Void> task) {
////                                    if (task.isComplete()) {
////                                        Toast.makeText(mContext, "Added to Cart", Toast.LENGTH_LONG).show();
////                                    }
////                                }
////                            });
////                        }
////                    }
////                });
//                //---------------------------------------cart---value---store-------end------------------------
//
//            }
//        });


        //---------------dialog-----start------------------------------------------------------------------

        productListViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final Dialog dialog = new Dialog(mContext, R.style.MyAlertDialogTheme);
                DialogProductOpenBinding mDialogProductOpenBinding = DataBindingUtil.inflate(LayoutInflater.from(mContext), R.layout.dialog_product_open, null, false);
                mDialogProductOpenBinding.proWhereComes.setText(productDetails.getCat_name() + " > " + productDetails.getSub_cat_name());
                mDialogProductOpenBinding.proId.setText(productDetails.getPro_code() + productDetails.getPro_code_number());
//                mDialogProductOpenBinding.proName.setText(productDetails.getPro_name());
                mDialogProductOpenBinding.proSize.setText(productDetails.getPro_size());
                mDialogProductOpenBinding.proWT.setText(productDetails.getPro_weight());

//                Picasso.get().load(productDetails.getImg()).placeholder(R.drawable.placeholder).fit().centerInside().into(mDialogProductOpenBinding.proImg);


                //--------------------------------------------------------------------------------------------------------------------------------------
                mImgLists = new ArrayList<>();

                FirebaseFirestore.getInstance().collection(Constant.PRODUCT_LIST_REF).document(product_id).collection(Constant.IMG_LIST).addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                        if (queryDocumentSnapshots != null) {
                            for (final DocumentChange dc : queryDocumentSnapshots.getDocumentChanges()) {
                                switch (dc.getType()) {
                                    case ADDED:
                                        String img = dc.getDocument().getString(Constant.IMG_KEY);
//                                        Toast.makeText(mContext, "img :- " +img, Toast.LENGTH_SHORT).show();
                                        Log.d(Constant.TAG, "ID:-" + dc.getDocument().getId());


                                        ImgList mImgList = new ImgList(img, id);
                                        mImgLists.add(mImgList);

                                        id++;


                                        break;
                                    case MODIFIED:
                                        break;
                                    case REMOVED:
                                        break;
                                }
                            }

//                            ImageAdapter adapter = new ImageAdapter(mContext);
//                            mDialogProductOpenBinding.viewPager.setAdapter(adapter);

                            MultiImgListAdapter mMultiImgListAdapter = new MultiImgListAdapter(mContext, mImgLists);
                            mDialogProductOpenBinding.viewPager.setAdapter(mMultiImgListAdapter);
                            if (mImgLists.size() == 1) {

                            } else {
                                mDialogProductOpenBinding.dotsIndicator.setViewPager(mDialogProductOpenBinding.viewPager);

                            }
                        }
                    }
                });
                //--------------------------------------------------------------------------------------------------------------------------------------


                mDialogProductOpenBinding.imgclose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                //---------------------qty--update--with--plus--&--minus--&---usingEditText----[start]------------------------
                mDialogProductOpenBinding.psc.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        mDialogProductOpenBinding.minus.setVisibility(View.GONE);
                        mDialogProductOpenBinding.plus.setVisibility(View.GONE);

                        mDialogProductOpenBinding.btnUpdateQty.setVisibility(View.VISIBLE);


                        mDialogProductOpenBinding.btnUpdateQty.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if (mDialogProductOpenBinding.psc.getText().toString().length() == 0) {
                                    Toast.makeText(mContext, "Please enter qty", Toast.LENGTH_SHORT).show();
                                } else {

                                    int updateQty = Integer.parseInt(mDialogProductOpenBinding.psc.getText().toString());
                                    InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Activity.INPUT_METHOD_SERVICE);
                                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                                    if (updateQty == 0) {
                                        Toast.makeText(mContext, "Please enter zero above quantity ", Toast.LENGTH_SHORT).show();
                                        InputMethodManager imm1 = (InputMethodManager) mContext.getSystemService(Activity.INPUT_METHOD_SERVICE);
                                        imm1.hideSoftInputFromWindow(v.getWindowToken(), 0);
                                    } else {
                                        FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF).document(productDetails.getProduct_id()).update(Constant.PRODUCT_QUANTITY_KEY, updateQty);
                                        Toast.makeText(mContext, "Updated", Toast.LENGTH_SHORT).show();
                                        mDialogProductOpenBinding.minus.setVisibility(View.VISIBLE);
                                        mDialogProductOpenBinding.plus.setVisibility(View.VISIBLE);
                                        mDialogProductOpenBinding.btnUpdateQty.setVisibility(View.GONE);
                                        InputMethodManager imm1 = (InputMethodManager) mContext.getSystemService(Activity.INPUT_METHOD_SERVICE);
                                        imm1.hideSoftInputFromWindow(v.getWindowToken(), 0);
                                    }
                                }
                            }
                        });
                        return false;
                    }
                });
                //---------------------qty--update--with--plus--&--minus--&---usingEditText----[end]------------------------


                mDialogProductOpenBinding.btnOrderNow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String note = mDialogProductOpenBinding.edtNote.getText().toString();
                        InputMethodManager imm1 = (InputMethodManager) mContext.getSystemService(Activity.INPUT_METHOD_SERVICE);
                        imm1.hideSoftInputFromWindow(v.getWindowToken(), 0);
                        final String product_id = productDetails.getProduct_id();
                        DocumentReference myCartRef = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF).document(product_id);
                        myCartRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                            @Override
                            public void onSuccess(DocumentSnapshot documentSnapshot) {
                                if (TextUtils.isEmpty(mDialogProductOpenBinding.psc.getText().toString())) {
                                    return;
                                }
                                int currentPsc = Integer.parseInt(mDialogProductOpenBinding.psc.getText().toString());
                                if (TextUtils.isEmpty(mDialogProductOpenBinding.psc.getText().toString()) || currentPsc == 0) {
                                    currentPsc = 1;
                                }

                                String product_id_final = mDialogProductOpenBinding.proId.getText().toString();
                                if (documentSnapshot.exists()) {
                                    MyCart myCart1 = documentSnapshot.toObject(MyCart.class);


//                                    MyCart myCart = new MyCart(currentPsc, cat_id, cat_name, pro_code_final, pro_name, pro_size, pro_weight, sub_cat_id, sub_cat_name, img, product_id,note);
                                    MyCart myCart = new MyCart(currentPsc, cat_id, cat_name, product_id_final, pro_name, pro_size, pro_weight, sub_cat_id, sub_cat_name, img, product_id, note);
                                    CollectionReference mCollectionReferenceCart = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF);
                                    mCollectionReferenceCart.document(product_id).set(myCart).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @RequiresApi(api = Build.VERSION_CODES.M)
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isComplete()) {
                                                Toast.makeText(mContext, "Added to Cart", Toast.LENGTH_LONG).show();
//                                                mContext.startActivity(new Intent(mContext, CartActivity.class));
                                                long time_stamp= System.currentTimeMillis();
                                                FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid())
                                                        .collection(Constant.ITEM_LIST_REF).document(product_id)
                                                        .update("cart_timestamp",time_stamp);
                                            }
                                        }
                                    });
                                } else {
//                                    MyCart myCart = new MyCart(currentPsc, cat_id, cat_name, pro_code_final, pro_name, pro_size, pro_weight, sub_cat_id, sub_cat_name, img, product_id,note);
                                    MyCart myCart = new MyCart(currentPsc, cat_id, cat_name, product_id_final, pro_name, pro_size, pro_weight, sub_cat_id, sub_cat_name, img, product_id, note);
                                    CollectionReference mCollectionReferenceCart = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF);
                                    mCollectionReferenceCart.document(product_id).set(myCart).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @RequiresApi(api = Build.VERSION_CODES.M)
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isComplete()) {
                                                Toast.makeText(mContext, "Added to Cart", Toast.LENGTH_LONG).show();
//                                                mContext.startActivity(new Intent(mContext, CartActivity.class));
                                                long time_stamp= System.currentTimeMillis();
                                                FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid())
                                                        .collection(Constant.ITEM_LIST_REF).document(product_id)
                                                        .update("cart_timestamp",time_stamp);
                                            }
                                        }
                                    });
                                }
                            }
                        });
                        //----------------------------------------
                    }
                });


                //------------------------REAL TIME COUNT CART ---------START----------------------------------------
                FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF).document(product_id).addSnapshotListener(new EventListener<DocumentSnapshot>() {
                    @Override
                    public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                        if (documentSnapshot != null) {
                            MyCart myCart = documentSnapshot.toObject(MyCart.class);
                            if (myCart != null) {

                                int qty = myCart.getProduct_quantity();
                                mDialogProductOpenBinding.psc.setText(String.valueOf(qty));
                            }
                        }
                    }
                });

                //------------------------REAL TIME COUNT CART ---------END----------------------------------------

                mDialogProductOpenBinding.plus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        //--------------------------------
                        final String product_id = productDetails.getProduct_id();
                        String product_id_final = mDialogProductOpenBinding.proId.getText().toString();

                        DocumentReference myCartRef = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF).document(product_id);
                        myCartRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                            @Override
                            public void onSuccess(DocumentSnapshot documentSnapshot) {
                                if (documentSnapshot.exists()) {
                                    MyCart myCart1 = documentSnapshot.toObject(MyCart.class);
//                                    MyCart myCart = new MyCart(myCart1.getProduct_quantity() + 1, cat_id, cat_name, pro_code_final, pro_name, pro_size, pro_weight, sub_cat_id, sub_cat_name, img, product_id,null);
                                    MyCart myCart = new MyCart(myCart1.getProduct_quantity() + 1, cat_id, cat_name, product_id_final, pro_name, pro_size, pro_weight, sub_cat_id, sub_cat_name, img, product_id, null);
                                    CollectionReference mCollectionReferenceCart = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF);
                                    mCollectionReferenceCart.document(product_id).set(myCart).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @RequiresApi(api = Build.VERSION_CODES.M)
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isComplete()) {
//                                            Toast.makeText(mContext, "Added to Cart", Toast.LENGTH_LONG).show();
                                                long time_stamp= System.currentTimeMillis();
                                                FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid())
                                                        .collection(Constant.ITEM_LIST_REF).document(product_id)
                                                        .update("cart_timestamp",time_stamp);
                                            }
                                        }
                                    });
                                } else {
//                                    MyCart myCart = new MyCart(1, cat_id, cat_name, pro_code_final, pro_name, pro_size, pro_weight, sub_cat_id, sub_cat_name, img, product_id,null);
                                    MyCart myCart = new MyCart(1, cat_id, cat_name, product_id_final, pro_name, pro_size, pro_weight, sub_cat_id, sub_cat_name, img, product_id, null);
                                    CollectionReference mCollectionReferenceCart = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF);
                                    mCollectionReferenceCart.document(product_id).set(myCart).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @RequiresApi(api = Build.VERSION_CODES.M)
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isComplete()) {
//                                                Toast.makeText(mContext, "Added to Cart", Toast.LENGTH_LONG).show();
                                                long time_stamp= System.currentTimeMillis();
                                                FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid())
                                                        .collection(Constant.ITEM_LIST_REF).document(product_id)
                                                        .update("cart_timestamp",time_stamp);
                                            }
                                        }
                                    });
                                }
                            }
                        });
                        //--------------------------------
                    }
                });


                mDialogProductOpenBinding.minus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        //-------------------------
                        final String product_id = productDetails.getProduct_id();
                        String product_id_final = mDialogProductOpenBinding.proId.getText().toString();

                        DocumentReference myCartRef = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF).document(product_id);
                        myCartRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                            @Override
                            public void onSuccess(DocumentSnapshot documentSnapshot) {

                                if (documentSnapshot.exists()) {
                                    final MyCart myCart1 = documentSnapshot.toObject(MyCart.class);
//                                    MyCart myCart = new MyCart(myCart1.getProduct_quantity() - 1, cat_id, cat_name, pro_code_final, pro_name, pro_size, pro_weight, sub_cat_id, sub_cat_name, img, product_id,null);
                                    MyCart myCart = new MyCart(myCart1.getProduct_quantity() - 1, cat_id, cat_name, product_id_final, pro_name, pro_size, pro_weight, sub_cat_id, sub_cat_name, img, product_id, null);
                                    CollectionReference mCollectionReferenceCart = FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid()).collection(Constant.ITEM_LIST_REF);
                                    mCollectionReferenceCart.document(product_id).set(myCart).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @RequiresApi(api = Build.VERSION_CODES.M)
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isComplete()) {
                                                long time_stamp= System.currentTimeMillis();
                                                FirebaseFirestore.getInstance().collection(Constant.MY_CART_REF).document(mFirebaseUser.getUid())
                                                        .collection(Constant.ITEM_LIST_REF).document(product_id)
                                                        .update("cart_timestamp",time_stamp);
                                            }
                                        }
                                    });

                                    if (myCart1.getProduct_quantity() - 1 == 0) {

                                        mCollectionReferenceCart.document(myCart1.getProduct_id()).delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                            }
                                        });
                                    }

                                } else {

                                }


                            }
                        });
                        //-------------------------
                    }
                });

                dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
                dialog.setContentView(mDialogProductOpenBinding.getRoot());
                dialog.show();


            }
        });

        //---------------dialog-----------------end--------------------------------------------------------


    }


    public boolean checkConnection() {

        ConnectivityManager manager = (ConnectivityManager) mContext.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = manager.getActiveNetworkInfo();

        if (null != activeNetwork) {

            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
//                Toast.makeText(mContext, "Wifi Enabled", Toast.LENGTH_SHORT).show();
                return true;
            }

            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
//                Toast.makeText(mContext, "Data Network Enabled", Toast.LENGTH_SHORT).show();
                return true;

            }


        } else {
            Toast.makeText(mContext, "No Internet Connection", Toast.LENGTH_SHORT).show();
            return false;
        }


        return false;
    }

    @Override
    public int getItemCount() {
        return mSubCategoryLists.size();
    }

    public void filterList(ArrayList<DocumentSnapshot> filteredList) {
        mSubCategoryLists = filteredList;
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        RowCatWiseProductListBinding mRowCatWiseProductListBinding;

        public ViewHolder(@NonNull RowCatWiseProductListBinding itemView) {
            super(itemView.getRoot());
            mRowCatWiseProductListBinding = itemView;
        }
    }

}
