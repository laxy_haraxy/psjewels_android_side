package com.psjewels.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;


import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.psjewels.R;
import com.psjewels.model.ImageUrl;
import com.psjewels.utils.Constant;
import com.squareup.picasso.Picasso;

public class ImagesFireBaseAdapter extends FirestoreRecyclerAdapter<ImageUrl, BannerImgListViewHolder> {

    private Context mContext;
    private CollectionReference mDatabaseReferenceBannerImages;
    private ProgressDialog mProgressDialog;

    public ImagesFireBaseAdapter(@NonNull FirestoreRecyclerOptions<ImageUrl> options, Context mContext) {
        super(options);
        this.mContext = mContext;
    }

    @Override
    public void onDataChanged() {
        super.onDataChanged();
        notifyDataSetChanged();
    }

    @Override
    protected void onBindViewHolder(@NonNull final BannerImgListViewHolder holder, int position, @NonNull final ImageUrl model) {
        mProgressDialog = new ProgressDialog(mContext );
        mProgressDialog.setMessage(Constant.PROGRESS_DIALOG_MESSAGE);
        mProgressDialog.show();
        mProgressDialog.setCancelable(false);
        final String pushKey = getSnapshots().getSnapshot(position).getId();

        FirebaseUser mFirebaseUser;
        mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();


if(mFirebaseUser != null){
    mDatabaseReferenceBannerImages = FirebaseFirestore.getInstance().collection(Constant.USER_LIST_REF).document(mFirebaseUser.getUid()).collection(Constant.IMG_COLLECTION_CARDS);
}


        Picasso.get().load(model.getImage_url()).fit().centerInside().placeholder(R.drawable.placeholder).into(holder.mImageViewLaptop);
        mProgressDialog.dismiss();
        holder.mImageViewDelete.setVisibility(View.VISIBLE);
        holder.mImageViewDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder;


                builder = new AlertDialog.Builder(mContext);

                builder.setTitle("Delete Image")
                        .setMessage("Are you sure you want to delete this image?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // continue with delete
                                final ProgressDialog mProgressDialog = new ProgressDialog(mContext);
                                mProgressDialog.setMessage(Constant.PROGRESS_DIALOG_MESSAGE);
                                mProgressDialog.show();
                                //delete from storage
                                FirebaseStorage storage = FirebaseStorage.getInstance();
                                StorageReference photoRef = storage.getReferenceFromUrl(model.getImage_url());
                                photoRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        mDatabaseReferenceBannerImages.document(pushKey).delete().addOnCompleteListener(new
                                                                                                                                OnCompleteListener<Void>() {
                                                                                                                                    @Override
                                                                                                                                    public void onComplete(@NonNull Task<Void> task) {
                                                                                                                                        mProgressDialog.dismiss();
                                                                                                                                    }
                                                                                                                                });

                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception exception) {

                                        mDatabaseReferenceBannerImages.document(pushKey).delete().addOnCompleteListener(new
                                                                                                                                OnCompleteListener<Void>() {
                                                                                                                                    @Override
                                                                                                                                    public void onComplete(@NonNull Task<Void> task) {
                                                                                                                                        mProgressDialog.dismiss();
                                                                                                                                    }
                                                                                                                                });

                                    }
                                });

                                dialog.dismiss();
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                                dialog.dismiss();
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();

            }
        });


    }

    @NonNull
    @Override
    public BannerImgListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.row_layout_collect_details_img, viewGroup, false);
        return new BannerImgListViewHolder(view);
    }
}

class BannerImgListViewHolder extends RecyclerView.ViewHolder {
    ImageView mImageViewLaptop;
    ImageView mImageViewDelete;

    BannerImgListViewHolder(@NonNull View itemView) {
        super(itemView);
        mImageViewLaptop = itemView.findViewById(R.id.image_view_row_layout_multiple_img);
        mImageViewDelete = itemView.findViewById(R.id.image_view_delete_row_layout_multiple_img);
    }
}

