package com.psjewels.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.viewpager.widget.PagerAdapter;

import com.psjewels.R;
import com.psjewels.databinding.RowLayoutViewPagerProductDetailsBinding;
import com.psjewels.model.ImgList;
import com.psjewels.utils.Constant;
import com.squareup.picasso.Picasso;


import java.util.ArrayList;

public class MultiImgListAdapter extends PagerAdapter {
    private Context mContext;
    private LayoutInflater layoutInflater;
    private ArrayList<ImgList> mImgLists;
    private String pushKey;




    public MultiImgListAdapter(Context mContext, ArrayList<ImgList> mImgLists) {
        this.mContext = mContext;
        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mImgLists = mImgLists;

    }




    @Override
    public int getCount() {
        if (mImgLists != null) {
            return mImgLists.size();
        }

        return 0;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {



        final RowLayoutViewPagerProductDetailsBinding mRowLayoutViewPagerProductDetailsBinding = DataBindingUtil.inflate(layoutInflater, R.layout.row_layout_view_pager_product_details, container, false);
        final ImgList mImgList = mImgLists.get(position);


        Picasso.get().load(mImgList.getImg()).placeholder(R.drawable.placeholder).fit().centerInside().into(mRowLayoutViewPagerProductDetailsBinding.imgProductDetails);

        container.addView(mRowLayoutViewPagerProductDetailsBinding.getRoot());

        return mRowLayoutViewPagerProductDetailsBinding.getRoot();
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((LinearLayout) object);
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == ((LinearLayout) object);

    }


}
