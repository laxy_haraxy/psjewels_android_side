package com.psjewels.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.psjewels.R;
import com.psjewels.activity.CategoryWiseProductShowActivity;
import com.psjewels.activity.SubCatListActivity;
import com.psjewels.databinding.RowLayoutCatListBinding;
import com.psjewels.model.AddCategory;
import com.psjewels.model.ProductDeatils;
import com.psjewels.model.SubCatList;
import com.psjewels.utils.Constant;
import com.squareup.picasso.Picasso;


public class SubCatListAdapter extends FirestoreRecyclerAdapter<SubCatList, SubCatListAdapter.CatViewHolder> {

    private Context mContext;
    private LayoutInflater mLayoutInflater;

    /**
     * Create a new RecyclerView adapter that listens to a Firestore Query.  See {@link
     * FirestoreRecyclerOptions} for configuration options.
     *
     * @param options
     */
    public SubCatListAdapter(@NonNull FirestoreRecyclerOptions<SubCatList> options, Context mContext) {
        super(options);
        this.mContext = mContext;
    }




    @Override
    protected void onBindViewHolder(@NonNull CatViewHolder catViewHolder, final int i, @NonNull final SubCatList mSubCatList) {
        catViewHolder.mRowLayoutCatListBinding.txtCatName.setText(mSubCatList.getSub_cat_name());

        if(mSubCatList.getSub_cat_img() != null){

            Picasso.get().load(mSubCatList.getSub_cat_img()).placeholder(R.drawable.placeholder)
                    .fit().centerInside()
                    .into(catViewHolder.mRowLayoutCatListBinding.imgViewCat);

        }


    catViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {


            Intent mIntent = new Intent(mContext, CategoryWiseProductShowActivity.class);
            mIntent.putExtra(Constant.CAT_ID_KEY, mSubCatList.getCat_id());
            mIntent.putExtra(Constant.CAT_NAME,mSubCatList.getCat_name());
            mIntent.putExtra(Constant.SUB_CAT_NAME,mSubCatList.getSub_cat_name());
            mIntent.putExtra(Constant.SUB_CAT_ID,getSnapshots().getSnapshot(i).getId());

            mContext.startActivity(mIntent);
        }
    });


    }

    public boolean checkConnection() {

        ConnectivityManager manager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = manager.getActiveNetworkInfo();

        if (null != activeNetwork) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
//                Toast.makeText(this, "Wifi Enabled", Toast.LENGTH_SHORT).show();
                return true;
            }
            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
//                Toast.makeText(this, "Data Network Enabled", Toast.LENGTH_SHORT).show();
                return true;
            }
        } else {
            Toast.makeText(mContext, "No Internet Connection", Toast.LENGTH_SHORT).show();
            return false;
        }
        return false;
    }

    @NonNull
    @Override
    public CatViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (mLayoutInflater == null) {
            mLayoutInflater = LayoutInflater.from(parent.getContext());
        }
        RowLayoutCatListBinding mRowLayoutCatListBinding = DataBindingUtil.inflate(mLayoutInflater, R.layout.row_layout_cat_list, parent, false);
        return new CatViewHolder(mRowLayoutCatListBinding);


    }

    public class CatViewHolder extends RecyclerView.ViewHolder {
        RowLayoutCatListBinding mRowLayoutCatListBinding;

        public CatViewHolder(@NonNull RowLayoutCatListBinding itemView) {
            super(itemView.getRoot());
            mRowLayoutCatListBinding = itemView;
        }
    }


}
