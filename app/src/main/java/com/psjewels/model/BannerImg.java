package com.psjewels.model;

public class BannerImg {
    private String image_url;

    public BannerImg(String imgage_url) {
        this.image_url = imgage_url;
    }

    public BannerImg() {
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }
}
