package com.psjewels.model;

public class listStateCity {

    String city,state;

    public listStateCity() {
    }

    public listStateCity(String city, String state) {
        this.city = city;
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getState(String seletedState) {
        return seletedState;
    }
}
