package com.psjewels.model;

public class SearchKeyword  {

    private String id,search_keyword;

    public SearchKeyword() {
    }

    public SearchKeyword(String id, String search_keyword) {
        this.id = id;
        this.search_keyword = search_keyword;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSearch_keyword() {
        return search_keyword;
    }

    public void setSearch_keyword(String search_keyword) {
        this.search_keyword = search_keyword;
    }
}
