package com.psjewels.model;

public class SubCatList {

    String cat_id,cat_name,sub_cat_img,sub_cat_name,sub_cat_id;

    public SubCatList() {
    }

    public SubCatList(String cat_id, String cat_name, String sub_cat_img, String sub_cat_name) {
        this.cat_id = cat_id;
        this.cat_name = cat_name;
        this.sub_cat_img = sub_cat_img;
        this.sub_cat_name = sub_cat_name;
    }

    public String getSub_cat_id() {
        return sub_cat_id;
    }

    public void setSub_cat_id(String sub_cat_id) {
        this.sub_cat_id = sub_cat_id;
    }

    public String getCat_id() {
        return cat_id;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    public String getCat_name() {
        return cat_name;
    }

    public void setCat_name(String cat_name) {
        this.cat_name = cat_name;
    }

    public String getSub_cat_img() {
        return sub_cat_img;
    }

    public void setSub_cat_img(String sub_cat_img) {
        this.sub_cat_img = sub_cat_img;
    }

    public String getSub_cat_name() {
        return sub_cat_name;
    }

    public void setSub_cat_name(String sub_cat_name) {
        this.sub_cat_name = sub_cat_name;
    }
}
