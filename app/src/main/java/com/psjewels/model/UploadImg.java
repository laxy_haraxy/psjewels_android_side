package com.psjewels.model;

import android.net.Uri;

import java.util.List;

public class UploadImg {


    private Uri imageUri;

    private String imgUrl;

    public UploadImg(Uri imageUri, String imgUrl) {
        this.imageUri = imageUri;
        this.imgUrl = imgUrl;
    }

    public UploadImg(String imageUrl) {

    }

    public Uri getImageUri() {
        return imageUri;
    }

    public void setImageUri(Uri imageUri) {
        this.imageUri = imageUri;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }
}
