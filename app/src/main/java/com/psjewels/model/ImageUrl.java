package com.psjewels.model;

public class ImageUrl {
    private String image_url;

    public ImageUrl(String image_url) {
        this.image_url = image_url;
    }

    public ImageUrl() {
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }
}
