package com.psjewels.model;

import java.util.List;

public class ImgList {

    private String img;
    private int id;


    public ImgList() {
    }

    public ImgList(String img, int id) {
        this.img = img;
        this.id = id;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}