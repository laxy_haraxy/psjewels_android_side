package com.psjewels.model;

public class AddUser {
    private String token_id,user_id,company_name,contact_person,phone_number,address,state,city,deviceId;

    private String acc_created_date;

    private boolean status;

    private long time_stamp;

    String Latitude;
    String Longitude;


    public AddUser() {
    }


    public AddUser(String token_id, String user_id, String company_name, String contact_person, String phone_number, String address, String state, String city, String deviceId, String acc_created_date, boolean status, long time_stamp, String latitude, String longitude) {
        this.token_id = token_id;
        this.user_id = user_id;
        this.company_name = company_name;
        this.contact_person = contact_person;
        this.phone_number = phone_number;
        this.address = address;
        this.state = state;
        this.city = city;
        this.deviceId = deviceId;
        this.acc_created_date = acc_created_date;
        this.status = status;
        this.time_stamp = time_stamp;
        Latitude = latitude;
        Longitude = longitude;
    }


    public String getToken_id() {
        return token_id;
    }

    public void setToken_id(String token_id) {
        this.token_id = token_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getContact_person() {
        return contact_person;
    }

    public void setContact_person(String contact_person) {
        this.contact_person = contact_person;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getAcc_created_date() {
        return acc_created_date;
    }

    public void setAcc_created_date(String acc_created_date) {
        this.acc_created_date = acc_created_date;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public long getTime_stamp() {
        return time_stamp;
    }

    public void setTime_stamp(long time_stamp) {
        this.time_stamp = time_stamp;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }
}


