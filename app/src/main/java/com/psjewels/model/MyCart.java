package com.psjewels.model;

public class MyCart {

    int product_quantity;
    String cat_id, cat_name, pro_code, pro_name, pro_size, pro_weight, sub_cat_id, sub_cat_name, img,product_id;
    String note;


    public MyCart() {
    }


    public MyCart(int product_quantity, String cat_id, String cat_name, String pro_code, String pro_name, String pro_size, String pro_weight, String sub_cat_id, String sub_cat_name, String img, String product_id,String note) {
        this.product_quantity = product_quantity;
        this.cat_id = cat_id;
        this.cat_name = cat_name;
        this.pro_code = pro_code;
        this.pro_name = pro_name;
        this.pro_size = pro_size;
        this.pro_weight = pro_weight;
        this.sub_cat_id = sub_cat_id;
        this.sub_cat_name = sub_cat_name;
        this.img = img;
        this.product_id = product_id;
        this.note = note;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public int getProduct_quantity() {
        return product_quantity;
    }

    public void setProduct_quantity(int product_quantity) {
        this.product_quantity = product_quantity;
    }

    public String getCat_id() {
        return cat_id;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    public String getCat_name() {
        return cat_name;
    }

    public void setCat_name(String cat_name) {
        this.cat_name = cat_name;
    }

    public String getPro_code() {
        return pro_code;
    }

    public void setPro_code(String pro_code) {
        this.pro_code = pro_code;
    }

    public String getPro_name() {
        return pro_name;
    }

    public void setPro_name(String pro_name) {
        this.pro_name = pro_name;
    }

    public String getPro_size() {
        return pro_size;
    }

    public void setPro_size(String pro_size) {
        this.pro_size = pro_size;
    }

    public String getPro_weight() {
        return pro_weight;
    }

    public void setPro_weight(String pro_weight) {
        this.pro_weight = pro_weight;
    }

    public String getSub_cat_id() {
        return sub_cat_id;
    }

    public void setSub_cat_id(String sub_cat_id) {
        this.sub_cat_id = sub_cat_id;
    }

    public String getSub_cat_name() {
        return sub_cat_name;
    }

    public void setSub_cat_name(String sub_cat_name) {
        this.sub_cat_name = sub_cat_name;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }
}
