package com.psjewels.model;

public class MyOrder {

    private String order_date, order_time,user_id,note;
    private AddUser mAddUser;
    private boolean order_accepted,order_pending;
    private int order_id;
    private long time_stamp;
    String notes;
    public MyOrder() {
    }


    public MyOrder(String order_date, String order_time, String user_id, String note, AddUser mAddUser, boolean order_accepted, boolean order_pending, int order_id, long time_stamp) {
        this.order_date = order_date;
        this.order_time = order_time;
        this.user_id = user_id;
        this.note = note;
        this.mAddUser = mAddUser;
        this.order_accepted = order_accepted;
        this.order_pending = order_pending;
        this.order_id = order_id;
        this.time_stamp = time_stamp;


    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getOrder_date() {
        return order_date;
    }

    public void setOrder_date(String order_date) {
        this.order_date = order_date;
    }

    public String getOrder_time() {
        return order_time;
    }

    public void setOrder_time(String order_time) {
        this.order_time = order_time;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public AddUser getmAddUser() {
        return mAddUser;
    }

    public void setmAddUser(AddUser mAddUser) {
        this.mAddUser = mAddUser;
    }

    public boolean isOrder_accepted() {
        return order_accepted;
    }

    public void setOrder_accepted(boolean order_accepted) {
        this.order_accepted = order_accepted;
    }

    public boolean isOrder_pending() {
        return order_pending;
    }

    public void setOrder_pending(boolean order_pending) {
        this.order_pending = order_pending;
    }

    public int getOrder_id() {
        return order_id;
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    public long getTime_stamp() {
        return time_stamp;
    }

    public void setTime_stamp(long time_stamp) {
        this.time_stamp = time_stamp;
    }
}
