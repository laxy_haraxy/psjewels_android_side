package com.psjewels.model;

import com.google.protobuf.LazyStringArrayList;

import java.lang.reflect.Array;
import java.util.List;

public class ProductDeatils {

    private  String cat_id,cat_name,pro_code,pro_name,pro_size,pro_weight,sub_cat_id,sub_cat_name,img,product_id,pro_concet_name;
    private  int pro_code_number;
    private  long time_stamp;
     private List<String> keyword_list_name;




    public ProductDeatils() {
    }

    public ProductDeatils(String cat_id, String cat_name, String pro_code, String pro_name, String pro_size, String pro_weight, String sub_cat_id, String sub_cat_name, String img, String product_id, String pro_concet_name, int pro_code_number, long time_stamp, List<String> keyword_list_name) {
        this.cat_id = cat_id;
        this.cat_name = cat_name;
        this.pro_code = pro_code;
        this.pro_name = pro_name;
        this.pro_size = pro_size;
        this.pro_weight = pro_weight;
        this.sub_cat_id = sub_cat_id;
        this.sub_cat_name = sub_cat_name;
        this.img = img;
        this.product_id = product_id;
        this.pro_concet_name = pro_concet_name;
        this.pro_code_number = pro_code_number;
        this.time_stamp = time_stamp;
        this.keyword_list_name = keyword_list_name;
    }


    public String getCat_id() {
        return cat_id;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    public String getCat_name() {
        return cat_name;
    }

    public void setCat_name(String cat_name) {
        this.cat_name = cat_name;
    }

    public String getPro_code() {
        return pro_code;
    }

    public void setPro_code(String pro_code) {
        this.pro_code = pro_code;
    }

    public String getPro_name() {
        return pro_name;
    }

    public void setPro_name(String pro_name) {
        this.pro_name = pro_name;
    }

    public String getPro_size() {
        return pro_size;
    }

    public void setPro_size(String pro_size) {
        this.pro_size = pro_size;
    }

    public String getPro_weight() {
        return pro_weight;
    }

    public void setPro_weight(String pro_weight) {
        this.pro_weight = pro_weight;
    }

    public String getSub_cat_id() {
        return sub_cat_id;
    }

    public void setSub_cat_id(String sub_cat_id) {
        this.sub_cat_id = sub_cat_id;
    }

    public String getSub_cat_name() {
        return sub_cat_name;
    }

    public void setSub_cat_name(String sub_cat_name) {
        this.sub_cat_name = sub_cat_name;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getPro_concet_name() {
        return pro_concet_name;
    }

    public void setPro_concet_name(String pro_concet_name) {
        this.pro_concet_name = pro_concet_name;
    }

    public int getPro_code_number() {
        return pro_code_number;
    }

    public void setPro_code_number(int pro_code_number) {
        this.pro_code_number = pro_code_number;
    }

    public long getTime_stamp() {
        return time_stamp;
    }

    public void setTime_stamp(long time_stamp) {
        this.time_stamp = time_stamp;
    }

    public List<String> getKeyword_list_name() {
        return keyword_list_name;
    }

    public void setKeyword_list_name(List<String> keyword_list_name) {
        this.keyword_list_name = keyword_list_name;
    }
}
